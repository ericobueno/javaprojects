package com.ebueno.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.naming.Binding;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingEnumeration;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

/**
 * Servlet implementation class JndiTestServlet
 */

@WebServlet("/JndiTestServlet")
public class JndiTestServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	// @Resource(name = "jdbc/web_student_tracker")
	private DataSource dataSource;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public JndiTestServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		Connection connection = null;
		Statement sqlStatment = null;
		ResultSet resultSet = null;
		String dataSourceName = "java:comp/env/jdbc/tomcat_web_student_tracker";

		System.out.println("JNDI Test Servlet. DataSourceName=" + dataSourceName);

		// 1 - Set up printWriter

		PrintWriter writer = response.getWriter();

		writer.println("<html><body>");

		writer.println("Hello from JNDI TestServlet <br /><br />");
		
		writer.format("JNDI datasource= %s</br></br>", dataSourceName);
		
		try {

			Context ctx = new InitialContext();

			NamingEnumeration<Binding> bindings = ctx.listBindings("");

			while (bindings.hasMore()) {
				Binding bd = (Binding) bindings.next();
				writer.println(bd.getName() + ": " + bd.getObject());
			}
			
			dataSource = (DataSource) ctx.lookup(dataSourceName);

			// 2 - Get connection to database

			connection = dataSource.getConnection();

			// 3 - Create SQL statement

			sqlStatment = connection.createStatement();

			// 4 - Execute query statement

			resultSet = sqlStatment.executeQuery("select * from student");

			// 5 - Process result set

			while (resultSet.next()) {

				String email = resultSet.getString("email");

				writer.println("email: " + email + "<br />");

			}

			writer.println("</body></html>");

		} catch (Exception e) {

			e.printStackTrace();
		}
	}
}
