package com.ebueno.web.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import com.ebueno.web.model.Student;

public class StudentDao {

	private DataSource datasource;

	public StudentDao() throws Exception {
		super();
	}

	public StudentDao(DataSource datasource) throws Exception {
		this.datasource = datasource;

		System.out.println("DataSOurce injected into StudentDao");
	}

	public List<Student> getStudents() throws Exception {
		Connection connection = null;
		Statement statement = null;
		ResultSet rs = null;

		List<Student> students = new ArrayList<>();

		try {

			connection = datasource.getConnection();

			statement = connection.createStatement();

			String sql = "select * from student order by last_name";

			rs = statement.executeQuery(sql);

			while (rs.next()) {

				Student student = new Student(rs.getInt("id"), rs.getString("first_name"), rs.getString("last_name"),
						rs.getString("email"));

				students.add(student);
			}
			return students;

		} finally {

			close(connection, statement, rs);
		}
	}

	public Student addStudent(Student student) throws SQLException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;

		String sql = "insert into student (first_name, last_name, email) values (?,?,?)";

		try {

			connection = datasource.getConnection();

			preparedStatement = connection.prepareStatement(sql);

			preparedStatement.setString(1, student.getFirstName());
			preparedStatement.setString(2, student.getLastName());
			preparedStatement.setString(3, student.getEmail());

			// int count = preparedStatement.executeUpdate();
			boolean success = preparedStatement.execute();

			if (success)
				System.out.println("Inserted " + student);

			return student;

		} finally {

			close(connection, preparedStatement, null);
		}
	}

	private void close(Connection connection, Statement statement, ResultSet rs) {

		try {

			if (rs != null) {
				rs.close();
				rs = null;
			}

			if (statement != null) {
				statement.close();
				statement = null;
			}

			if (connection != null) {
				connection.close();
				connection = null;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public Student getStudent(String studentId) throws Exception {
		Connection connection = null;
		String sql;
		PreparedStatement preparedStatement = null;
		ResultSet rs = null;
		Student student = null;
		int Id = 0;

		try {
			// Convert student id to int

			Id = Integer.parseInt(studentId);

			// get connection to database

			connection = datasource.getConnection();

			// create sql to get select student

			sql = "select * from student where id = ?";

			// create prepared statement

			preparedStatement = connection.prepareStatement(sql);

			// set params into prepared statement

			preparedStatement.setInt(1, Id);

			// retrieve data to populate student object
			rs = preparedStatement.executeQuery();

			if (rs.next()) {

				String first_name = rs.getString("first_name");
				String last_name = rs.getString("last_name");
				String email = rs.getString("email");

				student = new Student(Id, first_name, last_name, email);
			} else {
				throw new Exception("Could find student Id = " + Id);
			}

		} finally {
			close(connection, preparedStatement, rs);
		}

		return student;
	}

	public Student updateStudent(Student updatedStudent) throws SQLException {
		Connection connection = null;
		String sql;
		PreparedStatement preparedStatement = null;
		ResultSet rs = null;

		try {

			// get connection to database

			connection = datasource.getConnection();

			// create sql to get select student

			sql = "update student set first_name = ?, last_name = ?, email=? where id = ?";

			// create prepared statement

			preparedStatement = connection.prepareStatement(sql);

			// set params into prepared statement

			preparedStatement.setString(1, updatedStudent.getFirstName());
			preparedStatement.setString(2, updatedStudent.getLastName());
			preparedStatement.setString(3, updatedStudent.getEmail());

			preparedStatement.setInt(4, updatedStudent.getId());

			// preparedStatement.execute();
			int count = preparedStatement.executeUpdate();
			if (count == 0) {
				System.out.println("Failed updating student");
			} else {
				System.out.println("Updated " + count + " record(s)");
			}

		} finally {
			close(connection, preparedStatement, rs);
		}

		return updatedStudent;
	}

	public void deleteStudent(int studentId) throws SQLException {
		Connection connection = null;
		String sql;
		PreparedStatement preparedStatement = null;
		ResultSet rs = null;

		try {

			// get connection to database

			connection = datasource.getConnection();

			// create sql to get select student

			sql = "delete from student where id = ?";

			// create prepared statement

			preparedStatement = connection.prepareStatement(sql);

			// set params into prepared statement

			preparedStatement.setInt(1, studentId);

			boolean success = preparedStatement.execute();

			System.out.println("deleting student returned " + success);

		} finally {
			close(connection, preparedStatement, rs);
		}
	}

}
