package com.ebueno.web.controllers;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import com.ebueno.web.dao.StudentDao;
import com.ebueno.web.model.Student;

/**
 * Servlet implementation class StudentControllerServlet
 * @author Erico
 */
@WebServlet("/StudentControllerServlet")
public class StudentControllerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;


//	@Resource(name = "jdbc/web_student_tracker")
	@Resource(name = "jdbc/tomcat_web_student_tracker")
	private DataSource dataSource;

	private StudentDao studentDao = null;

	@Override
	public void init() throws ServletException {
		super.init();

		try {
			studentDao = new StudentDao(dataSource);
		} catch (Exception e) {

			throw new ServletException(e);
		}
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		try {

			String command = request.getParameter("command");

			if (command == null) {
				command = "LIST";
			}

			switch (command) {
			case "LIST":
				listStudents(request, response);
				break;

			case "ADD":
				addStudent(request, response);
				break;

			case "LOAD":
				loadStudent(request, response);
				break;

			case "UPDATE":
				updateStudent(request, response);
				break;

			case "DELETE":
				deleteStudent(request, response);
				break;

			default:
				listStudents(request, response);
			}

		} catch (Exception e) {
			throw new ServletException(e);
		}

	}

	private void deleteStudent(HttpServletRequest request, HttpServletResponse response) throws Exception {

		String studentId = request.getParameter("studentId");

		int id = Integer.parseInt(studentId);

		System.out.println("Delete student id = " + id);
		// Perform update on database
		
		studentDao.deleteStudent(id);

		// Send them back to list-students page
		listStudents(request, response); // Go show all students

	}

	private void updateStudent(HttpServletRequest request, HttpServletResponse response) throws Exception {
		System.out.println("Updating student");

		// Read updated student info from form

		String studentId = request.getParameter("studentId");
		String firstName = request.getParameter("firstName");
		String lastName = request.getParameter("lastName");
		String email = request.getParameter("email");

		// Create an updated Student object

		int id = Integer.parseInt(studentId);

		Student updatedStudent = new Student(id, firstName, lastName, email);

		// Perform update on database
		studentDao.updateStudent(updatedStudent);

		// Send them back to list-students page
		listStudents(request, response); // Go show all students

	}

	private void loadStudent(HttpServletRequest request, HttpServletResponse response) throws Exception {

		// Read Student Id from form data
		String studentId = request.getParameter("studentId");

		// Get student from database (StudentDao

		Student student = studentDao.getStudent(studentId);

		// Place student in request attribute

		request.setAttribute("STUDENT", student);

		// Send to jsp page: update-student form

		RequestDispatcher requestDispatcher = request.getRequestDispatcher("/update-student-form.jsp");

		requestDispatcher.forward(request, response);

	}

	private void addStudent(HttpServletRequest request, HttpServletResponse response) throws Exception {

		Student student = new Student(request.getParameter("firstName"), request.getParameter("lastName"),
				request.getParameter("email"));

		System.out.println("Adding" + student);
		Student addStudent = studentDao.addStudent(student); // add the student

		System.out.println("Added" + addStudent);

		listStudents(request, response); // Go show all students

	}

	private void listStudents(HttpServletRequest request, HttpServletResponse response) throws Exception {

		List<Student> students = studentDao.getStudents();

		request.setAttribute("DATE", new Date());

		request.setAttribute("STUDENT_LIST", students);

		RequestDispatcher requestDispatcher = request.getRequestDispatcher("/list-students.jsp");

		requestDispatcher.forward(request, response);
	}

}
