package com.ebueno.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

/**
 * Servlet implementation class TestServlet
 */
@WebServlet("/TestServlet")
public class TestServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	// Define Datasource/connection pool for Resource injection

	@Resource(name = "jdbc/tomcat_web_student_tracker")
//	@Resource(name = "jdbc/web_student_tracker")
	private DataSource dataSource;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		Connection connection = null;
		Statement sqlStatment = null;
		ResultSet resultSet = null;

		System.out.println("Test Servlet called");

		// 1 - Set up printWriter

		PrintWriter writer = response.getWriter();

		writer.println("<html><body>");

		writer.println("Hello from TestServlet <br /><br />");

		try {

			// 2 - Get connection to database

			connection = dataSource.getConnection();

			// 3 - Create SQL statement

			sqlStatment = connection.createStatement();

			// 4 - Execute query statement

			resultSet = sqlStatment.executeQuery("select * from student");

			// 5 - Process result set

			while (resultSet.next()) {

				String email = resultSet.getString("email");

				writer.println("email: " + email + "<br />");

			}

			writer.println("</body></html>");

		} catch (Exception e) {

			e.printStackTrace();
		}

	}

}
