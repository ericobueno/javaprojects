<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Student tracker Application</title>

<link type="text/css" rel="stylesheet" href="css/style.css">

</head>
<body>

	<div id="wrapper">
		<div id="header">
			<h2>FOOBAR University</h2>
			<h3>${DATE}</h3>
		</div>

	</div>

	<div id="container">
		<div id="content">

			<br /> <input type="button" value="Add Student"
				onclick="window.location.href='add-student-form.jsp'; return false;"
				class=" add-student-button" />

			<table border="1">

				<tr>
					<th>First Name</th>
					<th>Last Name</th>
					<th>Email</th>
					<th>Action</th>
				</tr>

				<!-- Set up a link to update each student -->

				<c:forEach var="aStudent" items="${STUDENT_LIST}">
				
					<c:url var="updateLink" value="StudentControllerServlet">
						<c:param name="command" value="LOAD" />
						<c:param name="studentId" value="${aStudent.id}" />
					</c:url>
					
					<!-- Set up a link to delete each student -->
					
					<c:url var="deleteLink" value="StudentControllerServlet">
						<c:param name="command" value="DELETE" />
						<c:param name="studentId" value="${aStudent.id}" />
					</c:url>
					
					<tr>
						<td>${aStudent.firstName}</td>
						<td>${aStudent.lastName}</td>
						<td>${aStudent.email}</td>
						<td>
							<a href="${updateLink}">Update</a> |
							<a href="${deleteLink}" 
							onclick="if(!(confirm('Are you sure?'))) return false">Delete</a> <!-- THis is java script -->
						</td>
					</tr>
				</c:forEach>

			</table>

			<h1>
				<a href="index.jsp">Home</a>
			</h1>

		</div>
	</div>
</body>
</html>