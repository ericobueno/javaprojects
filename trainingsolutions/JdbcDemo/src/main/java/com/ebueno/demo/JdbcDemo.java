package com.ebueno.demo;

import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

/**
 * Hello world!
 *
 */
public class JdbcDemo {

	Connection connection = null;
	Statement stmt = null;
	ResultSet rs = null;

//	static String url = "jdbc:mysql://localhost:3306/demo?useSSL=false";
//	static String user = "student";
//	static String password = "Buddy357";

	public static void main(String[] args) throws Exception {
		String url;
		String user;
		String password;
		Connection connection = null;
		JdbcDemo jdbc = null;

		try {
			Properties props = new Properties();
			props.load(new FileInputStream("jdbc.properties"));
			// props.load(new
			// FileInputStream("C:/local/config/jdbc.properties"));

			url = props.getProperty("url");
			user = props.getProperty("user");
			password = props.getProperty("password");

			System.out.println("url=" + url);
			System.out.println("user=" + user);

			connection = DriverManager.getConnection(url, user, password);
			jdbc = new JdbcDemo(connection);
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Failed to connect to mySql");
		}

		jdbc.list();

		jdbc.insertDemo();

		jdbc.updateDemo();

		jdbc.deleteDemo();

		if (connection != null) {
			connection.close();
		}
	}

	public JdbcDemo(Connection connection) {
		this.connection = connection;
	}

	public void list() {
		try {

			stmt = connection.createStatement();

			rs = stmt.executeQuery("select * from employees");

			while (rs.next()) {
				System.out.println(rs.getString("first_name") + ", " + rs.getString("last_name"));
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			close(stmt, rs);
		}
	}

	public void insertDemo() {

		System.out.println("\nBegin insertDemo");

		try {

			stmt = connection.createStatement();

			String sql = "insert into employees " + "(last_name, first_name, email, department, salary) " + "values "
					+ "('Bueno', 'Erico', 'ericobueno@comcast.net', 'TransArmor', 137000.00)";

			int affected = stmt.executeUpdate(sql);

			System.out.println("Inserted " + affected + " employees(s0");

			rs = stmt.executeQuery("select * from employees order by first_name");

			while (rs.next()) {
				System.out.println(rs.getString("first_name") + ", " + rs.getString("last_name"));
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void updateDemo() {

		System.out.println("\nBegin Update Demo");

		try {

			stmt = connection.createStatement();

			System.out.println("Before update");

			displayEmployee(connection, "Erico", "Bueno");

			String sql = "update employees " + "set email='erico@1031@gmail.com' "
					+ "where last_name='Bueno' and first_name='Erico'";

			int affected = stmt.executeUpdate(sql);

			System.out.println("Updated " + affected + " employees(s");

			System.out.println("After update");

			displayEmployee(connection, "Erico", "Bueno");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void deleteDemo() {

		System.out.println("\nBegin Delete Demo");

		try {

			stmt = connection.createStatement();

			System.out.println("Before delete");

			displayEmployee(connection, "Erico", "Bueno");

			String sql = "delete from employees " + "where last_name='Bueno' and first_name='Erico'";

			int affected = stmt.executeUpdate(sql);

			System.out.println("Deleted " + affected + " employees(s");

			System.out.println("After delete");

			displayEmployee(connection, "Erico", "Bueno");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void displayEmployee(Connection connection, String firstName, String lastName) throws SQLException {

		PreparedStatement stmt = null;
		ResultSet rs = null;

		try {

			// Prepare statement
			stmt = connection.prepareStatement(
					"select last_name, first_name, email from employees where last_name=? and first_name=?");

			stmt.setString(1, lastName);
			stmt.setString(2, firstName);

			// Execute SQL query
			rs = stmt.executeQuery();

			// Process result set
			while (rs.next()) {
				String theLastName = rs.getString("last_name");
				String theFirstName = rs.getString("first_name");
				String email = rs.getString("email");

				System.out.printf("%s %s, %s\n", theFirstName, theLastName, email);
			}
		} catch (Exception exc) {
			exc.printStackTrace();
		}
	}

	private void close(Statement stmt, ResultSet rs) {

		try {
			if (rs != null) {
				rs.close();
			}

			if (stmt != null) {
				stmt.close();
			}
		} catch (SQLException e) {
			System.out.println("Error closing");
			e.printStackTrace();
		}
	}
}
