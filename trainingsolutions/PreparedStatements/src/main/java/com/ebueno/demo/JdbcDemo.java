package com.ebueno.demo;

import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

import javax.sql.DataSource;

import com.mysql.jdbc.jdbc2.optional.MysqlConnectionPoolDataSource;

//import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;


/**
 * Hello world!
 *
 */
public class JdbcDemo {

	Connection connection = null;
	Statement stmt = null;
	ResultSet rs = null;

//	static String url = "jdbc:mysql://localhost:3306/demo?useSSL=false";
//	static String user = "student";
//	static String password = "Buddy357";

	public static void main(String[] args) throws Exception {
		
		String url;
		String user;
		String password;
		Connection connection = null;
		JdbcDemo jdbc = null;

		
		try {
			
			Properties props = new Properties();
			props.load(new FileInputStream("jdbc.properties"));
//			props.load(new FileInputStream("C:/local/config/jdbc.properties"));
			
			url = props.getProperty("url");
			user = props.getProperty("user");
			password = props.getProperty("password");
			
			System.out.println("url=" + url);
			System.out.println("user=" + user);
			
			DataSource datasource = geMysqlDataSource( url, user, password);
			
			connection = datasource.getConnection(); // Preferred way
			
//			connection = DriverManager.getConnection(url, user, password); // Old way
			
			jdbc = new JdbcDemo(connection);
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Failed to connect to mySql");
		}

		jdbc.listAll();

		jdbc.listHighSalary(90000.00, "Legal");

		jdbc.insertDemo();

		jdbc.updateDemo();

		jdbc.deleteDemo();

		if (connection != null) {
			connection.close();
		}
	}

	
	public static DataSource geMysqlDataSource(String url, String user, String password)
	{
		
		MysqlConnectionPoolDataSource ds = null;
		
		try {

			ds = new MysqlConnectionPoolDataSource();
			ds.setURL(url);
			ds.setUser(user);
			ds.setPassword(password);
			
		}
		catch ( Exception e) {
			e.printStackTrace();
		}
		
		return ds;
	}
	
	public JdbcDemo(Connection connection) {
		this.connection = connection;
	}

	public void listAll() {
		try {

			stmt = connection.createStatement();

			rs = stmt.executeQuery("select * from employees");

			while (rs.next()) {
				System.out.println(rs.getString("first_name") + ", " + rs.getString("last_name"));
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			close(stmt, rs);
		}
	}

	public void listHighSalary(double salary, String department) {
		try {
			System.out.println("\nList High Salaries");
			
			PreparedStatement preparedStatement = null;
			
			String sql = "select * from employees where salary > ? and department=?";

			preparedStatement = connection.prepareStatement(sql);

			preparedStatement.setDouble(1, salary);
			preparedStatement.setString(2, department);

			rs = preparedStatement.executeQuery();

			while (rs.next()) {
				System.out.println(
						rs.getString("first_name") + ", " + rs.getString("last_name") + " " + rs.getDouble("salary"));
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			close(stmt, rs);
		}
	}

	public void insertDemo() {

		System.out.println("\nBegin insertDemo");
		PreparedStatement preparedStatement = null;

		try {

			String sql = "insert into employees (last_name, first_name, email, department, salary) values (?,?,?,?,?)";

			preparedStatement = connection.prepareStatement(sql);

			preparedStatement.setString(1, "Bueno");
			preparedStatement.setString(2, "Erico");
			preparedStatement.setString(3, "ericobueno@comcast.net");
			preparedStatement.setString(4, "TransArmor");
			preparedStatement.setDouble(5, 127000);

			preparedStatement.execute();

			listAll();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void updateDemo() {

		System.out.println("\nBegin Update Demo");

		try {

			System.out.println("Before update");

			displayEmployee(connection, "Erico", "Bueno");

			String sql = "update employees set email=? where last_name=? and first_name=?";

			PreparedStatement preparedStatement = connection.prepareStatement(sql);

			preparedStatement.setString(1, "erico1031@gmail.com");
			preparedStatement.setString(2, "Bueno");
			preparedStatement.setString(3, "Erico");

			int affected = preparedStatement.executeUpdate();

			System.out.println("Updated " + affected + " employees(s");

			System.out.println("After update");

			displayEmployee(connection, "Erico", "Bueno");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void deleteDemo() {

		System.out.println("\nBegin Delete Demo");

		try {

			System.out.println("Before delete");

			displayEmployee(connection, "Erico", "Bueno");

			String sql = "delete from employees where last_name=? and first_name=?";

			PreparedStatement preparedStatement = connection.prepareStatement(sql);

			preparedStatement.setString(1, "Bueno");
			preparedStatement.setString(2, "Erico");

			int affected = preparedStatement.executeUpdate();

			System.out.println("Deleted " + affected + " employees(s");

			System.out.println("After delete");

			displayEmployee(connection, "Erico", "Bueno");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void displayEmployee(Connection connection, String firstName, String lastName) throws SQLException {

		PreparedStatement stmt = null;
		ResultSet rs = null;

		try {

			// Prepare statement
			stmt = connection.prepareStatement(
					"select last_name, first_name, email from employees where last_name=? and first_name=?");

			stmt.setString(1, lastName);
			stmt.setString(2, firstName);

			// Execute SQL query
			rs = stmt.executeQuery();

			// Process result set
			while (rs.next()) {
				String theLastName = rs.getString("last_name");
				String theFirstName = rs.getString("first_name");
				String email = rs.getString("email");

				System.out.printf("%s %s, %s\n", theFirstName, theLastName, email);
			}
		} catch (Exception exc) {
			exc.printStackTrace();
		}
	}

	private void close(Statement stmt, ResultSet rs) {

		try {
			if (rs != null) {
				rs.close();
			}

			if (stmt != null) {
				stmt.close();
			}
		} catch (SQLException e) {
			System.out.println("Error closing");
			e.printStackTrace();
		}
	}
}
