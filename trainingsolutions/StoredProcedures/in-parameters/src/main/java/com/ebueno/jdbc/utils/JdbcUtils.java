package com.ebueno.jdbc.utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.sql.DataSource;

import com.mysql.jdbc.jdbc2.optional.MysqlConnectionPoolDataSource;

public class JdbcUtils {

    public static DataSource getMysqlDataSource(String url, String user,
               String password) {

          MysqlConnectionPoolDataSource ds = null;

          try {

               ds = new MysqlConnectionPoolDataSource();
               ds.setURL(url);
               ds.setUser(user);
               ds.setPassword(password);

          } catch (Exception e) {
               e.printStackTrace();
          }
          return ds;
    }

    public static void listAllEmployees(Connection connection,
               String department) {
          
          Statement stmt = null;
          PreparedStatement preparedStatement = null;
          ResultSet rs = null;

          try {
               if (department.isEmpty()) {
                    stmt = connection.createStatement();

                    rs = stmt.executeQuery("select * from employees");
               } else {
                    String sql = "select * from employees where department=?";

                    preparedStatement = connection.prepareStatement(sql);

                    preparedStatement.setString(1, department);

                    rs = preparedStatement.executeQuery();
               }

               while (rs.next()) {
                    System.out.format("%s,%s salary= %.2f\n",
                               rs.getString("first_name"), rs.getString("last_name"),
                               rs.getDouble("salary"));
               }

          } catch (Exception e) {
               e.printStackTrace();
          } finally {
               close(stmt, rs);
          }
    }

    public static void close(Statement stmt, ResultSet rs) {

          try {
               if (rs != null) {
                    rs.close();
               }

               if (stmt != null) {
                    stmt.close();
               }

          } catch (SQLException e) {
               System.out.println("Error closing");
               e.printStackTrace();
          }
    }
}

