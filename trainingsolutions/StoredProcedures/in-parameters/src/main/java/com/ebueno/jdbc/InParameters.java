package com.ebueno.jdbc;

import java.io.FileInputStream;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.util.Properties;

import javax.sql.DataSource;

import com.ebueno.jdbc.utils.JdbcUtils;

public class InParameters {

	public static void main(String[] args) {

		String url;
		String user;
		String password;
		Connection connection = null;

		try {

			Properties props = new Properties();
			props.load(new FileInputStream("jdbc.properties"));

			url = props.getProperty("url");
			user = props.getProperty("user");
			password = props.getProperty("password");

			System.out.println("url=" + url);
			System.out.println("user=" + user);

			DataSource datasource = JdbcUtils.getMysqlDataSource(url, user, password);

			connection = datasource.getConnection(); // Preferred way

			String department = "Engineering";
			int increase = 10000;

			CallableStatement callableStatement = connection
					.prepareCall("{call increase_salaries_for_department(?,?)}");

			callableStatement.setString(1, department);
			callableStatement.setDouble(2, increase);

			System.out.println("Before");
			JdbcUtils.listAllEmployees(connection, department);

			callableStatement.execute();

			System.out.println("\nAfter");
			JdbcUtils.listAllEmployees(connection, department);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
