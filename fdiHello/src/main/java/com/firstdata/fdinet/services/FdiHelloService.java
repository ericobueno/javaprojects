package com.firstdata.fdinet.services;

import java.util.*;
import java.io.IOException;

import fdinet.core.*;

/*
* GENERATED CODE, DO NOT TOUCH !
*/
public abstract class FdiHelloService extends FDiNetServiceBase implements FdiHelloDef {

	private Map<String, BaseHandler> handlers = new HashMap<String, BaseHandler>();
		
	public FdiHelloService(String serviceName) {
		this(serviceName, Processor.NULL_PROCESSOR);
	}

	public FdiHelloService(String serviceName, Processor processor) {
		super(serviceName, processor);
		handlers.put("getMessage",new BaseHandler(fdinet.core.StringHolder.class) {
			@SuppressWarnings("unchecked")
			@Override
			protected void processRequest(Object request, TypedResponseHandler<?> r) throws FDiNetException {
				getMessage((fdinet.core.StringHolder) request, (TypedResponseHandler<fdinet.core.StringHolder>)r);
			}
		});

		setBaseProcessors(handlers);
	}
}
