package com.firstdata.fdinet.services;

import fdinet.core.TypedResponseHandler;
import fdinet.core.annotations.FDiNetService;
import fdinet.core.annotations.FDiNetServiceAPI_RR;
import fdinet.core.annotations.FDiNetWebInfo;
import fdinet.core.StringHolder;

@FDiNetService(version=1)
@FDiNetWebInfo(path="fdihello")
public interface FdiHello {
	
	@FDiNetServiceAPI_RR(responseType = StringHolder.class, timeoutMillis=10000)
	@FDiNetWebInfo(path="/getMessage")
	public void getMessage(StringHolder input, TypedResponseHandler<StringHolder> response);
	
}
