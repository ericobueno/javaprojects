package com.firstdata.fdinet.services;

import fdinet.core.StringHolder;
import fdinet.core.TypedResponseHandler;

public class FdiHelloServiceImpl extends FdiHelloService {

	public FdiHelloServiceImpl() {
		super("fdihello");
	}
	
	public void getMessage(StringHolder request, TypedResponseHandler<StringHolder> handler) {
		
		System.out.println("Got this from the caller : " + request.getValue());
		
		handler.handleResponse(new StringHolder(System.currentTimeMillis() + " " + request.getValue()));
	}
}
