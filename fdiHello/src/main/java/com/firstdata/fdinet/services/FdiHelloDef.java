package com.firstdata.fdinet.services;

import fdinet.core.*;
import fdinet.core.annotations.*;
import fdinet.core.entitlements.UserType;
/*
* GENERATED CODE, DO NOT TOUCH !
*/
@FDiNetService(version=1, serviceType="FdiHello")
@FDiNetWebInfo(path="fdihello", methods={WebDef.WebMethod.POST,})
@Description("")

public interface FdiHelloDef {


	@FDiNetWebInfo(path="/getMessage", pathTemplate="", methods={WebDef.WebMethod.POST,})
	@JSON(inputSchemaFile=".json", outputSchemaFile=".json") 
	@Description("")
	@FDiNetServiceAPI_RR(timeoutMillis=10000, responseType=fdinet.core.StringHolder.class)
	void getMessage(fdinet.core.StringHolder request, TypedResponseHandler<fdinet.core.StringHolder> handler) throws FDiNetException;

}
