package com.firstdata.fdinet.services;

import fdinet.core.*;
import fdinet.core.TypeSerializer.EncodingType;

/*
* GENERATED CODE, DO NOT TOUCH !
*/
public final class FdiHelloProxy extends FDiNetServiceProxyBase implements FdiHelloDef {

	public FdiHelloProxy(String serviceName) throws FDiNetException {
		this(serviceName, EncodingType.JSON, 1);
	}

	public FdiHelloProxy(String serviceName, EncodingType enc, int version) throws FDiNetException {
		super(serviceName, enc, version);
	}

	@Override
	public final void getMessage(fdinet.core.StringHolder request, TypedResponseHandler<fdinet.core.StringHolder> handler) {
		invokeRR("getMessage", request, 10000, handler, fdinet.core.StringHolder.class);
	}

}
