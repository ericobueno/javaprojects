package com.firstdata.fdinet.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.firstdata.fdinet.FdiHelloApplication;
import com.firstdata.fdinet.config.AnotherConfigClass;
import com.firstdata.fdinet.config.AppConfig;

@RestController
public class MainController {

	private static Logger logger = LoggerFactory.getLogger(MainController.class);

	@Autowired
	AppConfig appConfig;

	@Autowired
	AnotherConfigClass another;

	@RequestMapping(value = "/user", method = RequestMethod.GET)
	public String sayUser() {
		String greeting;

		logger.warn("Hit main controller /user");

		greeting = "User= " + appConfig.getUser() + " " + another.getTrustStore() + " " + System.currentTimeMillis();

		return greeting;
	}

	@RequestMapping(value = "/password", method = RequestMethod.GET)
	public String sayPassword() {
		String greeting;

		logger.info("Hit main controller /password");

		greeting = "Password= " + appConfig.getPassword() + " " + System.currentTimeMillis();

		return greeting;
	}

	@RequestMapping(value = "/showAll", method = RequestMethod.GET)
	public String showAllProperties() {
		String properties;

		logger.warn("Hit main controller /showAllProperties");

		properties = "@ " +  System.currentTimeMillis() + "<br />" + appConfig.toString() + "<br />" + another.toString();

//		logger.info(properties);
		
		return properties;
	}
}
