package com.firstdata.fdinet;

import com.firstdata.fdinet.services.FdiHelloProxy;

import fdinet.core.StringHolder;
import fdinet.core.TypedResponseHandler;

public class FdiHelloClient {
	
	public static void main(String[] args) throws Exception {
		
		FdiHelloProxy proxy = new FdiHelloProxy("transarmor.fdihello");
		
		proxy.getMessage(new StringHolder("Some Message"), new TypedResponseHandler<StringHolder>() {
			
			public void handleResponse(StringHolder e) {
				System.out.println("Got response " + e);
			}

			public void handleError(int statusCode, String text) {
				
				System.err.println("Got Error " + statusCode + " " + text);
				
			}
		});
	}
}
