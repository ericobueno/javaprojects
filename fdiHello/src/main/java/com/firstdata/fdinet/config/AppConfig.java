package com.firstdata.fdinet.config;

import java.util.Properties;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class AppConfig {
	
	private static Logger logger = LoggerFactory.getLogger(AppConfig.class);
	
	@Value("${fdinet.user}")
	private String user;

	@Value("${fdinet.password}")
	private String password;

	@Value("${fdinet.url}")
	private String url;
	
	@Value("${fdinet.trustStore}")
	private String trustStore;
	
	@Value("${fdinet.trustStorePassword}")
	private String trustStorePassword;
	

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getTrusStore() {
		return trustStore;
	}

	public void setTrusStore(String trusStore) {
		this.trustStore = trusStore;
	}

	public String getTrustStorePassword() {
		return trustStorePassword;
	}

	public void setTrustStorePassword(String trustStorePassword) {
		this.trustStorePassword = trustStorePassword;
	}

	@PostConstruct
	public void appSetEnv() {
		
			
		logger.info("Bueno - appSetEnv called @PostConstruct");
		logger.info("url=" + url);
		logger.info("user=" + user);
		logger.info("password=" + password);
		logger.info("turstStore=" + trustStore);
		logger.info("turstStorePassword=" + trustStorePassword);
		
		Properties props = System.getProperties();
		
		props.setProperty("fdinet.coreUrl", url);
		props.setProperty("fdinet.user", user);
		props.setProperty("fdinet.password",  password);
		props.setProperty("javax.net.ssl.trustStore", trustStore);
		props.setProperty("javax.net.ssl.trustStorePassword", trustStorePassword);
		
		//props.setProperty("javax.net.ssl.keystore", "c:/users/ebueno/fdinet/ssl.keystore");
		//props.setProperty("javax.net.ssl.keyStorePassword", "P4ssw0rd");
	}

	@Override
	public String toString() {
		return "AppConfig [user=" + user + ", password=" + password + ", url=" + url + ", trustStore=" + trustStore
				+ ", trustStorePassword=" + trustStorePassword + "]";
	}
}
