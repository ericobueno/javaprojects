package com.firstdata.fdinet.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix="fdinet")
public class AnotherConfigClass {
	
	private String user;
	private String password;
	private String url;
	private String trustStore;
	private String trustStorePassword;
	
	public AnotherConfigClass() {
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getTrustStore() {
		return trustStore;
	}

	public void setTrustStore(String trustStore) {
		this.trustStore = trustStore;
	}

	public String getTrustStorePassword() {
		return trustStorePassword;
	}

	public void setTrustStorePassword(String trustStorePassword) {
		this.trustStorePassword = trustStorePassword;
	}

	@Override
	public String toString() {
		return "AnotherConfigClass [user=" + user + ", password=" + password + ", url=" + url + ", trustStore="
				+ trustStore + ", trustStorePassword=" + trustStorePassword + "]";
	}
}
