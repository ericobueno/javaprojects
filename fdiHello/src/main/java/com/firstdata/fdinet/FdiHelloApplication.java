package com.firstdata.fdinet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ImportResource;

import fdinet.core.FDiNetServiceGroup;

@SpringBootApplication
@ImportResource({ "classpath*:Services.xml" })
@ComponentScan("com.firstdata.fdinet")
public class FdiHelloApplication {

	private static Logger logger = LoggerFactory.getLogger(FdiHelloApplication.class);

	public static void main(String[] args) {

		ApplicationContext context = SpringApplication.run(FdiHelloApplication.class, args);
		
		try {
			logger.info("Bueno-Initialize FDINET");
			FDiNetServiceGroup.initServices((FDiNetServiceGroup.Config) context.getBean("svcGroup"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
