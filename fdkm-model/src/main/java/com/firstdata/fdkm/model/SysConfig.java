package com.firstdata.fdkm.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the SYS_CONFIG database table.
 * 
 */
@Entity
@Table(name="SYS_CONFIG")
@NamedQuery(name="SysConfig.findAll", query="SELECT s FROM SysConfig s")
public class SysConfig implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="CFG_KEY")
	private String cfgKey;

	@Column(name="CFG_VALUE")
	private String cfgValue;

	public SysConfig() {
	}

	public String getCfgKey() {
		return this.cfgKey;
	}

	public void setCfgKey(String cfgKey) {
		this.cfgKey = cfgKey;
	}

	public String getCfgValue() {
		return this.cfgValue;
	}

	public void setCfgValue(String cfgValue) {
		this.cfgValue = cfgValue;
	}

}