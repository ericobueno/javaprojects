package com.firstdata.fdkm.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonManagedReference;


/**
 * The persistent class for the SECURITY_OBJECT database table.
 * 
 */
@Entity
@Table(name="SECURITY_OBJECT")
@NamedQuery(name="SecurityObject.findAll", query="SELECT s FROM SecurityObject s")
public class SecurityObject implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="SCY_OBJ_ID")
	private long scyObjId;

	@Temporal(TemporalType.DATE)
	@Column(name="COMPROMISED_DATE")
	private Date compromisedDate;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_DATE")
	private Date createdDate;

	@Temporal(TemporalType.DATE)
	@Column(name="DESTROYED_DATE")
	private Date destroyedDate;

	@Temporal(TemporalType.DATE)
	@Column(name="END_DATE")
	private Date endDate;

	@Lob
	@Column(name="OBJ_DATA")
	private String objData;

	@Column(name="SCY_OBJ_ALIAS")
	private String scyObjAlias;

	@Temporal(TemporalType.DATE)
	@Column(name="START_DATE")
	private Date startDate;

	private String status;

	//bi-directional many-to-one association to AuditLog
	@OneToMany(mappedBy="securityObject")
	private List<AuditLog> auditLogs;

	//bi-directional many-to-one association to SecurityClass
	@JsonManagedReference
//	@JsonIgnore
	@ManyToOne
	@JoinColumn(name="SCY_CLASS_ID")
	private SecurityClass securityClass;

	public SecurityObject() {
	}

	public long getScyObjId() {
		return this.scyObjId;
	}

	public void setScyObjId(long scyObjId) {
		this.scyObjId = scyObjId;
	}

	public Date getCompromisedDate() {
		return this.compromisedDate;
	}

	public void setCompromisedDate(Date compromisedDate) {
		this.compromisedDate = compromisedDate;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getDestroyedDate() {
		return this.destroyedDate;
	}

	public void setDestroyedDate(Date destroyedDate) {
		this.destroyedDate = destroyedDate;
	}

	public Date getEndDate() {
		return this.endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getObjData() {
		return this.objData;
	}

	public void setObjData(String objData) {
		this.objData = objData;
	}

	public String getScyObjAlias() {
		return this.scyObjAlias;
	}

	public void setScyObjAlias(String scyObjAlias) {
		this.scyObjAlias = scyObjAlias;
	}

	public Date getStartDate() {
		return this.startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<AuditLog> getAuditLogs() {
		return this.auditLogs;
	}

	public void setAuditLogs(List<AuditLog> auditLogs) {
		this.auditLogs = auditLogs;
	}

	public AuditLog addAuditLog(AuditLog auditLog) {
		getAuditLogs().add(auditLog);
		auditLog.setSecurityObject(this);

		return auditLog;
	}

	public AuditLog removeAuditLog(AuditLog auditLog) {
		getAuditLogs().remove(auditLog);
		auditLog.setSecurityObject(null);

		return auditLog;
	}

	public SecurityClass getSecurityClass() {
		return this.securityClass;
	}

	public void setSecurityClass(SecurityClass securityClass) {
		this.securityClass = securityClass;
	}

	@Override
	public String toString() {
		return "SecurityObject [scyObjId=" + scyObjId + ", createdDate=" + createdDate + ", objData=" + objData
				+ ", scyObjAlias=" + scyObjAlias + ", startDate=" + startDate + ", status=" + status + ", auditLogs="
				+ auditLogs + "]";
	}
	
	

}