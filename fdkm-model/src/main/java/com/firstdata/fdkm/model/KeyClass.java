package com.firstdata.fdkm.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;


/**
 * The persistent class for the KEY_CLASS database table.
 * 
 */
@Entity
@Table(name="KEY_CLASS")
@NamedQuery(name="KeyClass.findAll", query="SELECT k FROM KeyClass k")
public class KeyClass implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="SCY_CLASS_ID")
	private long scyClassId;

	private String algorithm;

	@Column(name="AUTO_GENERATE")
	private String autoGenerate;

	@Column(name="CIPHER_MODE")
	private String cipherMode;

	@Column(name="KEY_SIZE")
	private int keySize;

	//bi-directional one-to-one association to SecurityClass
	@JsonIgnore
	@OneToOne
	@JoinColumn(name="SCY_CLASS_ID")
	private SecurityClass securityClass;

	public KeyClass() {
	}

	public long getScyClassId() {
		return this.scyClassId;
	}

	public void setScyClassId(long scyClassId) {
		this.scyClassId = scyClassId;
	}

	public String getAlgorithm() {
		return this.algorithm;
	}

	public void setAlgorithm(String algorithm) {
		this.algorithm = algorithm;
	}

	public String getAutoGenerate() {
		return this.autoGenerate;
	}

	public void setAutoGenerate(String autoGenerate) {
		this.autoGenerate = autoGenerate;
	}

	public String getCipherMode() {
		return this.cipherMode;
	}

	public void setCipherMode(String cipherMode) {
		this.cipherMode = cipherMode;
	}

	public int getKeySize() {
		return this.keySize;
	}

	public void setKeySize(int keySize) {
		this.keySize = keySize;
	}

	public SecurityClass getSecurityClass() {
		return this.securityClass;
	}

	public void setSecurityClass(SecurityClass securityClass) {
		this.securityClass = securityClass;
	}

	@Override
	public String toString() {
		return "KeyClass [scyClassId=" + scyClassId + ", algorithm=" + algorithm + ", autoGenerate=" + autoGenerate
				+ ", cipherMode=" + cipherMode + ", keySize=" + keySize + "]";
	}

	
}