package com.firstdata.fdkm.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the AUDIT_LOG database table.
 * 
 */
@Entity
@Table(name="AUDIT_LOG")
@NamedQuery(name="AuditLog.findAll", query="SELECT a FROM AuditLog a")
public class AuditLog implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="AUDIT_ID")
	private long auditId;

	@Column(name="AUDIT_MSG")
	private String auditMsg;

	@Temporal(TemporalType.DATE)
	@Column(name="ENTRY_DATE")
	private Date entryDate;

	//bi-directional many-to-one association to SecurityObject
	@ManyToOne
	@JoinColumn(name="SCY_OBJ_ID")
	private SecurityObject securityObject;

	public AuditLog() {
	}

	public long getAuditId() {
		return this.auditId;
	}

	public void setAuditId(long auditId) {
		this.auditId = auditId;
	}

	public String getAuditMsg() {
		return this.auditMsg;
	}

	public void setAuditMsg(String auditMsg) {
		this.auditMsg = auditMsg;
	}

	public Date getEntryDate() {
		return this.entryDate;
	}

	public void setEntryDate(Date entryDate) {
		this.entryDate = entryDate;
	}

	public SecurityObject getSecurityObject() {
		return this.securityObject;
	}

	public void setSecurityObject(SecurityObject securityObject) {
		this.securityObject = securityObject;
	}

}