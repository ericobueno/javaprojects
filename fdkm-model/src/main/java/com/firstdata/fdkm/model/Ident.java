package com.firstdata.fdkm.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the IDENT database table.
 * 
 */
@Entity
@NamedQuery(name="Ident.findAll", query="SELECT i FROM Ident i")
public class Ident implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="IDENT_ID")
	private long identId;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_DATE")
	private Date createdDate;

	@Column(name="IDENT_NAME")
	private String identName;

	//bi-directional many-to-one association to Client
	@OneToMany(mappedBy="ident")
	private List<Client> clients;

	//bi-directional many-to-one association to ClientCert
	@OneToMany(mappedBy="ident")
	private List<ClientCert> clientCerts;

	public Ident() {
	}

	public long getIdentId() {
		return this.identId;
	}

	public void setIdentId(long identId) {
		this.identId = identId;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getIdentName() {
		return this.identName;
	}

	public void setIdentName(String identName) {
		this.identName = identName;
	}

	public List<Client> getClients() {
		return this.clients;
	}

	public void setClients(List<Client> clients) {
		this.clients = clients;
	}

	public Client addClient(Client client) {
		getClients().add(client);
		client.setIdent(this);

		return client;
	}

	public Client removeClient(Client client) {
		getClients().remove(client);
		client.setIdent(null);

		return client;
	}

	public List<ClientCert> getClientCerts() {
		return this.clientCerts;
	}

	public void setClientCerts(List<ClientCert> clientCerts) {
		this.clientCerts = clientCerts;
	}

	public ClientCert addClientCert(ClientCert clientCert) {
		getClientCerts().add(clientCert);
		clientCert.setIdent(this);

		return clientCert;
	}

	public ClientCert removeClientCert(ClientCert clientCert) {
		getClientCerts().remove(clientCert);
		clientCert.setIdent(null);

		return clientCert;
	}

}