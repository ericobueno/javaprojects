package com.firstdata.fdkm.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;


/**
 * The persistent class for the SECURITY_CLASS database table.
 * 
 */
@Entity
@Table(name="SECURITY_CLASS")
@NamedQuery(name="SecurityClass.findAll", query="SELECT s FROM SecurityClass s")
public class SecurityClass implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="SCY_CLASS_ID")
	private long scyClassId;

	private String description;

	private int duration;

	@Column(name="SCY_CLASS_NAME")
	private String scyClassName;

	@Column(name="SCY_CLASS_TYPE")
	private String scyClassType;

	//bi-directional one-to-one association to KeyClass
	@OneToOne(mappedBy="securityClass")
	private KeyClass keyClass;

	//bi-directional many-to-one association to SecurityObject
	//@JsonIgnore
	@JsonBackReference
	@OneToMany(mappedBy="securityClass")
	private List<SecurityObject> securityObjects;

	public SecurityClass() {
	}

	public long getScyClassId() {
		return this.scyClassId;
	}

	public void setScyClassId(long scyClassId) {
		this.scyClassId = scyClassId;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getDuration() {
		return this.duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public String getScyClassName() {
		return this.scyClassName;
	}

	public void setScyClassName(String scyClassName) {
		this.scyClassName = scyClassName;
	}

	public String getScyClassType() {
		return this.scyClassType;
	}

	public void setScyClassType(String scyClassType) {
		this.scyClassType = scyClassType;
	}

	public KeyClass getKeyClass() {
		return this.keyClass;
	}

	public void setKeyClass(KeyClass keyClass) {
		this.keyClass = keyClass;
	}

	public List<SecurityObject> getSecurityObjects() {
		return this.securityObjects;
	}

	public void setSecurityObjects(List<SecurityObject> securityObjects) {
		this.securityObjects = securityObjects;
	}

	public SecurityObject addSecurityObject(SecurityObject securityObject) {
		getSecurityObjects().add(securityObject);
		securityObject.setSecurityClass(this);

		return securityObject;
	}

	public SecurityObject removeSecurityObject(SecurityObject securityObject) {
		getSecurityObjects().remove(securityObject);
		securityObject.setSecurityClass(null);

		return securityObject;
	}

	@Override
	public String toString() {
		return "SecurityClass [scyClassId=" + scyClassId + ", description=" + description + ", duration=" + duration
				+ ", scyClassName=" + scyClassName + ", scyClassType=" + scyClassType + "]";
	}

	
}