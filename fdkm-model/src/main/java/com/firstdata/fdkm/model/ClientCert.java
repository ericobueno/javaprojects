package com.firstdata.fdkm.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the CLIENT_CERT database table.
 * 
 */
@Entity
@Table(name="CLIENT_CERT")
@NamedQuery(name="ClientCert.findAll", query="SELECT c FROM ClientCert c")
public class ClientCert implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="CLIENT_CERT_ID")
	private long clientCertId;

	@Column(name="CLIENT_CERT_CN")
	private String clientCertCn;

	@Temporal(TemporalType.DATE)
	@Column(name="CLIENT_CERT_EXP_DATE")
	private Date clientCertExpDate;

	@Column(name="CLIENT_CERT_SN")
	private String clientCertSn;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_DATE")
	private Date createdDate;

	//bi-directional many-to-one association to Ident
	@ManyToOne
	@JoinColumn(name="IDENT_ID")
	private Ident ident;

	public ClientCert() {
	}

	public long getClientCertId() {
		return this.clientCertId;
	}

	public void setClientCertId(long clientCertId) {
		this.clientCertId = clientCertId;
	}

	public String getClientCertCn() {
		return this.clientCertCn;
	}

	public void setClientCertCn(String clientCertCn) {
		this.clientCertCn = clientCertCn;
	}

	public Date getClientCertExpDate() {
		return this.clientCertExpDate;
	}

	public void setClientCertExpDate(Date clientCertExpDate) {
		this.clientCertExpDate = clientCertExpDate;
	}

	public String getClientCertSn() {
		return this.clientCertSn;
	}

	public void setClientCertSn(String clientCertSn) {
		this.clientCertSn = clientCertSn;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Ident getIdent() {
		return this.ident;
	}

	public void setIdent(Ident ident) {
		this.ident = ident;
	}

}