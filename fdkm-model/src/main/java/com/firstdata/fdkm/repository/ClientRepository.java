package com.firstdata.fdkm.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.firstdata.fdkm.model.Client;

@RepositoryRestResource
public interface ClientRepository extends CrudRepository<Client, Long> {

	Client findByClientId(@Param("q") long id);

	List<Client> findByClientName(@Param("q") String name);

}
