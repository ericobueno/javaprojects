package com.firstdata.fdkm.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.firstdata.fdkm.model.SecurityObject;

@RepositoryRestResource
public interface SecurityObjectRepository extends CrudRepository<SecurityObject, Long> {

	List<SecurityObject> findBySecurityClassScyClassName(@Param("n") String name);
	
	List<SecurityObject> findByStatusAndSecurityClassScyClassName(@Param("s") String status, @Param("n") String name);
	
	@Query("SELECT o FROM SecurityObject o WHERE o.status = UPPER(:status)")
	List<SecurityObject> findAllActiveScyObjects(@Param("status") String status );
	
	// Key related queries
	
	@Query("SELECT o FROM SecurityObject o WHERE o.securityClass.scyClassType = 'KEY'")
	List<SecurityObject> findAllKeys();
	
	@Query("SELECT o FROM SecurityObject o WHERE o.securityClass.scyClassName = :className")
	List<SecurityObject> findKeysBySecurityClassName(@Param("className") String className);
	
	@Query("SELECT o FROM SecurityObject o WHERE o.securityClass.scyClassName = :className AND o.status = 'ACTIVE'")
	List<SecurityObject> findActiveKey(@Param("className") String className );
	
	@Query("SELECT o FROM SecurityObject o WHERE o.scyObjId = :keyId")
	List<SecurityObject> findKeyById( @Param("keyId") long keyId );
	

	// Certificates query
	
	@Query("SELECT o FROM SecurityObject o WHERE o.securityClass.scyClassType = 'CERT'")
	List<SecurityObject> findAllCertificates();
	
	@Query("SELECT o FROM SecurityObject o WHERE o.scyObjAlias = :certAlias")
	List<SecurityObject> findCert(@Param("certAlias") String certAlias);
}
