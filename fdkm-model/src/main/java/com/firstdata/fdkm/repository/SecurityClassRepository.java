package com.firstdata.fdkm.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.firstdata.fdkm.model.SecurityClass;

@RepositoryRestResource
public interface SecurityClassRepository extends CrudRepository<SecurityClass, Long> {
	
	List<SecurityClass> findByScyClassName(@Param("q") String name);

}
