package com.firstdata.fdkm.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.firstdata.fdkm.model.ClientCert;

@RepositoryRestResource
public interface ClientCertRepository extends CrudRepository<ClientCert, Long> {

}
