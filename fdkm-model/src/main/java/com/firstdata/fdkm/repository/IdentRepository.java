package com.firstdata.fdkm.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.firstdata.fdkm.model.Ident;

@RepositoryRestResource
public interface IdentRepository extends CrudRepository<Ident, Long> {

	void deleteByIdentName(@Param("q") String Name);
	
	Ident findByIdentId(@Param("q") long id);
	
	List<Ident> findByIdentName(@Param("q") String name);
	
	List<Ident> findByIdentNameContains(@Param("q") String name);
	
}
