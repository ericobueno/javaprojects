package com.firstdata.fdkm.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.firstdata.fdkm.model.AuditLog;

@RepositoryRestResource
public interface AuditLogRepository extends CrudRepository<AuditLog, Long> {

}
