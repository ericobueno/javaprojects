package com.firstdata.fdkm;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;

import com.firstdata.fdkm.model.Client;
import com.firstdata.fdkm.model.Ident;
import com.firstdata.fdkm.model.KeyClass;
import com.firstdata.fdkm.model.SecurityClass;
import com.firstdata.fdkm.model.SecurityObject;
import com.firstdata.fdkm.repository.AuditLogRepository;
import com.firstdata.fdkm.repository.ClientRepository;
import com.firstdata.fdkm.repository.IdentRepository;
import com.firstdata.fdkm.repository.KeyClassRepository;
import com.firstdata.fdkm.repository.SecurityClassRepository;
import com.firstdata.fdkm.repository.SecurityObjectRepository;

@SpringBootApplication
// @PropertySource("file:/local/config/fdkm-model.properties")
@PropertySource("file:${configFile}")
public class FdkmModelApplication {

	@Autowired
	ClientRepository clientRepository;
	@Autowired
	IdentRepository identRepository;
	@Autowired
	SecurityClassRepository securityClassRepository;
	@Autowired
	KeyClassRepository keyClassRepository;
	@Autowired
	SecurityObjectRepository securityObjectRepository;
	@Autowired
	AuditLogRepository auditLogRepository;

	Logger logger = LoggerFactory.getLogger(this.getClass());

	public static void main(String[] args) {

		// ApplicationContext ctx = SpringApplication.run(FdkmModelApplication.class,
		// args);

		SpringApplication springApplication = new SpringApplication(FdkmModelApplication.class);

		ConfigurableApplicationContext ctx = springApplication.run(args);

		String[] beanDefinitionNames = ctx.getBeanDefinitionNames();

		for (String beanName : beanDefinitionNames) {

			System.out.println("beanName= " + beanName);

		}

	}

	public void initDb() {

		logger.warn("loading database..");

		logger.info("Deleting Client");
		clientRepository.deleteAll();

		logger.info("Deleting Ident");
		identRepository.deleteAll();

		Ident id1 = new Ident();
		id1.setIdentName("FDC-NA");

		logger.warn("Before identity {}", id1);
		id1 = identRepository.save(id1);

		List<Ident> ids = identRepository.findByIdentName("FDC-NA");
		Ident theId = ids.get(0);

		logger.warn("Added identity {}", theId);

		Client client = new Client();

		client.setClientName("token-vault-1");
		client.setClientStatus("ACTIVE");
		client.setHostName("n1pvap1066");
		client.setClientIp("192.168.1.3");
		client.setIdent(theId);
		clientRepository.save(client);

		client.setClientName("token-vault-2");
		client.setClientStatus("ACTIVE");
		client.setHostName("n1pvap1067");
		client.setClientIp("192.168.1.5");
		client.setIdent(theId);
		clientRepository.save(client);

		// /*
		// * logger.warn("record count: {}", ir.count()); Iterable<Identity> findAll =
		// * ir.findAll();
		// *
		// * for (Identity identity : findAll) { logger.info(identity.toString()); }
		// * ir.findAll().forEach(x -> logger.warn(x.toString()));
		// */

		logger.info("Deleting AuditLog");
		auditLogRepository.deleteAll();

		logger.info("Deleting SecurityObject");
		securityObjectRepository.deleteAll();

		logger.info("Deleting KeyClass");
		keyClassRepository.deleteAll();

		logger.info("Deleting SecurityClass");
		securityClassRepository.deleteAll();

		SecurityClass securityClass = new SecurityClass();

		securityClass.setScyClassName("fdsecure-stm-kc-encrypt");
		securityClass.setScyClassType("KEY");
		securityClass.setDuration(730);
		securityClass.setDescription("PAN encryption key");

		securityClass = securityClassRepository.save(securityClass);

		List<SecurityClass> scs = securityClassRepository.findByScyClassName("fdsecure-stm-kc-encrypt");
		logger.info("Found {} Security Class", scs.size());
		securityClass = scs.get(0);

		logger.warn("PAN encryption Security class : {}", securityClass.toString());

		KeyClass keyClass = new KeyClass();
		keyClass.setScyClassId(securityClass.getScyClassId());
		keyClass.setAlgorithm("AES");
		keyClass.setKeySize(256);
		keyClass.setCipherMode("CBC");
		keyClass.setAutoGenerate("Y");
		keyClass.setSecurityClass(securityClass);

		keyClass = keyClassRepository.save(keyClass);
		/*
		 * Create Keys
		 * 
		 */
		Date today = new Date();
		Calendar c = Calendar.getInstance();

		SecurityObject securityObject = new SecurityObject();

		long objId = ThreadLocalRandom.current().nextLong(1000, 999999999);
		securityObject.setScyObjId(objId);
		securityObject.setScyObjAlias("pan1");
		securityObject.setStatus("EXPIRED");
		securityObject.setObjData("AAAAAAAAAAAAAAAAAAAAAAA");
		securityObject.setStartDate(new Date());

		c.setTime(today);
		c.add(Calendar.DATE, securityClass.getDuration());

		securityObject.setEndDate(c.getTime());

		securityObject.setSecurityClass(securityClass);
		securityObject = securityObjectRepository.save(securityObject);

		objId = ThreadLocalRandom.current().nextLong(1000, 999999999);
		securityObject.setScyObjId(objId);
		securityObject.setScyObjAlias("pan2");
		securityObject.setStatus("EXPIRED");
		securityObject.setObjData("BBBBBBBBBBBBBBBBBBB");
		securityObject.setStartDate(new Date());

		c.setTime(today);
		c.add(Calendar.DATE, securityClass.getDuration());
		securityObject.setEndDate(c.getTime());

		securityObject.setSecurityClass(securityClass);
		securityObject = securityObjectRepository.save(securityObject);

		objId = ThreadLocalRandom.current().nextLong(1000, 999999999);
		securityObject.setScyObjId(objId);
		securityObject.setScyObjAlias("pan3");
		securityObject.setStatus("COMPROMISED");
		securityObject.setObjData("KKKKKKKKKKKKKKKKKKKK");
		securityObject.setStartDate(new Date());
		
		c.setTime(today);
		c.add(Calendar.DATE, securityClass.getDuration());
		securityObject.setEndDate(c.getTime());
		
		securityObject.setCompromisedDate(new Date());
		securityObject.setSecurityClass(securityClass);
		securityObject = securityObjectRepository.save(securityObject);

		objId = ThreadLocalRandom.current().nextLong(1000, 999999999);
		securityObject.setScyObjId(objId);
		securityObject.setScyObjAlias("pan4");
		securityObject.setStatus("ACTIVE");
		securityObject.setObjData("XXXXXXXXXXXXXXXXXXXXXXXXX");
		securityObject.setStartDate(new Date());
		
		c.setTime(today);
		c.add(Calendar.DATE, securityClass.getDuration());
		securityObject.setEndDate(c.getTime());
		
		securityObject.setCompromisedDate(null);
		securityObject.setSecurityClass(securityClass);
		securityObject = securityObjectRepository.save(securityObject);

		/*
		 * Add HMAC security class
		 * 
		 * 
		 * 
		 */

		logger.info("Adding HMAC SecurityClass");

		securityClass.setScyClassId(0); // Allow oracle to populate
		securityClass.setScyClassName("fdsecure-stm-kc-hmac");
		securityClass.setScyClassType("KEY");
		securityClass.setDuration(0); // Infinite
		securityClass.setDescription("PAN HMAC key");

		securityClass = securityClassRepository.save(securityClass);

		scs = securityClassRepository.findByScyClassName("fdsecure-stm-kc-hmac");
		logger.info("Found {} HMAC Security Class", scs.size());
		securityClass = scs.get(0);

		logger.warn("HMAC Security class : {}", securityClass.toString());

		logger.info("Adding HMAC KeyClass");
		keyClass.setScyClassId(securityClass.getScyClassId());
		keyClass.setAlgorithm("AES");
		keyClass.setKeySize(256);
		keyClass.setCipherMode("-");
		keyClass.setAutoGenerate("Y");
		keyClass.setSecurityClass(securityClass);

		keyClass = keyClassRepository.save(keyClass);

		objId = ThreadLocalRandom.current().nextLong(1000, 999999999);
		securityObject.setScyObjId(objId);
		securityObject.setScyObjAlias("hmac1");
		securityObject.setStatus("ACTIVE");
		securityObject.setObjData("HMACHMACHHHHHHHHHHHHHHHHHHHHHHH");
		securityObject.setStartDate(new Date());
		
		c.setTime(today);
		c.add(Calendar.DATE, securityClass.getDuration());
		securityObject.setEndDate(c.getTime());	
		
		securityObject.setCompromisedDate(null);
		securityObject.setSecurityClass(securityClass);
		securityObject = securityObjectRepository.save(securityObject);

		logger.info("Adding Certificate SecurityClass");

		SecurityClass certClass = new SecurityClass();

		certClass.setScyClassId(0); // Allow oracle to populate
		certClass.setScyClassName("RSACACerts");
		certClass.setScyClassType("CERT");
		certClass.setDuration(730); // 2 years
		certClass.setDescription("Signed Key Certificates");

		certClass = securityClassRepository.save(certClass);

		scs = securityClassRepository.findByScyClassName("RSACACerts");
		logger.info("Found {} Security Class", scs.size());
		certClass = scs.get(0);

		SecurityObject cert = new SecurityObject();

		objId = ThreadLocalRandom.current().nextLong(1000, 999999999);
		cert.setScyObjId(objId);
		cert.setScyObjAlias("TACAT166257982270");
		cert.setStatus("ACTIVE");
		cert.setObjData("# BEGIN PUBLIC CERTIFICATE sdfasdfasd ");
		cert.setStartDate(new Date());

		c.setTime(today);
		c.add(Calendar.DATE, certClass.getDuration());
		cert.setEndDate(c.getTime());

		cert.setCompromisedDate(null);
		cert.setSecurityClass(certClass);

		cert = securityObjectRepository.save(cert);
	}

	@Bean
	CommandLineRunner loadDatabase(IdentRepository identRepository, ClientRepository clientRepository,
			SecurityClassRepository securityClassRepository, KeyClassRepository keyClassRepository,
			SecurityObjectRepository securityObjectRepository, AuditLogRepository auditLogRepository) {

		return args -> {

//			initDb();

		};
	}

}
