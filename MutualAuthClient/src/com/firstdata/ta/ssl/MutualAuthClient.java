package com.firstdata.ta.ssl;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.Principal;

import javax.net.SocketFactory;
import javax.net.ssl.HandshakeCompletedEvent;
import javax.net.ssl.HandshakeCompletedListener;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

public class MutualAuthClient {

	public static void main(String[] args) throws IOException {
		
		System.setProperty("javax.net.ssl.trustStore", "/Users/ebueno/certs/truststore.jks");
		System.setProperty("javax.net.ssl.trustStorePassword", "changeit");
		
//		System.setProperty("javax.net.ssl.keyStore", "/Users/ebueno/certs/transarmor.1dc.com.p12");
//		System.setProperty("javax.net.ssl.keyStorePassword", "Huvgoi!69");
		
		System.setProperty("javax.net.ssl.keyStore", "/Users/ebueno/certs/client.p12");
		System.setProperty("javax.net.ssl.keyStorePassword", "changeit");
		
		/* Same as    -Djavax.net.debug=ssl */ 
		System.setProperty("javax.net.debug", "ssl");
		
		
		SSLSocketFactory fac = (SSLSocketFactory)SSLSocketFactory.getDefault();
		
		SSLSocket socket = (SSLSocket) fac.createSocket("localhost", 8443 );
		
		socket.addHandshakeCompletedListener(new MyHandshakeCompletedListener());
		
		doProtocol( socket );
		
		socket.close();
	}
	
	
	private static void doProtocol(SSLSocket socket) throws IOException {
		
		String hello = "Hi I am the client\n!";
		
		InputStream in = socket.getInputStream();
		OutputStream out = socket.getOutputStream();
		
		out.write(hello.getBytes());
		
		int ch = 0;
		
		while( (ch = in.read()) != '!' ) {
			System.out.print((char)ch);
		}
		
		System.out.print((char)ch);	
		
	}
}
 
class MyHandshakeCompletedListener implements HandshakeCompletedListener {

	@Override
	public void handshakeCompleted(HandshakeCompletedEvent event) {
		System.out.println("Handshake completed");
		
		try {
			Principal peerPrincipal = event.getPeerPrincipal();
			System.out.println("Principal: " + peerPrincipal.getName());
		} catch (SSLPeerUnverifiedException e) {
			e.printStackTrace();
		}
	}
	
	
}
