package com.firstdata.courses.services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.springframework.stereotype.Service;
import com.firstdata.courses.resources.Topic;

@Service
public class TopicService {
	
	private List<Topic >topics = new ArrayList<> (Arrays.asList(
			new Topic("spring", "Spring Framework", "Teachs everythig about Spring framework"),
			new Topic("java", "Core Java", "Core JAva description"),
			new Topic("Angular", "Angular JS", "Angular JS description")));

	
	public List<Topic> getAllTopics() {
		return topics;
	}


	public Topic getTopic(String id) {
		
		Topic topic = (Topic) topics.stream().filter( t -> t.getId().equals(id)).findFirst().get();
		
		return topic;
	}


	public void addTopic(Topic topic) {
		topics.add(topic);
		
	}


	public void updateTopic(String id, Topic topic) {

		System.out.println("Updating topic=" + topic);
		for (int i = 0; i < topics.size(); i++) {
			Topic t = topics.get(i);
			
			if( t.getId().equals(id)) {
				topics.set(i, topic);
			}
			
		}
	}


	public void deleteTopic(String id) {
		
		topics.removeIf(t -> t.getId().equals(id));
		
	}

}
