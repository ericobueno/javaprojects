SELECT
    b.scy_obj_id,
    b.scy_obj_alias,
    b.status,
    b.created_date,
    b.obj_data,
    a.scy_class_name,
    a.scy_class_type,
    a.duration,
    a.description
FROM
    security_object b,
    security_class a
WHERE
    a.scy_class_id = b.scy_class_id