SELECT
    security_object.obj_name,
    security_object.obj_data,
    security_object.status,
    security_object.scy_obj_id,
    security_object.created_date,
    security_object.start_date,
    security_object.end_date,
    security_object.compromised_date,
    security_object.destroyed_date,
    security_class.type
FROM
    security_class
    INNER JOIN security_object ON security_class.scy_class_id = security_object.scy_class_id
WHERE
    security_object.status = 'ACTIVE'
    AND   security_class.name = 'FDENC'
    AND   security_class.type = 'KEY'