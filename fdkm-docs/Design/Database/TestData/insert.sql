INSERT INTO security_class (
    scy_class_id,
    scy_class_name,
    scy_class_type,
    duration,
    description
) VALUES (
    '111111',
    'fdsecure-stm-kc-encrypt',
    'KEY',
    '365',
    'AES Keys to encrypt credit card numbers- Valid for 1 year'
);


INSERT INTO key_class (
    scy_class_id,
    algorithm,
    key_size,
    cipher_mode,
    auto_generate
) VALUES (
    '111111',
    'AES',
    '256',
    'CBC',
    'Y'
);


INSERT INTO security_object (
    scy_obj_id,
    scy_obj_alias,
    scy_class_id,
    status,
    start_date,
    end_date,
    compromised_date,
    destroyed_date,
    obj_data
   
) VALUES (
   '3456789',
   '3456789',
   '111111',
   'ACTIVE',
   to_date('02/15/2018', 'MM/DD/YYYY'),
   to_date('02/15/2019', 'MM/DD/YYYY'),
   to_date('02/15/2099', 'MM/DD/YYYY'),
   to_date('02/15/2099', 'MM/DD/YYYY'),  
   'bla1 bla1 hkhJHuy88hIH8potf9juk'
);

INSERT INTO security_object (
    scy_obj_id,
    scy_obj_alias,
    scy_class_id,
    status,
    start_date,
    end_date,
    compromised_date,
    destroyed_date,
    obj_data
   
) VALUES (
   '3458790',
    '3458790',
   '111111',
   'EXPIRED',
   to_date('02/15/2018', 'MM/DD/YYYY'),
   to_date('02/15/2019', 'MM/DD/YYYY'),
   to_date('02/15/2099', 'MM/DD/YYYY'),
   to_date('02/15/2099', 'MM/DD/YYYY'),  
   'bla2 bla2 jk897wern8ynKhjy78gi5FdhT'
);




INSERT INTO security_class (
    scy_class_id,
    scy_class_name,
    scy_class_type,
    duration,
    description
) VALUES (
    '777777',
    'fdsecure-stm-kc-hmac',
    'KEY',
    '0',
    'HMAC Key to hash credit card numbers - Duration is infinite'
);


INSERT INTO key_class (
    scy_class_id,
    algorithm,
    key_size,
    cipher_mode,
    auto_generate
) VALUES (
    '777777',
    'HMAC-SHA256',
    '256',
    '-',
    'N'
);

INSERT INTO security_object (
    scy_obj_id,
    scy_obj_alias,
    scy_class_id,
    status,
    start_date,
    end_date,
    compromised_date,
    destroyed_date,
    obj_data
   
) VALUES (
   '8765468',
   '8765468',
   '777777',
   'ACTIVE',
   to_date('02/15/2018', 'MM/DD/YYYY'),
   to_date('02/15/2019', 'MM/DD/YYYY'),
   to_date('02/15/2099', 'MM/DD/YYYY'),
   to_date('02/15/2099', 'MM/DD/YYYY'),  
   'bla3 bla3 HMAC jk897wern8ynKhjy78gi5FdhT'
);

INSERT INTO security_class (
    scy_class_id,
    scy_class_name,
    scy_class_type,
    duration,
    description
) VALUES (
    '767676',
    'RSACACerts',
    'CERT',
    '730',
    'TransArmor signed key certificates - Duration 2 years'
);

INSERT INTO security_object (
    scy_obj_id,
    scy_obj_alias,
    scy_class_id,
    status,
    start_date,
    end_date,
    compromised_date,
    destroyed_date,
    obj_data
   
) VALUES (
   '76452190',
   'a1pvap026.1dc.com',
   '767676',
   'ACTIVE',
   to_date('02/15/2018', 'MM/DD/YYYY'),
   to_date('02/15/2019', 'MM/DD/YYYY'),
   to_date('02/15/2099', 'MM/DD/YYYY'),
   to_date('02/15/2099', 'MM/DD/YYYY'),
   'CN=a1pvap026.1dc.com'
);

INSERT INTO security_object (
    scy_obj_id,
    scy_obj_alias,
    scy_class_id,
    status,
    start_date,
    end_date,
    compromised_date,
    destroyed_date,
    obj_data
   
) VALUES (
   '983125',
   'a3pvap020.1dc.com',
   '767676',
   'ACTIVE',
   to_date('02/15/2018', 'MM/DD/YYYY'),
   to_date('02/15/2019', 'MM/DD/YYYY'),
   to_date('02/15/2099', 'MM/DD/YYYY'),
   to_date('02/15/2099', 'MM/DD/YYYY'),
   'CN=a3pvap020.1dc.com jklldf8h888sdfdsfgsdfg'
);



