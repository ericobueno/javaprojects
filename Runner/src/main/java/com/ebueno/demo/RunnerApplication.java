package com.ebueno.demo;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.core.annotation.AnnotationAwareOrderComparator;

@SpringBootApplication
public class RunnerApplication implements CommandLineRunner {

	public static void main(String[] args) throws Exception {

		ConfigurableApplicationContext ctx = SpringApplication.run(RunnerApplication.class, args);

		List<Object> runners = new ArrayList<Object>();

		runners.addAll(ctx.getBeansOfType(CommandLineRunner.class).values());

		System.out.println("----->Found " + runners.size() + " CommandRUnners");

		AnnotationAwareOrderComparator.sort(runners);

		for (Object runner : new LinkedHashSet<Object>(runners)) {

			if (runner instanceof CommandLineRunner) {

				CommandLineRunner myRunner = (CommandLineRunner) runner;

				String name = myRunner.getClass().getName();

				System.out.println("----->Running my runner. Class=" + name);

				myRunner.run(args);
			}
		}
	}

	@Bean("buenoBeanNotLoaddatabse")
	CommandLineRunner loadDatabase() {

		CommandLineRunner a = new CommandLineRunnerImpl();

		return a;
	}

	@Bean
	CommandLineRunner foo() {

		return args -> {
			System.out.println("----->Running my foo1.");
			System.out.println("----->Running my foo2.");
		};
	}

	@Bean
	CommandLineRunner bar() {

		CommandLineRunner runner;

		runner = args -> {
			System.out.println("----->Running my bar1.");
			System.out.println("----->Running my bar2.");
		};

		return runner;
	}

	@Override
	public void run(String... args) throws Exception {

		System.out.println("Is this a bean");

	}

}
