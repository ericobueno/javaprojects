package com.ebueno.EnumException;

/**
 *
 * @author Erico Bueno
 * @author Suresh Vermulapali
 *
 *
 */


public enum FdkmError {

	OK(0, "Success"),
	SYSTEM_ERROR(10, "System Error"),
	KEY_NOT_FOUND(20, "Key not Found"),
	CERT_NOT_FOUND(20, "Certificate not Found");

	private int errorCode;
	private String message;

	private FdkmError(int errorCode, String message) {
		this.errorCode = errorCode;
		this.message = message;
	}

	public int getErrorCode() {
		return errorCode;
	}

	public String getMessage() {
		return message;
	}
}
