package com.ebueno.EnumException;

public class FdkmException extends Exception {
	
	private static final long serialVersionUID = 4470295741559524731L;
	
	FdkmError fdkmError;

	public FdkmException(FdkmError fdkmError) {
				
		super(fdkmError.getMessage());
		
		this.fdkmError = fdkmError;
		
	}


	public FdkmError getFdkmError() {
		return fdkmError;
	}

}
