package com.ebueno.EnumException;


public class App {
	public static void main(String[] args) {
		FdkmError fdkmError = FdkmError.KEY_NOT_FOUND;

		System.out.println("Error = " + fdkmError);

		for (FdkmError error : FdkmError.values()) {
			System.out.println("ErrorCode = " + error.getErrorCode() + " Message=" + error.getMessage());
		}

		try {
			process("other");
		} catch (FdkmException e) {

			FdkmError error = e.getFdkmError();

			System.out.println("ErrorCode=" + error.getErrorCode() + " Message=" + error.getMessage());
			
		} catch (Exception e) {
			
			FdkmError error = FdkmError.SYSTEM_ERROR;
			
			System.out.println("ErrorCode=" + error.getErrorCode() + " Message=" + error.getMessage());
			
		}

		System.out.println("After try catch block");
	}

	public static void process(String action) throws Exception {

		boolean error = true;
		System.out.println("Processing before throw");

		switch (action) {

		case "throw":
			if (error)
				throw new FdkmException(FdkmError.KEY_NOT_FOUND);
			break;

		case "other":
			if (error)
				throw new Exception("Some other exception");
			break;

		default:
			System.out.println("Processing normally");
		}
	}
}
