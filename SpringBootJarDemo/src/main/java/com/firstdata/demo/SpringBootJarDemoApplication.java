package com.firstdata.demo;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.annotation.Order;

@SpringBootApplication
@Order(2)
public class SpringBootJarDemoApplication implements CommandLineRunner {

	@Value("${myProfile}")
	private String myProfile;

	public static void main(String[] args) {
		SpringApplication.run(SpringBootJarDemoApplication.class, args);

	}

	@Override
	public void run(String... args) throws Exception {

		System.out.println("Main Application Runner");
		System.out.println("myProfile=" + myProfile);

		int i = 0;

		for (String arg : args) {
			System.out.println("arg[" + i++ + "]=" + arg);
		}

	}
}
