package com.firstdata.demo;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;

@Configuration
public class AppConfig {
	
	@Bean
	@Order(1)
	public CommandLineRunner bueno() {
		
		return (args) -> System.out.println("Bueno runner");
	}
	
	
	@Bean
	@Order(3)
	public CommandLineRunner suresh() {
		
		return new CommandLineRunnerImpl();
		
	}
}

class CommandLineRunnerImpl implements CommandLineRunner {

	@Override
	public void run(String... arg0) throws Exception {
		System.out.println("Suresh's runner");
	}
}
