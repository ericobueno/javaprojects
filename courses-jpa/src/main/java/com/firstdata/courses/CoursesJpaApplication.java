package com.firstdata.courses;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

/*
 * 
{

    "id": "Java",
    "name": "Java Core",
    "description": "Java programming Description"
},
{
    "id": "Networking",
    "name": "Core Networking ",
    "description": "Networking basics Description"
}

*/

@SpringBootApplication
public class CoursesJpaApplication {
	
	Logger logger = LoggerFactory.getLogger(CoursesJpaApplication.class);

	public static void main(String[] args) {
		
		ApplicationContext ctx = SpringApplication.run(CoursesJpaApplication.class, args);
		
		HelloWorld h = ctx.getBean(HelloWorld.class);
		
		h.greet();
		
	}
//
//	@Bean
//	CommandLineRunner loadDatabase(TopicRepository tr) {
//
//		return args -> {
//
//			logger.debug("loading Courses database..");
//
//			tr.save(new Topic("Java", "Java Core", "Java programming Description"));
//			tr.save(new Topic("Networking", "Core Networking ", "Networking basics Description"));
//
//			logger.debug("record count: {}", tr.count());
//
//			tr.findAll().forEach(x -> logger.debug(x.toString()));
//		};
//
//	}
//
//	@Bean
//	DataSource datasource() throws SQLException {
//
//		OracleDataSource datasource = new OracleDataSource();
//
//		datasource.setUser("tdes");
//		datasource.setPassword("tde$12dev");
//		datasource.setURL("jdbc:oracle:thin:@armdbd1:1621:tdesdev");
//		datasource.setImplicitCachingEnabled(true);
//		datasource.setFastConnectionFailoverEnabled(true);
//
//		return datasource;
//
//	}
}
