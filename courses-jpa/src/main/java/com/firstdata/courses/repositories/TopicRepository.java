package com.firstdata.courses.repositories;

import org.springframework.data.repository.CrudRepository;
import com.firstdata.courses.resources.Topic;

public interface TopicRepository extends CrudRepository<Topic, String> {
	

}
