package com.firstdata.courses.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.firstdata.courses.resources.Course;

public interface CourseRepository extends CrudRepository<Course, String> {
	
	public List<Course> findByName(String name);
	public List<Course> findByTopicId(String topicId);
}
