package com.firstdata.courses.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.firstdata.courses.repositories.TopicRepository;
import com.firstdata.courses.resources.Topic;

@Service
public class TopicService {

	@Autowired
	private TopicRepository topicRepository;

	public List<Topic> getAllTopics() {

		List<Topic> topics = new ArrayList<>();
		
//		Iterable<Topic> allTopics = topicRepository.findAll(); // Using Iterable
//    	for (Topic topic : allTopics)
//    	{
//    		System.out.println("topic: " + topic);
//    		topics.add(topic);
//    	}
//    	
		
		topicRepository.findAll().forEach(topics::add);    // Using method references and Lambda expression

		return topics;
	}

	public Topic getTopic(String id) {

		Topic topic = topicRepository.findOne(id);
		
		return topic;
	}

	public void addTopic(Topic topic) {

		topicRepository.save(topic);
	}

	public void updateTopic(String id, Topic topic) {

		System.out.println("Updating topic=" + topic);
		
		topicRepository.save(topic);
	}

	public void deleteTopic(String id) {

		topicRepository.delete(id);

	}
}
