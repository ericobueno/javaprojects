package com.firstdata.courses.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.firstdata.courses.repositories.CourseRepository;
import com.firstdata.courses.resources.Course;

@Service
public class CourseService {

	@Autowired
	private CourseRepository courseRepository;

	public List<Course> getAllCourses(String topicId) {

		List<Course> courses = new ArrayList<>();
		
//		Iterable<Course> allCourses = courseRepository.findByTopicId(topicId); // Using Iterable
//    	for (Course Course : allCourses)
//    	{
//    		System.out.println("Course: " + Course);
//    		courses.add(Course);
//    	}
//    	
		
		courseRepository.
		findByTopicId(topicId).
		forEach(courses::add);    // Using method references and Lambda expression

		return courses;
	}

	public Course getCourse(String id) {

		Course course = courseRepository.findOne(id);
		
		return course;
	}

	public void addCourse(Course course) {

		courseRepository.save(course);
	}

	public void updateCourse(Course course) {

		System.out.println("Updating Course=" + course);
		
		courseRepository.save(course);
	}

	public void deleteCourse(String id) {

		courseRepository.delete(id);

	}
}
