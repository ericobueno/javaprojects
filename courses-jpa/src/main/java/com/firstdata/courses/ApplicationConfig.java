package com.firstdata.courses;

import java.sql.SQLException;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import com.firstdata.courses.repositories.TopicRepository;
import com.firstdata.courses.resources.Topic;

import oracle.jdbc.pool.OracleDataSource;

@Configuration
public class ApplicationConfig {

	@Bean
	CommandLineRunner loadDatabase(TopicRepository tr) {
		Logger logger = LoggerFactory.getLogger(ApplicationConfig.class);

		return args -> {

			logger.debug("loading Courses database..");

			tr.save(new Topic("Java", "Java Core", "Java programming Description"));
			tr.save(new Topic("Networking", "Core Networking ", "Networking basics Description"));

			logger.debug("record count: {}", tr.count());

			tr.findAll().forEach(x -> logger.debug(x.toString()));
		};

	}

	@Bean
	DataSource datasource() throws SQLException {

		OracleDataSource datasource = new OracleDataSource();

		datasource.setUser("tdes");
		datasource.setPassword("tde$12dev");
		datasource.setURL("jdbc:oracle:thin:@armdbd1:1621:tdesdev");
		datasource.setImplicitCachingEnabled(true);
		datasource.setFastConnectionFailoverEnabled(true);

		return datasource;

	}

	@Bean		// DEclares the class HelloWorld as a bean that can be injected or autowired
	public HelloWorld helloWorld() {
		return new HelloWorld();
	}
}
