package com.firstdata.courses.controllers;

import java.util.List;

import javax.inject.Inject;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.firstdata.courses.HelloWorld;
import com.firstdata.courses.resources.Topic;
import com.firstdata.courses.services.TopicService;

@RestController
public class TopicController {
	
	private static Logger logger = Logger.getLogger(TopicController.class);
	
	@Autowired
	private TopicService topicService;
	
	@Inject
	private HelloWorld greeter;

//	http://localhost:8080/topics		List All topics
	
	@RequestMapping(value = "/topics", method = RequestMethod.GET)
	public List<Topic> getAllTopics() {
		
		logger.info("Get all topics");
		
		greeter.greet();
		
		return topicService.getAllTopics();
	}

//	http://localhost:8080/topics/Java
//	http://localhost:8080/topics/Networking
			
	@RequestMapping(value = "/topics/{id}", method = RequestMethod.GET)
	public Topic getTopic(@PathVariable("id") String id) {

		logger.info("Requesting topic=" + id);
		
		//System.out.println("Requesting topic=" + id);
		
		return topicService.getTopic(id);
	}

	/*
	POST to http://localhost:8080/topics with Body Below
	{

	    "id": "Java",
	    "name": "Java Core",
	    "description": "Java programming Description"
	}
*/
	@RequestMapping(method = RequestMethod.POST, value = "/topics")
	public void addTopic(@RequestBody Topic topic) {

		System.out.println("Adding " + topic);

		topicService.addTopic(topic);
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/topics/{id}")
	public void updateTopic(@RequestBody Topic topic, @PathVariable("id") String id) {

		System.out.println("Updating " + topic);

		topicService.updateTopic(id, topic);
	}

	@RequestMapping(value = "/topics/{id}", method = RequestMethod.DELETE)
	public void deleteTopic(@PathVariable("id") String id) {

		System.out.println("Deleting topic=" + id);

		topicService.deleteTopic(id);
	}
}
