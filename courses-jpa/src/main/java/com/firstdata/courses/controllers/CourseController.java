package com.firstdata.courses.controllers;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.firstdata.courses.resources.Course;
import com.firstdata.courses.resources.Topic;
import com.firstdata.courses.services.CourseService;

@RestController
public class CourseController {

	private static Logger logger = Logger.getLogger(CourseController.class);

	@Autowired
	private CourseService courseService;

	@RequestMapping(value = "/topics/{id}/courses", method = RequestMethod.GET)
	public List<Course> getAllCourses(@PathVariable("id") String id) {

		logger.info("Get all courses for given topic ID");

		return courseService.getAllCourses(id);
	}

	@RequestMapping(value = "/topics/{topicId}/courses/{id}", method = RequestMethod.GET)
	public Course getCourse(@PathVariable("id") String id) {

		logger.info("Requesting course=" + id);

		// System.out.println("Requesting topic=" + id);

		return courseService.getCourse(id);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/topics/{topicId}/courses")
	public void addCourse(@RequestBody Course course, @PathVariable("topicId") String topicId) {

		System.out.println("Adding " + course);

		course.setTopic(new Topic(topicId, "", ""));

		courseService.addCourse(course);
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/topics/{topicId}/courses/{id}")
	public void updateCourse(@RequestBody Course course, @PathVariable("topicId") String topicId,
			@PathVariable("id") String id) {

		System.out.println("Updating " + course);

		course.setTopic(new Topic(topicId, "", ""));

		courseService.updateCourse(course);
	}

	@RequestMapping(value = "/topics/{topicId}/courses/{id}", method = RequestMethod.DELETE)
	public void deleteCourse(@PathVariable("topicId") String topicId, @PathVariable("id") String id) {

		System.out.println("Deleting course=" + id);

		courseService.deleteCourse(id);
	}
}
