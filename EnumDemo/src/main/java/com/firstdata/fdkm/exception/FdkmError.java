package com.firstdata.fdkm.exception;

public enum FdkmError {
	
	DUPLICATE(1,"Duplicate Key");
	
	private int errorCode;
	private String errorDescription;
	
	private FdkmError(int errorCode, String errorDescription) {
		this.errorCode = errorCode;
		this.errorDescription = errorDescription;
	}

	public int getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorDescription() {
		return errorDescription;
	}

	public void setErrorDescription(String errorDescription) {
		this.errorDescription = errorDescription;
	}
}
