package com.firstdata.fdkm.EnumDemo;

public class Application {

	public static void main(String[] args) {
		Color myColor = Color.YELLOW;

		for (Color c : Color.values()) {
			
			System.out.printf("The color is %s\n", c);
			
			System.out.printf("%s\tvalue:%s%n", c.name(), c.value());
		}

		sayColor(Color.GREEN);
		
		sayColor(myColor);
	}

	public static void sayColor(Color color) {

		System.out.printf("Color %s\tvalue = %s\n", color.name(), color.value());
	}

}
