package com.firstdata.fdkm.EnumDemo;

public enum Color {
	
	RED(255,0,0), BLUE(0,0,255), GREEN(0,255,0), YELLOW(255,255,0), BLACK(0,0,0);
	
	private int red;
	private int green;
	private int blue;
	
	private Color(int red, int green, int blue) {
		this.red = red;
		this.green = green;
		this.blue = blue;
	}
	

	public String value()
	{
		return String.format("{%03d, %03d, %03d}", red, green, blue);
		
	}
	
	@Override
	public String toString() {
		
		String originalName = super.toString();
		
		return originalName.substring(0, 1) + originalName.substring(1).toLowerCase();
	}
}
