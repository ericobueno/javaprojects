package com.firstdata.fdkm.exception;

import com.firstdata.fdkm.constant.FdkmError;

public class FdkmException extends Exception {

	private static final long serialVersionUID = 3644719603435545096L;

	FdkmError fdkmError;

	public FdkmException(FdkmError fdkmError) {

		super(fdkmError.getMessage());

		this.fdkmError = fdkmError;
	}

	public FdkmException(FdkmError fdkmError, String message ) {

		super(fdkmError.getMessage() + " (" + message + ")");

		this.fdkmError = fdkmError;
	}

	public FdkmError getFdkmError() {
		return fdkmError;
	}

	public int getErrorCode() {
		return fdkmError.getErrorCode();
	}
}
