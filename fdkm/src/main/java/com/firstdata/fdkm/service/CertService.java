package com.firstdata.fdkm.service;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.firstdata.fdkm.common.FdkmCert;
import com.firstdata.fdkm.constant.FdkmError;
import com.firstdata.fdkm.exception.FdkmException;
import com.firstdata.fdkm.model.SecurityClass;
import com.firstdata.fdkm.model.SecurityObject;
import com.firstdata.fdkm.repository.SecurityClassRepository;
import com.firstdata.fdkm.repository.SecurityObjectRepository;

@Service
public class CertService {

	private Logger logger = LoggerFactory.getLogger(this.getClass().getName());

	@Autowired
	private SecurityClassRepository securityClassRepository;

	@Autowired
	private SecurityObjectRepository securityObjectRepository;

	@Autowired
	private CryptoService cryptoService;

	@Transactional
	public FdkmCert getCert(String certAlias) throws FdkmException {

		SecurityObject scyObj;
		FdkmCert cert = null;

		List<SecurityObject> scyObjs = securityObjectRepository.findCert(certAlias);

		logger.info("Found {} certs with alias={}", scyObjs.size(), certAlias);

		if (scyObjs.size() == 1) {

			scyObj = scyObjs.get(0);

			String certData = cryptoService.decryptScyObj(scyObj.getObjData()); // Decrypt the certificate

			cert = new FdkmCert(scyObj.getScyObjId(), scyObj.getScyObjAlias(), scyObj.getStatus(),
					scyObj.getSecurityClass().getScyClassName(), scyObj.getCreatedDate(), scyObj.getEndDate(),
					certData);

		} else {
			throw new FdkmException(FdkmError.CERT_NOT_FOUND);
		}

		return cert;
	}

	@Transactional
	public FdkmCert storeCert(String certClassName, String certAlias, String b64CertData) throws FdkmException {

		Date today = new Date();
		List<SecurityClass> securityClassList;
		SecurityClass certClass = new SecurityClass();
		FdkmCert fdkmCert = null;

		try {

			Calendar calendar = Calendar.getInstance();

			try { // Make sure we don't have this certificate already
				fdkmCert = getCert(certAlias);
				throw new FdkmException(FdkmError.DUPLICATE_CERTIFICATE); // Error is already in database

			} catch (FdkmException e) {
				if (e.getFdkmError() != FdkmError.CERT_NOT_FOUND) {
					throw e;
				}
			}

			securityClassList = securityClassRepository.findByScyClassName(certClassName);

			logger.info("Found {} entries for {} Security Class", securityClassList.size(), certClassName);

			int found = securityClassList.size();

			if (found == 1) {

				certClass = securityClassList.get(0); // get the security class

				SecurityObject cert = new SecurityObject();

				long objId = ThreadLocalRandom.current().nextLong(1234567, 9999999999L);

				cert.setScyObjId(objId);
				cert.setScyObjAlias(certAlias);
				cert.setStatus("ACTIVE");
				cert.setCreatedDate(today);
				cert.setStartDate(today);
				calendar.setTime(today);
				calendar.add(Calendar.DATE, certClass.getDuration());
				cert.setEndDate(calendar.getTime());
				cert.setSecurityClass(certClass); // Link to the parent Security class

				String objData = cryptoService.encryptCert(b64CertData); // Encrypt the Certificate

				cert.setObjData(objData);

				SecurityObject save = securityObjectRepository.save(cert);

				logger.info("Create {} End {}", save.getCreatedDate(), save.getEndDate());

				fdkmCert = getCert(certAlias);

			} else if (found == 0) {
				throw new FdkmException(FdkmError.CERT_CLASS_NOT_FOUND);
			} else {
				throw new FdkmException(FdkmError.TOO_MANY_FOUND);
			}
		} catch (FdkmException e) {
			throw e;
		} catch (Exception e) {
			throw new FdkmException(FdkmError.SYSTEM_ERROR, e.getMessage());
		}

		return fdkmCert;
	}
}
