package com.firstdata.fdkm.service;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Base64;

import javax.annotation.PostConstruct;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Hex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.firstdata.fdkm.constant.FdkmError;
import com.firstdata.fdkm.exception.FdkmException;

/*
String originalInput = "test input";
String encodedString = Base64.getEncoder().encodeToString(originalInput.getBytes());


byte[] decodedBytes = Base64.getDecoder().decode(encodedString);
String decodedString = new String(decodedBytes);

*/

@Service
public class CryptoService {

	private SecretKeySpec masterKey;

	@Value("${fdkm.secret}")
	private String secret;

	@Value("${fdkm.ivs}")
	private String ivs;

	private static final String salt = "AmericaIsGreat";

	private static byte[] key;
	private byte[] iv;
	IvParameterSpec ivSpecs;

	private Logger logger = LoggerFactory.getLogger(this.getClass().getName());

	public CryptoService() throws FdkmException {

	}

	@PostConstruct
	void init() {

		logger.info("------>>>secret={} ivs={}", secret, ivs);
		try {
			setMasterKey();

			iv = Hex.decodeHex(ivs.toCharArray());

			ivSpecs = new IvParameterSpec(iv);

		} catch (Exception e) {

			logger.error("Error initializing. {}", e.getMessage());
		}
	}

	public void setMasterKey() throws UnsupportedEncodingException, NoSuchAlgorithmException {

		MessageDigest sha = null;

		secret += salt;

		key = secret.getBytes("UTF-8");

		sha = MessageDigest.getInstance("SHA-256");

		key = sha.digest(key);

		key = Arrays.copyOf(key, 32);

		logger.info("Hashed value key({})={}", key.length, Hex.encodeHexString(key));

		masterKey = new SecretKeySpec(key, "AES");

		byte[] encoded = masterKey.getEncoded();

		logger.info("MasterKey is {} bytes. data={}", encoded.length, Hex.encodeHexString(encoded));

	}

	public String getMasterKey() {

		String key;

		byte[] encoded = masterKey.getEncoded();

		key = new String(Hex.encodeHex(encoded));

		logger.info("Master is {} bytes. data={}", encoded.length, key);

		return key;
	}

	public CryptoService(byte[] masterKey) {
		super();
	}

	public String encryptCert(String b64Data) throws FdkmException {

		try {

			byte[] cDataBytes = Base64.getDecoder().decode(b64Data); // Remove Base64 Wrapper

			logger.info("Store cert=\n{}", new String(cDataBytes));

			byte[] eDataBytes = encrypt(cDataBytes);

			return Base64.getEncoder().encodeToString(eDataBytes); // Wrap into Base 64 to store in database

		} catch (Exception e) {
			throw new FdkmException(FdkmError.SYSTEM_ERROR, e.getMessage());
		}
	}

	public String encryptKey(byte[] cData) throws FdkmException {

		try {

			byte[] eDataBytes = encrypt(cData);

			String eData = Base64.getEncoder().encodeToString(eDataBytes); // Wrap it into Base64 to store in database

			return eData;

		} catch (Exception e) {
			throw new FdkmException(FdkmError.SYSTEM_ERROR, e.getMessage());
		}
	}

	public String decryptScyObj(String eData) throws FdkmException {

		byte[] eDataBytes; // Encrypted data in byte array format
		byte[] cDataBytes; // Clear data in byte array format
		String cData; // Clear data in String format

		try {

			logger.info("Decrypting {}", eData);

			eDataBytes = Base64.getDecoder().decode(eData);  // Unwrap from Base64

			cDataBytes = decrypt(eDataBytes);

			logger.info("Retrieved cert=\n{}", new String(cDataBytes));

			cData = Base64.getEncoder().encodeToString(cDataBytes); // Data is binary. Encode Base64 for transport

			return cData;

		} catch (Exception e) {
			throw new FdkmException(FdkmError.SYSTEM_ERROR, e.getMessage());
		}
	}

	private byte[] encrypt(byte[] cData) throws Exception {

		Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");

		cipher.init(Cipher.ENCRYPT_MODE, masterKey, ivSpecs);

		byte[] eDataBytes = cipher.doFinal(cData);

		return eDataBytes;
	}

	private byte[] decrypt(byte[] eDataBytes) throws Exception {

		Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");

		cipher.init(Cipher.DECRYPT_MODE, masterKey, ivSpecs);

		byte[] cDataBytes = cipher.doFinal(eDataBytes);

		return cDataBytes;
	}

}
