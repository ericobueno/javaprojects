package com.firstdata.fdkm.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.firstdata.fdkm.common.FdkmKey;
import com.firstdata.fdkm.constant.FdkmError;
import com.firstdata.fdkm.exception.FdkmException;
import com.firstdata.fdkm.model.SecurityObject;
import com.firstdata.fdkm.repository.SecurityObjectRepository;

@Service
public class KeyService {

	private Logger logger = LoggerFactory.getLogger(this.getClass().getName());

	@Autowired
	private SecurityObjectRepository securityObjectRepository;

	public List<SecurityObject> getAllKeys() {

		List<SecurityObject> scyObjs = new ArrayList<>();

		Iterable<SecurityObject> allObjs = securityObjectRepository.findAll();

		for (SecurityObject scyObj : allObjs) {
			System.out.println("scyObject: " + scyObj);
			scyObjs.add(scyObj);
		}

		// allObjs.forEach(scyObjs::add);

		// scyObjs.clear();

		return scyObjs;
	}
	
	@Transactional
	public List<SecurityObject> getKeysForClass(String scyClassName) {

		List<SecurityObject> keys = securityObjectRepository.findKeysBySecurityClassName(scyClassName);

		return keys;
	}

	@Transactional
	public FdkmKey getActiveKeyForClass(String scyClassName) throws FdkmException {

		SecurityObject scyObj;
		FdkmKey key = null;

		List<SecurityObject> scyObjs = securityObjectRepository.findActiveKey(scyClassName);

		logger.info("Found {} active keys", scyObjs.size());

		if (scyObjs.size() == 1) {

			scyObj = scyObjs.get(0);

			key = new FdkmKey(scyObj.getScyObjId(), scyObj.getStatus(), scyObj.getSecurityClass().getScyClassName(),
					scyObj.getCreatedDate(), scyObj.getEndDate(), scyObj.getObjData());

		} else {
			throw new FdkmException(FdkmError.KEY_NOT_FOUND);
		}

		return key;
	}

	@Transactional
	public FdkmKey getKeyById(long keyId) throws FdkmException {

		SecurityObject scyObj;
		FdkmKey key = null;

		List<SecurityObject> scyObjs = securityObjectRepository.findKeyById(keyId);

		logger.info("Found {} keys with id={}", scyObjs.size(), keyId);

		if (scyObjs.size() == 1) {

			scyObj = scyObjs.get(0);

			key = new FdkmKey(scyObj.getScyObjId(), scyObj.getStatus(), scyObj.getSecurityClass().getScyClassName(),
					scyObj.getCreatedDate(), scyObj.getEndDate(), scyObj.getObjData());

		} else {
			throw new FdkmException(FdkmError.KEY_NOT_FOUND);
		}

		return key;
	}
}
