package com.firstdata.fdkm.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.firstdata.fdkm.common.FdkmCert;
import com.firstdata.fdkm.common.FdkmCertRequest;
import com.firstdata.fdkm.common.FdkmCertResponse;
import com.firstdata.fdkm.common.FdkmKey;
import com.firstdata.fdkm.common.FdkmKeyRequest;
import com.firstdata.fdkm.common.FdkmKeyResponse;
import com.firstdata.fdkm.constant.FdkmError;
import com.firstdata.fdkm.exception.FdkmException;
import com.firstdata.fdkm.model.SecurityObject;
import com.firstdata.fdkm.service.CertService;
import com.firstdata.fdkm.service.KeyService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@Api(value = "Key Controller API", produces = MediaType.APPLICATION_JSON_VALUE)
public class FdkmController {

	private Logger logger = LoggerFactory.getLogger(this.getClass().getName());

	@Autowired
	private KeyService keyService;

	@Autowired
	private CertService certService;

	@ApiOperation("Retrieve all keys")
	// @ApiResponses(value = { @ApiResponse(code = 200, message = "OK") } )
	@RequestMapping(value = "/keys", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.APPLICATION_XML_VALUE })
	public List<SecurityObject> getAllKeys() {

		return keyService.getAllKeys();

	}

	@ApiOperation("Retrieve all keys for a security class given its name")
	@RequestMapping(value = "/keys/{scyClassName}", method = RequestMethod.GET, produces = {
			MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public List<SecurityObject> getKeysForclass(@PathVariable("scyClassName") String scyClassName) {

		return keyService.getKeysForClass(scyClassName);

	}

	@ApiOperation("Retrieve Active key for a security class given its name")
	@RequestMapping(value = "/keys/{scyClassName}/active", method = RequestMethod.GET, produces = {
			MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public FdkmKeyResponse getActiveKeyForclass(@PathVariable("scyClassName") String scyClassName) {

		FdkmKey fdkmKey = null;
		FdkmKeyResponse response;

		logger.info("Get active key for {}", scyClassName);

		try {

			fdkmKey = keyService.getActiveKeyForClass(scyClassName);

			response = new FdkmKeyResponse(fdkmKey);

		} catch (FdkmException e) {
			response = new FdkmKeyResponse(e.getErrorCode(), e.getMessage());
		} catch (Exception e) {
			response = new FdkmKeyResponse(FdkmError.SYSTEM_ERROR.getErrorCode(),
					FdkmError.SYSTEM_ERROR.getMessage() + " -> " + e.getMessage());
		}

		return response;
	}

	@ApiOperation("Retrieve key given its ID")
	@RequestMapping(value = "/keys/{scyClassName}/{keyId}", method = RequestMethod.GET, produces = {
			MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })

	public FdkmKeyResponse getKeyById(@PathVariable("scyClassName") String scyClassName,
			@PathVariable("keyId") long keyId) {

		FdkmKey fdkmKey = null;
		FdkmKeyResponse response;

		logger.info("Get key with ID={}", keyId);

		try {
			fdkmKey = keyService.getKeyById(keyId);
			response = new FdkmKeyResponse(fdkmKey);
		} catch (FdkmException e) {
			response = new FdkmKeyResponse(e.getErrorCode(), e.getMessage());
		} catch (Exception e) {
			response = new FdkmKeyResponse(FdkmError.SYSTEM_ERROR.getErrorCode(),
					FdkmError.SYSTEM_ERROR.getMessage() + " -> " + e.getMessage());
		}

		return response;
	}

	@RequestMapping(value = "/getActiveKey", method = RequestMethod.POST, consumes = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.APPLICATION_XML_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE,
					MediaType.APPLICATION_XML_VALUE })
	@ApiOperation("Retrieve Active Key for given security class")
	public FdkmKeyResponse getActiveKey(@RequestBody FdkmKeyRequest request) {

		FdkmKeyResponse response;

		logger.info("POST - Get active key for {}", request.getKeyClassName());

		try {

			FdkmKey fdkmKey = keyService.getActiveKeyForClass(request.getKeyClassName());
			response = new FdkmKeyResponse(fdkmKey);

		} catch (FdkmException e) {
			response = new FdkmKeyResponse(e.getErrorCode(), e.getMessage());
		} catch (Exception e) {
			response = new FdkmKeyResponse(FdkmError.SYSTEM_ERROR.getErrorCode(),
					FdkmError.SYSTEM_ERROR.getMessage() + " -> " + e.getMessage());
		}

		return response;
	}

	@RequestMapping(value = "/getKey", method = RequestMethod.POST, consumes = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.APPLICATION_XML_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE,
					MediaType.APPLICATION_XML_VALUE })
	@ApiOperation("Retrieve Key given it key ID")
	public FdkmKeyResponse getKey(@RequestBody FdkmKeyRequest request) {

		FdkmKeyResponse response;

		logger.info("POST - Get key with ID={}", request.getKeyId());

		try {
			FdkmKey fdkmKey = keyService.getKeyById(request.getKeyId());
			response = new FdkmKeyResponse(fdkmKey);

		} catch (FdkmException e) {
			response = new FdkmKeyResponse(e.getErrorCode(), e.getMessage());
		} catch (Exception e) {
			response = new FdkmKeyResponse(FdkmError.SYSTEM_ERROR.getErrorCode(),
					FdkmError.SYSTEM_ERROR.getMessage() + " -> " + e.getMessage());
		}

		return response;
	}

	@RequestMapping(value = "/getCert", method = RequestMethod.POST, consumes = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.APPLICATION_XML_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE,
					MediaType.APPLICATION_XML_VALUE })
	@ApiOperation("Retrieve certifcate given its alias")
	public FdkmCertResponse getCert(@RequestBody FdkmCertRequest request) {

		FdkmCertResponse response;

		logger.info("POST - Get certificate with Alias={}", request.getCertAlias());

		try {
			FdkmCert fdkmCert = certService.getCert(request.getCertAlias());

			response = new FdkmCertResponse(fdkmCert);

		} catch (FdkmException e) {
			response = new FdkmCertResponse(e.getErrorCode(), e.getMessage());
		} catch (Exception e) {
			response = new FdkmCertResponse(FdkmError.SYSTEM_ERROR.getErrorCode(),
					FdkmError.SYSTEM_ERROR.getMessage() + " -> " + e.getMessage());
		}

		return response;
	}

	@RequestMapping(value = "/storeCert", method = RequestMethod.POST, consumes = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.APPLICATION_XML_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE,
					MediaType.APPLICATION_XML_VALUE })
	@ApiOperation("Retrieve certifcate given its alias")
	public FdkmCertResponse storeCert(@RequestBody FdkmCertRequest request) {

		FdkmCertResponse response;

		logger.info("POST - Store certificate with certClass={} Alias={}", request.getCertClassName(),
				request.getCertAlias());

		try {
			
			FdkmCert fdkmCert = certService.storeCert(request.getCertClassName(), request.getCertAlias(), request.getCert());

			response = new FdkmCertResponse(fdkmCert);

		} catch (FdkmException e) {
			
			response = new FdkmCertResponse(e.getErrorCode(), e.getMessage());
			
		} catch (Exception e) {
			response = new FdkmCertResponse(FdkmError.SYSTEM_ERROR.getErrorCode(),
					FdkmError.SYSTEM_ERROR.getMessage() + " -> " + e.getMessage());
		}

		return response;
	}
}
