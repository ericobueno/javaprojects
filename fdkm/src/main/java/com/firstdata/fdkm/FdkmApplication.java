package com.firstdata.fdkm;

import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.PropertySource;

import oracle.sql.ARRAY;

@SpringBootApplication
@PropertySource("file:${configFile}")
public class FdkmApplication {
	
	private static Logger logger = LoggerFactory.getLogger(FdkmApplication.class);
	
	public static void main(String[] args) {
		
		ConfigurableApplicationContext ctx = SpringApplication.run(FdkmApplication.class, args);
		
		String[] beanDefinitionNames = ctx.getBeanDefinitionNames();
		
		List<String> beanNames = Arrays.asList(beanDefinitionNames);
		
		beanNames.forEach( n -> logger.info("beanId={}", n) );
	}
}
