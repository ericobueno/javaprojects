package com.firstdata.fdkm.config;

import java.util.function.Predicate;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * 
 * @author ebueno
 *
 *     See FDKM REST API at http://host:port/swagger-ui.html
 * 
 */

@EnableSwagger2
@PropertySource("classpath:swagger.properties")
@ComponentScan(basePackages = { "com.firstdata.fdkm.controller" })
@Configuration
public class SwaggerConfig {

	private static final String API_VERSION = "1.0";
	private static final String LICENSE = "Licensed by First Data";
	private static final String TITLE = "First Data Key Manager";
	private static final String DESCRIPTION = "FDKM REST API";

	private ApiInfo apiInfo() {

		Contact contact = new Contact("TransArmor", "http://www.firstdata.com",
				"TransArmorSupport@firstdata.com");

		return new ApiInfoBuilder().title(TITLE).description(DESCRIPTION).contact(contact).license(LICENSE)
				.version(API_VERSION).build();
	}

	// .paths(PathSelectors.any())

	@Bean
	public Docket stocksApi() {
		
		com.google.common.base.Predicate<String> p = s-> s.startsWith("/get") || s.startsWith("/store");
		
//		return new Docket(DocumentationType.SWAGGER_2).apiInfo(apiInfo()).pathMapping("/").select()
//				.paths( PathSelectors.regex("/get.*") ).build();
		
		return new Docket(DocumentationType.SWAGGER_2).apiInfo(apiInfo()).pathMapping("/").select()
						.paths(p).build();
	}

}
