package com.firstdata.regex;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class RegexTestHarness {
	// private static final String t1Regx =
	// "%([A-Z])([0-9]{1,19})\\^([^\\^]{2,26})\\^
	// ([0-9]{4}|\\^)([0-9]{3}|\\^)([^\\?]+)\\?";

	private static final String t1Regx = "(%([AB])([0-9]{1,19})\\^([^\\^]{2,26})\\^([0-9]{4}|\\^)([0-9]{3}|\\^)([^\\?]+)\\?)";

	private static final String t1mRegx = "(%M([0-9]{1,19})\\^([^\\^]{2,26})\\^([0-9]{4}|\\^)000000([0-9]{3,4})000000\\?)";

	private static final String t2Regx = "(;([0-9]{1,19})=([0-9]{4})([0-9]{3}|\\^)([^\\?]+)\\?)";

	private static final String t2mRegx = "(;([0-9]{1,19})=([0-9]{4})000000([0-9]{3,4})000\\?)";
	
	private static final String t1t2Regx = "\\h*(%([AB])([0-9]{1,19})\\^([^\\^]{2,26})\\^([0-9]{4}|\\^)([0-9]{3}|\\^)([^\\?]+)\\?)*" + 
			                               "\\h*(;([0-9]{1,19})=([0-9]{4})([0-9]{3}|\\^)([^\\?]+)\\?)*";
	
	
	public static void main(String[] args) throws IOException {

		String track1Track2 = "  %B4815881002861896^YATES/EUGENE JOHN         ^19039821000123456789?   ;1234567890123456=19027771234567?  ";
		String track1 = "  %B4815881002861896^YATES/EUGENE JOHN         ^19039821000123456789?";
		String track2 = ";1234567890123456=19027771234567?  ";
		
		String manual = "%M5444009999222205^MANUALLY/ENTERED^12120000001234000000?;5444009999222205=12120000001234000?";
//		String manual = ";5444009999222205=12120000001234000?";
		/*
		 * BufferedReader br = new BufferedReader(new
		 * InputStreamReader(System.in));
		 * System.out.print("\nEnter your regex: "); regex = br.readLine();
		 * Pattern pattern = Pattern.compile(t1Regx);
		 * System.out.print("Enter input string to search: "); line =
		 * br.readLine(); Matcher matcher = pattern.matcher(track);
		 */

		Pattern pattern = Pattern.compile(t1Regx);
		Matcher matcher = pattern.matcher(track1Track2);

		System.out.println("\nSearching for Track1");

		System.out.println("T1 Group count = " + matcher.groupCount());
		
		if (matcher.find()) {

			System.out.format("I found the text group " + " \"%s\" starting at " + "index %d and ending at index %d.%n",
					matcher.group(), matcher.start(), matcher.end());

			System.out.println("G1: " + matcher.group(1));
			System.out.println("CardType: " + matcher.group(2));
			System.out.println("PAN: " + matcher.group(3));
			System.out.println("Name: " + matcher.group(4));
			System.out.println("ExpDate: " + matcher.group(5));
			System.out.println("ServiceCode: " + matcher.group(6));
			System.out.println("Discretionary: " + matcher.group(7));

		} else {
			System.out.format("Track1 not found.%n");
		}

		System.out.println("\nSearching for Track2");

		pattern = Pattern.compile(t2Regx);
		matcher = pattern.matcher(track1Track2);
		System.out.println("T2 Group count = " + matcher.groupCount());

		if (matcher.find()) {

			System.out.format("I found the text group " + " \"%s\" starting at " + "index %d and ending at index %d.%n",
					matcher.group(), matcher.start(), matcher.end());

			System.out.println("G1: " + matcher.group(1));
			System.out.println("PAN: " + matcher.group(2));
			System.out.println("ExpDate: " + matcher.group(3));
			System.out.println("ServiceCode: " + matcher.group(4));
			System.out.println("Discretionary: " + matcher.group(5));
		} else {
			System.out.format("Track2 not found.%n");
		}

		pattern = Pattern.compile(t1mRegx);
		matcher = pattern.matcher(manual);

		System.out.println("\nSearching for Manual Track1");

		System.out.println("T1 Group count = " + matcher.groupCount());

		if (matcher.find()) {

			System.out.format("I found the text group " + " \"%s\" starting at " + "index %d and ending at index %d.%n",
					matcher.group(), matcher.start(), matcher.end());

			System.out.println("G1: " + matcher.group(1));
			System.out.println("PAN: " + matcher.group(2));
			System.out.println("Name: " + matcher.group(3));
			System.out.println("ExpDate: " + matcher.group(4));
			System.out.println("Security: " + matcher.group(5));

		} else {
			System.out.format("Manual Track1 not found.%n");
		}

		pattern = Pattern.compile(t2mRegx);
		matcher = pattern.matcher(manual);

		System.out.println("\nSearching for Manual Track2");

		System.out.println("T2 Group count = " + matcher.groupCount());

		if (matcher.find()) {

			System.out.format("I found the text group " + " \"%s\" starting at " + "index %d and ending at index %d.%n",
					matcher.group(), matcher.start(), matcher.end());

			System.out.println("G1: " + matcher.group(1));
			System.out.println("PAN: " + matcher.group(2));
			System.out.println("ExpDate: " + matcher.group(3));
			System.out.println("Security: " + matcher.group(4));

		} else {
			System.out.format("Manual Track2 not found.%n");
		}
		
		pattern = Pattern.compile(t1t2Regx);
		matcher = pattern.matcher(track1Track2);

		System.out.println("\nSearching for T1 and T2");

		System.out.println("T1T2 Group count = " + matcher.groupCount());
		System.out.println("Matcher= <" + matcher.toString() + ">");
		
		if (matcher.find()) {

			System.out.format("RegionStart=%d RegionEnd=%d\n", matcher.regionStart(), matcher.regionEnd());
			
			System.out.format("I found the text group " + " \"%s\" starting at " + "index %d and ending at index %d.%n",
					matcher.group(), matcher.start(), matcher.end());

			System.out.println("G1(Track1): " + matcher.group(1));
			System.out.println("G2: " + matcher.group(2));
			System.out.println("G3: " + matcher.group(3));
			System.out.println("G4: " + matcher.group(4));
			System.out.println("G5: " + matcher.group(5));
			System.out.println("G6: " + matcher.group(6));
			System.out.println("G7: " + matcher.group(7));
			System.out.println("G8(Track2): " + matcher.group(8));
			System.out.println("G9: " + matcher.group(9));
			System.out.println("G10: " + matcher.group(10));

		} else {
			System.out.format("Manual Track2 not found.%n");
		}
	}
}
