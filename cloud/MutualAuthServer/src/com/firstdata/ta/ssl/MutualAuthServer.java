package com.firstdata.ta.ssl;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLServerSocketFactory;
import javax.net.ssl.SSLSocket;

public class MutualAuthServer {

	public static void main(String[] args) throws IOException {
		
		System.setProperty("javax.net.ssl.keyStore", "/Users/ebueno/certs/server.jks");
		System.setProperty("javax.net.ssl.keyStorePassword", "changeit");
		
		System.setProperty("javax.net.ssl.trustStore", "/Users/ebueno/certs/truststore.jks");
		System.setProperty("javax.net.ssl.trustStorePassword", "changeit");
		
		/* Same as    -Djavax.net.debug=ssl */ 
//		System.setProperty("javax.net.debug", "ssl");
		
		SSLServerSocketFactory fac = (SSLServerSocketFactory)SSLServerSocketFactory.getDefault();
		
		SSLServerSocket listenSocket = (SSLServerSocket) fac.createServerSocket(8443);

		listenSocket.setNeedClientAuth(true); // Rejects connection is client certicate not sent or cannot be validated
		
//		listenSocket.setWantClientAuth(true); // It is ok not to have client certificate.
		
		SSLSocket socket = (SSLSocket) listenSocket.accept();
		
		System.out.println("Server started");
		
		doProtocol( socket );
		
		socket.close();
		
		System.out.println("Server stopped");
	}

	private static void doProtocol(SSLSocket socket) throws IOException {
		
		String hello = "Hi There - I am your server\n";
		
		InputStream in = socket.getInputStream();
		OutputStream out = socket.getOutputStream();
		
		out.write(hello.getBytes());
		
		int ch = 0;
		
		while( (ch = in.read()) != '!' ) {
			out.write(ch);
		}
		
		out.write('!');
		
	}

}
