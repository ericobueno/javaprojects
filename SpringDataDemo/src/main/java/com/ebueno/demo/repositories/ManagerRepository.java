package com.ebueno.demo.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.ebueno.demo.resources.Manager;

@RepositoryRestResource
public interface ManagerRepository extends CrudRepository<Manager, Long>  {
	
	Manager findByName( @Param("q") String name);
	
	List<Manager> findByEmployeesRoleContains(@Param("q") String role);

}
