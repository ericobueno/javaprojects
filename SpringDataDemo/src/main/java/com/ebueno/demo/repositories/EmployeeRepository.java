package com.ebueno.demo.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.ebueno.demo.resources.Employee;

@RepositoryRestResource
public interface EmployeeRepository extends CrudRepository < Employee, Long> {

	
	void deleteByLastName( @Param("q") String name );
	
	List<Employee> findByLastName(@Param("q") String name);
	
	List<Employee> findByRole(@Param("q") String role);
	
	List<Employee> findByFirstNameAndLastName(@Param("f") String firstName, @Param("l")String lastName);
	
//	List<Employee> findByFirstNameAndManagerName(@Param("f") String firstName, @Param("n") String name);
}
