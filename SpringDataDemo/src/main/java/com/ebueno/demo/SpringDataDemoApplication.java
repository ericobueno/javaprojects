package com.ebueno.demo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.ebueno.demo.repositories.EmployeeRepository;
import com.ebueno.demo.repositories.ManagerRepository;
import com.ebueno.demo.resources.Employee;
import com.ebueno.demo.resources.Manager;

@SpringBootApplication
public class SpringDataDemoApplication {

	Logger logger = LoggerFactory.getLogger(SpringDataDemoApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(SpringDataDemoApplication.class, args);
	}

	@Bean
	CommandLineRunner loadDatabase(EmployeeRepository er, ManagerRepository mr) {

		return args -> {
			int i = 0;
			for (String string : args) {
				System.out.println("Arg=[" + i++ + "]=" + string);
			}
			
			logger.warn("loading database..");

			Manager manager1 = new Manager("Howe");
			manager1 = mr.save(manager1);
			logger.warn("after Moreira= " + manager1);

			Manager manager2 = new Manager("Moreira");

			manager2 = mr.save(manager2);

			logger.warn("after Moreira= " + manager2);

			er.save(new Employee("Erico", "Bueno", "Engineer", manager1));
			er.save(new Employee("Carlos", "Bueno", "MD", manager1));
			er.save(new Employee("John", "Doe", "Accountant", manager2));

			logger.warn("record count: {}", er.count());

			Iterable<Employee> findAll = er.findAll();

			for (Employee employee : findAll) {
				logger.info(employee.toString());
			}

			er.findAll().forEach(x -> logger.warn(x.toString()));

		};
	}
}
