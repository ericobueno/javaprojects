package com.firstdata.ta.interfaces;

public interface Shape {
	
	public int draw(int x, int y); // Draw shape and return area

}
