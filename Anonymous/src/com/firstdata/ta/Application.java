package com.firstdata.ta;

import com.firstdata.ta.interfaces.Shape;

public class Application {

	public static void main(String[] args) {

		process(new Rectangle()); // Using a named class

		process(new Shape() { // Using an anonymous class defines here
			public int draw(int a, int b) {
				System.out.println("Drawing square a=" + a + " b=" + b);
				return a * b;
			};
		});

		process((a, b) -> { // Using a Lambda expression
			System.out.println("Drawing THe Shape a=" + a + " b=" + b);
			return a * b;
		});

		Shape a = new Shape() { // Using an anonymous class defines here

			public int draw(int a, int b) {
				System.out.println("Drawing Another square a=" + a + " b=" + b);
				return a * b;
			}
		};

		process(a);
	}

	public static void process(Shape shape) {

		int x = 2;
		int y = 4;

		int area = shape.draw(x, y);

		System.out.println("Area= " + area);
	}
}

class Rectangle implements Shape {

	@Override
	public int draw(int a, int b) {

		System.out.println("Drawing retangle a=" + a + " b=" + b);

		return a * b;
	}
}
