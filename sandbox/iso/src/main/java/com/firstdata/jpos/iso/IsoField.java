package com.firstdata.jpos.iso;

import javax.xml.bind.annotation.XmlAttribute;


public class IsoField {
	private int id;
	private String value;
	
	public IsoField() {
		super();
	}

	public IsoField(int id, String value) {
		super();
		this.id = id;
		this.value = value;
	}

	public int getId() {
		return id;
	}

	@XmlAttribute
	public void setId(int id) {
		this.id = id;
	}

	public String getValue() {
		return value;
	}

	@XmlAttribute
	public void setValue(String value) {
		this.value = value;
	}
	
	
	

}
