package com.firstdata.jpos.main;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import com.firstdata.jpos.iso.IsoField;
import com.firstdata.jpos.iso.IsoMsg;

/**
 * Hello world!
 *
 */
public class App {
	public static void main(String[] args) throws JAXBException, IOException {
		
		System.out.println("Marshal to src/main/resources/isomsg.xml");

		JAXBContext jaxbContext = JAXBContext.newInstance(IsoMsg.class);

		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

		File file = new File("src/main/resources/isomsg.xml");

		// jaxbMarshaller.marshal(gmf, file); // Marshal into a file

		StringWriter xmlDataWriter = new StringWriter();
		
		List<IsoField> fields = new ArrayList<>();

		fields.add( new IsoField(1,"0001"));
		fields.add( new IsoField(2,"0002"));
		fields.add( new IsoField(3,"0003"));
		
		IsoMsg msg = new IsoMsg(fields);
		
		jaxbMarshaller.marshal(msg, xmlDataWriter); // Marshal into a String
													// Writer
		System.out.println("Fmt: " + xmlDataWriter.toString());

		Writer writer = new BufferedWriter(new FileWriter(file));

		writer.write(xmlDataWriter.toString());

		writer.close();
		
		
	}
}
