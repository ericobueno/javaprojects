package com.firstdata.jpos.iso;



import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="isomsg")

public class IsoMsg {
	List<IsoField> fields;
	
	public IsoMsg() {
		super();
	}

	public IsoMsg(List<IsoField> fields) {
		super();
		this.fields = fields;
	}

	public List<IsoField> getFields() {
		return fields;
	}

	@XmlElement(name="field")
	public void setFields(List<IsoField> fields) {
		this.fields = fields;
	}
}
