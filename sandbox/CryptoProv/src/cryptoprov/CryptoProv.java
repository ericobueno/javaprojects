/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cryptoprov;

import com.ibm.crypto.pkcs11impl.provider.IBMPKCS11Impl;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.Provider;
import java.security.Security;

/**
 *
 * @author ebueno
 */
public class CryptoProv {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws KeyStoreException {

        System.out.println("List providers");

        Provider providers[] = Security.getProviders();

        for (int i = 0; i < providers.length; i++) {

            Provider provider = providers[i];

            System.out.println("Provider " + (i + 1) + " " + provider.getName());

        }

        Provider p = Security.getProvider("IBMPKCS11Impl");
       
        if (p == null) {
            System.out.println("Prov IBMPKCS11IMPL not found");
        } else {
            System.out.println("Prov IBMPKCS11IMPL found");
        }
        
        IBMPKCS11Impl prov = null;

        System.out.println("new IBMPKCS11Impl");

        try {
            prov = new IBMPKCS11Impl(args[0]);
        } catch (Exception e) {
            throw new KeyStoreException("KeyStoreException: " + e.getMessage());
        }

        KeyStore ks = KeyStore.getInstance("PKCS11IMPLKS", prov);
        if (ks == null) {
            System.out.println("KeyStore IBMPKCS11IMPLKS not found");
        } else {
            System.out.println("Prov IBMPKCS11IMPLKS found");
        }
    }

}
