/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.firstdata;

/**
 *
 * @author ebueno
 */
public class Main
{

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        int counter;
        int maxCount;
        int shifter;
        int nbits;
        int numberXtns = 0;

        counter = 0X1;
        maxCount = 0X1FFFFF;
//        maxCount = 0X1FFFFF;
        int newKey;
        int currentKey = 1;

        while (counter < maxCount)
        {
            shifter = Integer.lowestOneBit(counter);
            nbits = Integer.bitCount(counter);

            if (nbits < 11)
            {
//                System.out.println("nbits=" + nbits + " counter = " + Integer.toString(counter, 16));
                // newKey = derive(currentKey, counter);
                counter += 1;
                numberXtns++;

            }
            else
            {
//                System.out.println("skip nbits=" + nbits + " counter = " + Integer.toString(counter, 16));
                counter += shifter;
                nbits = Integer.bitCount(counter);
//                System.out.println("aftr nbits=" + nbits + " counter = " + Integer.toString(counter, 16));
            }
        }
        System.out.println("numberXtns = " + numberXtns);
    }

    public static int derive(int currentKey, int counter)
    {
        int newKey;
        
        newKey = currentKey;
        
        int shifter = 0X100000;
        
        while( (shifter >>= 1) > 0)
        {
            if( (shifter & counter) > 0)
            {
                System.out.println("current=" + currentKey + " data=" + Integer.toString(shifter, 16));
                newKey++;
            }
        }

        return newKey;
    }
}
