/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ebueno.dukpt;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import java.security.spec.AlgorithmParameterSpec;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.DESedeKeySpec;
import javax.crypto.spec.IvParameterSpec;
import org.apache.commons.codec.binary.Hex;
import org.bouncycastle.asn1.cms.CMSObjectIdentifiers;

/**
 *
 * @author Erico
 */
public class Crypto
{
    public static byte[] tripleDesCBCEncrypt(byte[] key,
            byte[] iv,
            boolean pad,
            byte[] cData)
    {
        try
        {
            byte[] desKey = new byte[24];

            if (key.length == 16)
            {
                System.arraycopy(key, 0, desKey, 0, 16);    // Make it a triple length key
                System.arraycopy(key, 0, desKey, 16, 8);
            }
            else if (key.length == 24)
            {
                System.arraycopy(key, 0, desKey, 0, 24); // Already triple length
            }

            DESedeKeySpec keySpec = new DESedeKeySpec(desKey);

            SecretKey secretKey = SecretKeyFactory.getInstance("DESede").generateSecret(keySpec);

            AlgorithmParameterSpec paramSpec = new IvParameterSpec(iv);

            Cipher cipher;
            if (pad)
            {
                cipher = Cipher.getInstance("DESede/CBC/PKCS5Padding", "BC");
            }
            else
            {
                cipher = Cipher.getInstance("DESede/CBC/NoPadding", "BC");
            }

            cipher.init(Cipher.ENCRYPT_MODE, secretKey, paramSpec);

            byte[] encryptedData = cipher.doFinal(cData);

            return encryptedData;
        }
        catch (Exception e)
        {
            System.out.println("Exception ;" + e.getMessage());
            throw new RuntimeException(e);
        }
    }

    public static byte[] tripleDesCBCDecrypt(byte[] key,
            byte[] iv,
            boolean pad,
            byte[] eData)
    {
        try
        {
            byte[] desKey = new byte[24];

            if (key.length == 16)
            {
                System.arraycopy(key, 0, desKey, 0, 16);    // Make it a triple length key
                System.arraycopy(key, 0, desKey, 16, 8);
            }
            else if (key.length == 24)
            {
                System.arraycopy(key, 0, desKey, 0, 24); // Already triple length
            }

            DESedeKeySpec keySpec = new DESedeKeySpec(desKey);

            SecretKey secretKey = SecretKeyFactory.getInstance("DESede").generateSecret(keySpec);

            AlgorithmParameterSpec paramSpec = new IvParameterSpec(iv);

            Cipher cipher;

            if (pad)
            {
                cipher = Cipher.getInstance("DESede/CBC/PKCS5Padding", "BC");
            }
            else
            {
                cipher = Cipher.getInstance("DESede/CBC/NoPadding", "BC");
            }

            cipher.init(Cipher.DECRYPT_MODE, secretKey, paramSpec);

            byte[] decryptedData = cipher.doFinal(eData);

            return decryptedData;
        }
        catch (Exception e)
        {
            System.out.println("Exception ;" + e.getMessage());
            throw new RuntimeException(e);
        }
    }

    public static byte[] tripleDesECBEncrypt(byte[] key,
            byte[] data)
    {
        try
        {
            byte[] desKey = new byte[24];

            if (key.length == 16)
            {
                System.arraycopy(key, 0, desKey, 0, 16);    // Make it a triple length key
                System.arraycopy(key, 0, desKey, 16, 8);
            }
            else if (key.length == 24)
            {
                System.arraycopy(key, 0, desKey, 0, 24); // Already triple length
            }

            DESedeKeySpec keySpec = new DESedeKeySpec(desKey);

            SecretKey secretKey = SecretKeyFactory.getInstance("DESede").generateSecret(keySpec);

            Cipher cipher;

            cipher = Cipher.getInstance("DESede/ECB/PKCS5Padding", "BC");

            cipher.init(Cipher.ENCRYPT_MODE, secretKey);

            byte[] encryptedData = cipher.doFinal(data);

            return encryptedData;
        }
        catch (Exception e)
        {
            System.out.println("Exception ;" + e.getMessage());
            throw new RuntimeException(e);
        }
    }

    public static byte[] desEncrypt(byte[] key, byte[] data)
    {
        try
        {
            byte[] iv = new byte[8];

            AlgorithmParameterSpec paramSpec = new IvParameterSpec(iv);

            DESKeySpec keySpec = new DESKeySpec(key);

            SecretKey secretKey = SecretKeyFactory.getInstance("DES").generateSecret(keySpec);

            Cipher cipher = Cipher.getInstance("DES/CBC/PKCS5Padding");

            cipher.init(Cipher.ENCRYPT_MODE, secretKey, paramSpec);

            byte[] encryptedData = cipher.doFinal(data);

            return encryptedData;
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }

    }

}
