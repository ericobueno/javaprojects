/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ebueno.dukpt;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.bouncycastle.util.Arrays;

/**
 *
 * @author Erico
 */
public final class DukptKey
{
    private byte[] key;
    private int lrc;

    public DukptKey() throws DecoderException
    {
        key = null;
        lrc = -1;  // Set it to erased invalid key
    }

    public DukptKey(String akey) throws Exception
    {
        lrc = -1;   // Set to invalid LRC

        if (akey.length() != 32)
        {
            System.out.println("Key length=" + akey.length());
            throw new Exception("Dukpt Key 1 must be 16 bytes");
        }

        this.key = Hex.decodeHex(akey.toCharArray());

        calculateLrc();

    }

    public DukptKey(byte[] akey) throws Exception
    {
        lrc = -1;   // Set to invalid LRC

        if (akey.length != 16)
        {
            System.out.println("Key length=" + akey.length);
            throw new Exception("Dukpt Key 2 must be 16 bytes");
        }

        this.key = akey;

        calculateLrc();

    }

    public byte[] getKey()
    {
        return key;
    }

    public String getKeyString()
    {
        return Utility.toHex(key);
        
    }

    public byte[] getLeftKey()
    {
        return Arrays.copyOfRange(key, 0, 8);
    }

    public byte[] getRightKey()
    {
        return Arrays.copyOfRange(key, 8, 16);
    }

    @Override
    public String toString()
    {
        return Utility.toHex(key);
    }

    public void calculateLrc()
    {
        byte b = 0x0;
        try
        {
            for (int i = 0; i < key.length; i++)
            {
                b += key[i];
                b &= 0XFF;
            }
            lrc = 0;
            lrc = ((b ^ 0XFF) + 1) & 0XFF;
        }
        catch (Exception ex)
        {
            throw new RuntimeException(ex);
        }
    }

    public void invalidateLrc()
    {
        lrc = -1;
    }

    public boolean isValid()
    {
        return lrc != -1;
    }

}
