/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ebueno.dukpt;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.bouncycastle.util.Arrays;

/**
 *
 * @author Erico
 */
public class KeySerialNumber
{

    private static final byte[] _001FFFFF = new byte[]
    {
        (byte) 0X00, (byte) 0X1F, (byte) 0XFF, (byte) 0XFF
    };

    private static final byte[] _E0 = new byte[]
    {
        (byte) 0XE0
    };

    private static final byte[] _E00000 = new byte[]
    {
        (byte) 0XE0, 0X00, 0X00
    };

    private final byte[] paddedKsn;
    private final byte[] unpaddedKsn;
    private final byte[] originalKsn;
    private final byte[] permanentKsn;
    private final byte[] baseKsn;
    private final byte[] keySetId;

    private final byte[] trsmId;
    private final int transactionCounter;

    /**
     * Create a new instance of the KeySerialNumber class
     *
     * @param ksn Key Serial Number to initialize with
     * @param ksnDescriptor KSN descriptor
     */
    public KeySerialNumber(String ksn, String ksnDescriptor) throws DecoderException
    {
        if (ksn.length() == 16)
        {
            ksn = "FFFF" + ksn;
        }

        paddedKsn = Hex.decodeHex(ksn.toCharArray());

        unpaddedKsn = stripKsn(paddedKsn);

        originalKsn = Utility.andArrayOffset(unpaddedKsn, _E00000, unpaddedKsn.length - 3);

        permanentKsn = Utility.andArrayOffset(paddedKsn, _E00000, 7);

        baseKsn = Arrays.copyOfRange(permanentKsn, 0, 8);

        int n = Integer.parseInt(ksnDescriptor.substring(0, 1));

        keySetId = Arrays.copyOfRange(paddedKsn, 0, n);

        int m = Integer.parseInt(ksnDescriptor.substring(2, 3));

        trsmId = Arrays.copyOfRange(baseKsn, n, n + m);

        byte[] counterBytes = Arrays.copyOfRange(paddedKsn, 6, 10);

        counterBytes = Utility.andArray(counterBytes, _001FFFFF);

        transactionCounter = Utility.fromByteArray(counterBytes);

//        System.out.println("trasactionCounter = " + transactionCounter );
    }

    /**
     * Strips the padding off the KSN
     *
     * @param ksn Padded KSN
     * @return Unpadded KSN
     */
    public static byte[] stripKsn(byte[] padded)
    {

        return Arrays.copyOfRange(padded, 2, 10);
    }

    /**
     * Base Key ID
     *
     * @return Base Key ID
     */
    public byte[] getKeySetId()
    {
        return keySetId;
    }

    public byte[] getBaseKsn()
    {
        return baseKsn;
    }

    public byte[] getPermanentKsn()
    {
        return permanentKsn;
    }

    /**
     * Get Padded KSN
     *
     * @return Padded KSN
     */
    public byte[] getPaddedKsn()
    {
        return paddedKsn;
    }

    /**
     * Get Transaction Counter
     *
     * @return Transaction Counter
     */
    public int getTransactionCounter()
    {
        return transactionCounter;
    }

    /**
     * Get TRSM ID
     *
     * @return TRSM ID
     */
    public byte[] getTrsmId()
    {
        return trsmId;
    }

    /**
     * Get unpadded KSN
     *
     * @return Unpadded KSN
     */
    public byte[] getUnpaddedKsn()
    {
        return unpaddedKsn;
    }

    public byte[] getOriginalKsn()
    {

        return originalKsn;
    }

    /**
     * Pack the KSN into a Hex string that can be put in an ISO message
     *
     * @return Packed KSN
     * @throws org.apache.commons.codec.DecoderException
     */
    public byte[] pack() throws DecoderException
    {
        return paddedKsn;
    }

}
