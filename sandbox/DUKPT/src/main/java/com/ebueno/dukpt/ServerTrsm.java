/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ebueno.dukpt;

import org.apache.commons.codec.DecoderException;

/**
 *
 * @author Erico
 */
public class ServerTrsm
{
    private DukptKey bdk;

    public ServerTrsm(String theBdk) throws Exception
    {
        bdk = new DukptKey(theBdk);
    }

    public ServerTrsm(DukptKey bdk)
    {
        this.bdk = bdk;
    }

    public DukptKey getPinKey(String theKsn) throws DecoderException, Exception
    {
        KeySerialNumber ksn = new KeySerialNumber(theKsn, "503");

        DukptKey ipek = Dukpt.getIpek(bdk, ksn);

        DukptKey derivedKey = Dukpt.DeriveKey(ipek, ksn);

        DukptKey pinKey = Dukpt.getPinEncryptionKey(derivedKey);

        return pinKey;
    }

    public DukptKey getMacKey(String theKsn) throws DecoderException, Exception
    {
        KeySerialNumber ksn = new KeySerialNumber(theKsn, "503");

        DukptKey ipek = Dukpt.getIpek(bdk, ksn);

        DukptKey derivedKey = Dukpt.DeriveKey(ipek, ksn);

        DukptKey macKey = Dukpt.getMacRequestKey(derivedKey);

        return macKey;
    }

    public DukptKey getDataKey(String theKsn) throws DecoderException, Exception
    {
        KeySerialNumber ksn = new KeySerialNumber(theKsn, "503");

        DukptKey ipek = Dukpt.getIpek(bdk, ksn);

        DukptKey derivedKey = Dukpt.DeriveKey(ipek, ksn);

        DukptKey dataKey = Dukpt.getDataEncryptionKey(derivedKey);

        return dataKey;
    }

    public String decrypt(DukptEncryptedPackage pkg) throws Exception
    {
        byte[] iv = new byte[8];

        String cData = null;

        DukptKey dk = getDataKey(pkg.getKsn());

        System.out.println("\nKSN: " + pkg.getKsn() + " Derives ---> Key: " + dk.getKeyString());

        byte[] eData = Utility.hexStringToByteArray(pkg.getEncryptedData());

        cData = Utility.toString(Crypto.tripleDesCBCDecrypt(dk.getKey(), iv, false, eData));

        return cData;
    }

    public String decrypt(String ksn, String eData) throws Exception
    {
        byte[] iv = new byte[8];

        String cData = null;

        DukptKey dk = getDataKey(ksn);

        System.out.println("\nKSN: " + ksn + " Derives ---> Key: " + dk.getKeyString());

        byte[] eDataBuf = Utility.hexStringToByteArray(eData);

        cData = Utility.toString(Crypto.tripleDesCBCDecrypt(dk.getKey(), iv, false, eDataBuf));

        int trackEnd = cData.indexOf('?');

        String track = cData.substring(0, trackEnd + 1);

        return track;
    }

}
