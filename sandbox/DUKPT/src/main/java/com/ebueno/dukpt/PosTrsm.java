/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ebueno.dukpt;

import org.apache.commons.codec.binary.Hex;

/**
 *
 * @author Erico
 */
public class PosTrsm
{
    private KeySerialNumber ksn;    // Remains for the life od the Terminal(TRSM)

    private int shiftRegister;          // 21 bits
    private int encryptionCounter;      // 0X000000 to 0X100000
    private int testCounter;

    private final DukptKey[] fkr = new DukptKey[22]; // Future key registers 1 to 21 - 0 is not used

    public PosTrsm()
    {
    }

    public void loadInitialKey(DukptKey ipek, KeySerialNumber terminalKsn) throws Exception
    {
        byte[] shiftr;
        DukptKey newKey;
        int fkrNumber;
        byte[] modKsn;

        this.ksn = terminalKsn;

        System.out.println("");

        /*      
         System.out.println("Padded  : " + Utility.toHex(ksn.getPaddedKsn()));

         System.out.println("Unpadded: " + Utility.toHex(ksn.getUnpaddedKsn()));

         System.out.println("Base KSN: " + Utility.toHex(ksn.getBaseKsn()));

         System.out.println("Original KSN: " + Utility.toHex(ksn.getOriginalKsn()));

         System.out.println("Key Set Id; " + Utility.toHex(ksn.getKeySetId()));

         System.out.println("trsm id: " + Utility.toHex(ksn.getTrsmId()));

         System.out.println("xtn counter: " + ksn.getTransactionCounter());
         */
        System.out.println("POS Key Injection for KSN : " + Utility.toHex(ksn.getPermanentKsn()));
        System.out.println("POS Key Injection for IPEK: " + ipek);
        System.out.println("");
        
        byte[] originalKsn = ksn.getOriginalKsn();

        encryptionCounter = 0;        // The beginning of everything
        testCounter = 0;
        shiftRegister = 0X100000;

        while (shiftRegister > 0)
        {
            shiftr = Utility.toByteArray(shiftRegister);

            modKsn = Utility.orArrayOffset(originalKsn, shiftr, originalKsn.length - 4); // ksn + shift register

            newKey = Dukpt.deriveNextKey(ipek, modKsn); // All 21 initial keys are derived from the IPEK

            fkrNumber = getFkrNumber(shiftRegister);

            fkr[fkrNumber] = newKey;

            System.out.println("fkr[" + fkrNumber + "] = " + newKey.getKeyString() + " " + Integer.toString(shiftRegister, 16));

            shiftRegister >>= 1;    // shift right
        }

        encryptionCounter += 1;     // We are ready for the first encryption

        if (encryptionCounter > 0x177777)   // Did it overflow
        {
            encryptionCounter = 0;          // Zero it out
            throw new Exception("This device expired");
        }
    }

    public SessionKeyParameters getPinKeyParameters() throws Exception
    {
        SessionKeyParameters pinKeyParameters;

        SessionKeyParameters sessionKeyParameters = getSessionKeyParameters();

        DukptKey pinKey = Dukpt.getPinEncryptionKey(sessionKeyParameters.getKey());

        pinKeyParameters = new SessionKeyParameters(pinKey,
                sessionKeyParameters.getSessionKsn(),
                sessionKeyParameters.getEncryptionCounter());

        return pinKeyParameters;
    }

    public SessionKeyParameters getMacKeyParameters() throws Exception
    {
        SessionKeyParameters pinKeyParameters;

        SessionKeyParameters sessionKeyParameters = getSessionKeyParameters();

        DukptKey macKey = Dukpt.getMacRequestKey(sessionKeyParameters.getKey());

        pinKeyParameters = new SessionKeyParameters(macKey,
                sessionKeyParameters.getSessionKsn(),
                sessionKeyParameters.getEncryptionCounter());

        return pinKeyParameters;
    }

    public SessionKeyParameters getDataKeyParameters() throws Exception
    {
        SessionKeyParameters pinKeyParameters;

        SessionKeyParameters sessionKeyParameters = getSessionKeyParameters();

        DukptKey dataKey = Dukpt.getDataEncryptionKey(sessionKeyParameters.getKey());

        pinKeyParameters = new SessionKeyParameters(dataKey,
                sessionKeyParameters.getSessionKsn(),
                sessionKeyParameters.getEncryptionCounter());

        return pinKeyParameters;
    }

    public SessionKeyParameters getSessionKeyParameters() throws Exception
    {
        DukptKey currentKey = getNextEncryptionKey();

        String recipientKsn = getRecipientKsn();
        int counter = encryptionCounter;

        replaceKey(currentKey); // Replace used key

        return new SessionKeyParameters(currentKey, recipientKsn, counter);
    }

    private DukptKey getNextEncryptionKey() throws Exception
    {
        DukptKey currentKey = null;
        int fkrNumber;
        boolean done = false;

        while (!done)
        {
            shiftRegister = Integer.lowestOneBit(encryptionCounter);    // SetBit

            fkrNumber = getFkrNumber(shiftRegister);

            currentKey = fkr[fkrNumber];

            if (currentKey.isValid())
            {
                done = true;
            }
            else
            {
                System.out.println("Key at index=" + fkrNumber + " is invalid");

                encryptionCounter += shiftRegister;

                if (encryptionCounter > 0x177777)   // Did it overflow
                {
                    encryptionCounter = 0;          // Zero it out
                    throw new Exception("This device expired");
                }
            }
        }       // End while

        return currentKey;
    }

    private void replaceKey(DukptKey currentKey) throws Exception
    {
        byte[] modifiedKsn;

        int nbits = Integer.bitCount(encryptionCounter);

        if (nbits >= 10)
        {
            int fkrNumber = getFkrNumber(shiftRegister);

            fkr[fkrNumber] = new DukptKey();    // Put invalid key here HERE HERE

            encryptionCounter += shiftRegister;

            if (encryptionCounter > 0x177777)   // Did it overflow
            {
                encryptionCounter = 0;          // Zero it out
                throw new Exception("This device expired");
            }
        }
        else
        {
            byte[] counter;
            DukptKey newKey;
            int fkrNumber;
            int m;
            int n;

            byte[] originalKsn = ksn.getOriginalKsn();

            while ((shiftRegister >>= 1) > 0)
            {
                fkrNumber = getFkrNumber(shiftRegister);

                /*
                 m = n + 2 ^ ( 21 - r )
                 where
                 n = transaction counter
                 r = FKR number
                 */
                m = encryptionCounter + (1 << (21 - fkrNumber));

                counter = Utility.toByteArray(m);

                modifiedKsn = Utility.orArrayOffset(originalKsn, counter, originalKsn.length - 4);

                newKey = Dukpt.deriveNextKey(currentKey, modifiedKsn);

                fkr[fkrNumber] = newKey;
            }

            // New Key-4
            encryptionCounter += 1;

            if (encryptionCounter > 0x177777)   // Did it overflow
            {
                encryptionCounter = 0;          // Zero it out
                throw new Exception("This device expired");
            }
        }
    }

    public String getRecipientKsn()
    {
        byte[] permanentKsn = ksn.getPermanentKsn();

        byte[] counter = Utility.toByteArray(encryptionCounter);

        byte[] recepientKsn = Utility.orArrayOffset(permanentKsn, counter, permanentKsn.length - 4);

        return Hex.encodeHexString(recepientKsn);
    }

    public static int getFkrNumber(int shiftRegister)
    {
        int fkrNumber = 0;

        switch (shiftRegister)
        {
            case 0X100000:
                fkrNumber = 1;
                break;
            case 0X080000:
                fkrNumber = 2;
                break;
            case 0X040000:
                fkrNumber = 3;
                break;
            case 0X020000:
                fkrNumber = 4;
                break;
            case 0X010000:
                fkrNumber = 5;
                break;
            case 0X008000:
                fkrNumber = 6;
                break;
            case 0X004000:
                fkrNumber = 7;
                break;
            case 0X002000:
                fkrNumber = 8;
                break;
            case 0X001000:
                fkrNumber = 9;
                break;
            case 0X000800:
                fkrNumber = 10;
                break;
            case 0X000400:
                fkrNumber = 11;
                break;
            case 0X000200:
                fkrNumber = 12;
                break;
            case 0X000100:
                fkrNumber = 13;
                break;
            case 0X000080:
                fkrNumber = 14;
                break;
            case 0X000040:
                fkrNumber = 15;
                break;
            case 0X000020:
                fkrNumber = 16;
                break;
            case 0X000010:
                fkrNumber = 17;
                break;
            case 0X000008:
                fkrNumber = 18;
                break;
            case 0X000004:
                fkrNumber = 19;
                break;
            case 0X000002:
                fkrNumber = 20;
                break;
            case 0X000001:
                fkrNumber = 21;
                break;
        }

        return fkrNumber;
    }

    public DukptEncryptedPackage encrypt(String readerData) throws Exception
    {
        SessionKeyParameters sessionKeyParameters;

        sessionKeyParameters = getDataKeyParameters();

        int counter = sessionKeyParameters.getEncryptionCounter();

        System.out.println("\nPOS KSN: " + sessionKeyParameters.getSessionKsn()
                + " ---> Key: " + sessionKeyParameters.getKey().getKeyString());

        DukptKey key = sessionKeyParameters.getKey();

        byte[] cData = Utility.toByteArray(readerData);

        byte[] iv = new byte[8];

        byte[] eData = Crypto.tripleDesCBCEncrypt(key.getKey(), iv, true, cData);

        String eDataS = Hex.encodeHexString(eData);

        DukptEncryptedPackage pkg = new DukptEncryptedPackage(sessionKeyParameters.getSessionKsn(), eDataS);

        return pkg;
    }

}
