package com.ebueno.dukpt;

/**
 *
 * @author Erico
 */
public class SessionKeyParameters
{
    private final DukptKey key;
    private final String SessionKsn;
    private final int encryptionCounter;

    public SessionKeyParameters(DukptKey key, String SessionKsn, int encryptioCounter)
    {
        this.key = key;
        this.SessionKsn = SessionKsn;
        this.encryptionCounter = encryptioCounter;
    }

    public DukptKey getKey()
    {
        return key;
    }

    public String getSessionKsn()
    {
        return SessionKsn;
    }

    public int getEncryptionCounter()
    {
        return encryptionCounter;
    }
    
    public DukptKey getPinEncryptionKey() throws Exception
    {
        return Dukpt.getPinEncryptionKey(key);
    }
    
    public DukptKey getMacRequestKey() throws Exception
    {
        return Dukpt.getMacRequestKey(key);
    }
    
    public DukptKey getDataEncryptioKey() throws Exception
    {
        return Dukpt.getDataEncryptionKey(key);
    }
}
