/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ebueno.dukpt;

/**
 *
 * @author Erico
 */
public class DukptEncryptedPackage
{
    private String ksn;
    private String encryptedData;

    public DukptEncryptedPackage(String ksn, String eData)
    {
        this.ksn = ksn;
        this.encryptedData = eData;
        System.out.println("ksnlen= " + ksn.length());
        System.out.println("eDataLen= " + eData.length());
    }

    public String getKsn()
    {
        return ksn;
    }

    public void setKsn(String ksn)
    {
        this.ksn = ksn;
    }

    public String getEncryptedData()
    {
        return encryptedData;
    }

    public void setEncryptedData(String encryptedData)
    {
        this.encryptedData = encryptedData;
    }
}
