/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ebueno.dukpt;

import java.io.PrintStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Erico
 */
public final class IdtechBlob extends Blob
{
    private byte cardEncodeType;
    private String trackStatus;

    private int t1Len;
    private int t2Len;
    private int t3Len;

    private byte maskedDataStatus;
    private byte hashDataStatus;

    private String t1MaskedData;
    private String t2MaskedData;
    private String t3MaskedData;

    private String t1Hash;
    private String t2Hash;
    private String t3Hash;

    public IdtechBlob()
    {
        super();
    }

    public IdtechBlob(String blob) throws Exception
    {
        super(blob);
        parse();
    }

    /**
     *
     */
    @Override
    public void parse() throws Exception
    {
        String tmp;

        currentIndex = 6;   // Point card encode type

        tmp = blob.substring(currentIndex, currentIndex + 2);
        currentIndex += 2;
        byte[] cet = Utility.hexStringToByteArray(tmp);
        cardEncodeType = cet[0];

        String cardType;

        switch (cardEncodeType)
        {
            case (byte) 0X80:
                cardType = "ISO/ABA";
                break;
            case (byte) 0X81:
                cardType = "AAMVA";
                break;
            case (byte) 0X83:
                cardType = "Other";
                break;
            case (byte) 0X84:
                cardType = "Raw";
                break;
            default:
                cardType = "Unknown";
                break;
        }
        
        PrintStream printf = System.out.printf("%s card detected (%02X)\n",
                cardType, cardEncodeType);
        
        if (cardEncodeType != (byte) 0x80)
        {

            throw new Exception("Card type " + cardType + " is not supported");
        }

        currentIndex = 10;   // Point to t1 len

        t1Len = Integer.parseInt(blob.substring(currentIndex, 
                currentIndex + 2), 16);
        
        currentIndex += 2;

        int nblks = t1Len / 8;
        if (t1Len % 8 > 0)
        {
            nblks += 1;
        }
        et1Len = nblks * 8;

        t2Len = Integer.parseInt(blob.substring(currentIndex,
                currentIndex + 2), 16);
        
        currentIndex += 2;

        nblks = t2Len / 8;
        if (t2Len % 8 > 0)
        {
            nblks += 1;
        }
        et2Len = nblks * 8;

        t3Len = Integer.parseInt(blob.substring(currentIndex,
                currentIndex + 2), 16);
        
        currentIndex += 2;

        nblks = t3Len / 8;
        if (t3Len % 8 > 0)
        {
            nblks += 1;
        }
        et3Len = nblks * 8;

        tmp = blob.substring(currentIndex, currentIndex + 2);
        currentIndex += 2;
        byte[] mds = Utility.hexStringToByteArray(tmp);
        maskedDataStatus = mds[0];

        tmp = blob.substring(currentIndex, currentIndex + 2);
        currentIndex += 2;
        byte[] hds = Utility.hexStringToByteArray(tmp);
        hashDataStatus = hds[0];

        if ((maskedDataStatus & 0X01) > 0)
        {
            t1MaskedData = blob.substring(currentIndex, currentIndex + t1Len);
            currentIndex += t1Len;
        }

        if ((maskedDataStatus & 0X02) > 0)
        {
            t2MaskedData = blob.substring(currentIndex, currentIndex + t2Len);
            currentIndex += t2Len;
        }

        if ((maskedDataStatus & 0X04) > 0)
        {
            t3MaskedData = blob.substring(currentIndex, currentIndex + t3Len);
            currentIndex += t3Len;
        }

        if ((hashDataStatus & 0X01) > 0)
        {
            t1Edata = blob.substring(currentIndex, currentIndex + et1Len * 2);
            currentIndex += et1Len * 2;
        }

        if ((hashDataStatus & 0X02) > 0)
        {
            t2Edata = blob.substring(currentIndex, currentIndex + et2Len * 2);
            currentIndex += et2Len * 2;
        }

        if ((hashDataStatus & 0X04) > 0)
        {
            t3Edata = blob.substring(currentIndex, currentIndex + et3Len * 2);
            currentIndex += et3Len * 2;
        }

        if ((hashDataStatus & 0X08) > 0)
        {
            t1Hash = blob.substring(currentIndex, currentIndex + 40);
            currentIndex += 40;
        }

        if ((hashDataStatus & 0X10) > 0)
        {
            t2Hash = blob.substring(currentIndex, currentIndex + 40);
            currentIndex += 40;
        }

        if ((hashDataStatus & 0X20) > 0)
        {
            t3Hash = blob.substring(currentIndex, currentIndex + 40);
            currentIndex += 40;
        }

        ksn = blob.substring(currentIndex, currentIndex + 20);
        currentIndex += 20;
    }

}
