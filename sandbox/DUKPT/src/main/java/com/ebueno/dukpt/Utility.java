/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ebueno.dukpt;

import static com.google.common.base.Preconditions.*;
import java.nio.ByteBuffer;
import java.security.spec.AlgorithmParameterSpec;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.DESedeKeySpec;
import javax.crypto.spec.IvParameterSpec;
import org.apache.commons.codec.binary.Hex;
import org.bouncycastle.util.Arrays;

/**
 *
 * @author Erico
 */
public class Utility
{

    public static byte[] andArray(byte[] input, byte[] mask)
    {
        return andArrayOffset(input, mask, 0);
    }

    public static byte[] andArrayOffset(byte[] dataIn, byte[] mask, int offset)
    {
        try
        {
            byte[] dataOut = Arrays.clone(dataIn);

            int endPoint = offset + mask.length;

            for (int i = offset; i < dataOut.length && i < endPoint; i++)
            {
                byte a = dataIn[i];
                byte b = mask[i - offset];
                dataOut[i] = (byte) (a & b);
            }
            return dataOut;
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
    }

    public static byte[] orArrayOffset(byte[] dataIn, byte[] mask, int offset)
    {
        try
        {
            byte[] dataOut = Arrays.clone(dataIn);

            int endPoint = offset + mask.length;

            for (int i = offset; i < dataOut.length && i < endPoint; i++)
            {
                byte a = dataIn[i];
                byte b = mask[i - offset];
                dataOut[i] = (byte) (a | b);
            }
            return dataOut;
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
    }

    public static byte[] xorArray(byte[] dataIn, byte[] mask)
    {
        return xorArrayOffset(dataIn, mask, 0);
    }

    public static byte[] xorArrayOffset(byte[] dataIn, byte[] mask, int offset)
    {
        try
        {
            byte[] dataOut = Arrays.clone(dataIn);

            int endPoint = offset + mask.length;

            for (int i = offset; i < dataOut.length && i < endPoint; i++)
            {
                byte a = dataIn[i];
                byte b = mask[i - offset];
                dataOut[i] = (byte) (a ^ b);
            }
            return dataOut;
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
    }

    public static String padLeft(String data, int length, char padChar)
    {
        int remaining = length - data.length();

        String newData = data;
        for (int i = 0; i < remaining; i++)
        {
            newData = padChar + newData;
        }
        return newData;
    }

    public static String shiftRightHexString(String str)
    {
        long r = Long.parseLong(str, 16);
        r >>= 1;
        String shifted = Long.toString(r, 16);

        return padLeft(shifted, str.length(), '0');
    }

    /* Begin Erico Bueno routines */
    private static String digits = "0123456789ABCDEF";

    /**
     * Return length many bytes of the passed in byte array as a hex string.
     *
     * @param data the bytes to be converted.
     * @param length the number of bytes in the data block to be converted.
     * @return a hex representation of length bytes of data.
     */
    public static String toHex(byte[] data, int length)
    {
        StringBuffer buf = new StringBuffer();

        for (int i = 0; i != length; i++)
        {
            int v = data[i] & 0xff;

            buf.append(digits.charAt(v >> 4));

            buf.append(digits.charAt(v & 0xf));
        }
        
        buf.append(" (").append(length).append(")");
        
        return buf.toString();
    }

    public static String toHexSpaced(byte[] data, int length)
    {
        StringBuffer buf = new StringBuffer();

        for (int i = 0; i != length; i++)
        {
            int v = data[i] & 0xff;

            buf.append(digits.charAt(v >> 4));

            buf.append(digits.charAt(v & 0xf));

            buf.append(' ');
        }

        buf.append(" (").append(length).append(")");

        return buf.toString();
    }

    /**
     * Return the passed in byte array as a hex string.
     *
     * @param data the bytes to be converted.
     * @return a hex representation of data.
     */
    public static String toHex(byte[] data)
    {
        return toHex(data, data.length);
    }

    public static String toHexSpaced(byte[] data)
    {
        return toHexSpaced(data, data.length);
    }

    /**
     * Convert a byte array of 8 bit characters into a String.
     *
     * @param bytes the array containing the characters
     * @param length the number of bytes to process
     * @return a String representation of bytes
     */
    public static String toString(byte[] bytes, int length)
    {
        char[] chars = new char[length];

        for (int i = 0; i != chars.length; i++)
        {
            chars[i] = (char) (bytes[i] & 0xff);
        }
        return new String(chars);
    }

    /**
     * Convert a byte array of 8 bit characters into a String.
     *
     * @param bytes the array containing the characters
     * @return a String representation of bytes
     */
    public static String toString(byte[] bytes)
    {
        return toString(bytes, bytes.length);
    }

    /**
     * Convert the passed in String to a byte array by taking the bottom 8 bits
     * of each character it contains.
     *
     * @param string the string to be converted
     * @return a byte array representation
     */
    public static byte[] toByteArray(String string)
    {
        byte[] bytes = new byte[string.length()];
        char[] chars = string.toCharArray();

        for (int i = 0; i != chars.length; i++)
        {
            bytes[i] = (byte) chars[i];
        }

        return bytes;
    }

    public static byte[] hexStringToByteArray(String hexstring)
    {
        int i = 0;

        if (hexstring == null || hexstring.length() <= 0)
        {
            return null;
        }

        String stringvector = "0123456789ABCDEF0123456789abcdef";

        byte[] bytevector =
        {
            0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
            0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F,
            0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
            0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F
        };

        byte[] out = new byte[hexstring.length() / 2];

        while (i < hexstring.length() - 1)
        {
            byte ch = 0x00;
            //Convert high nibble charater to a hex byte
            ch = (byte) (ch | bytevector[stringvector.indexOf(hexstring.charAt(i))]);

            ch = (byte) (ch << 4); //move this to the high bit

            //Convert the low nibble to a hexbyte
            ch = (byte) (ch | bytevector[stringvector.indexOf(hexstring.charAt(i + 1))]); //next hex value
            out[i / 2] = ch;
            i++;
            i++;
        }
        return out;
    }

    public static byte[] toByteArray(int value)
    {
        return ByteBuffer.allocate(4).putInt(value).array();
    }

    public static int fromByteArray(byte[] bytes)
    {
        int ret;
/*
        byte[] intArray = new byte[4];

        intArray = Utility.orArrayOffset(intArray, bytes, 1);

        System.out.println("Int len=" + bytes.length);

        ret = ByteBuffer.wrap(intArray).getInt();
*/
        ret = ByteBuffer.wrap(bytes).getInt();
        
        return ret;
    }

}
