/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ebueno.dukpt;

/**
 *
 * @author Erico
 */
public abstract class Blob
{
    protected String blob;
    
    protected String t1Edata;
    protected int et1Len;

    protected String t2Edata;
    protected int et2Len;

    protected String t3Edata;
    protected int et3Len;

    protected String ksn;

    protected int currentIndex;

    public Blob()
    {
        currentIndex = 0;
    }

    public Blob(String blob)
    {
        this.blob = blob;
        currentIndex = 0;
    }

    public void setBlob(String blob) throws Exception
    {
        this.blob = blob;
        currentIndex = 0;
        parse();
    }

    public abstract void parse() throws Exception;

    @Override
    public String toString()
    {
        return "Blob{" + blob + '}';
    }

    public String getKsn()
    {
        return ksn;
    }

    public String getEncryptedTrack1()
    {
        return t1Edata;
    }

    public String getEncryptedTrack2()
    {
        return t2Edata;
    }

    public String getEncryptedTrack3()
    {
        return t3Edata;
    }

    public String getBlob()
    {
        return blob;
    }

    public String getT1Edata()
    {
        return t1Edata;
    }

    public int getEt1Len()
    {
        return et1Len;
    }

    public String getT2Edata()
    {
        return t2Edata;
    }

    public int getEt2Len()
    {
        return et2Len;
    }

    public String getT3Edata()
    {
        return t3Edata;
    }

    public int getEt3Len()
    {
        return et3Len;
    }

}
