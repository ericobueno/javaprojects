/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ebueno.dukpt;

import java.security.spec.AlgorithmParameterSpec;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.DESedeKeySpec;
import javax.crypto.spec.IvParameterSpec;

import org.bouncycastle.util.Arrays;

/**
 *
 * @author Erico
 */
public class Dukpt
{
    private static final byte[] pinKeyMask = new byte[]
    {
        (byte) 0X00, (byte) 0X00, (byte) 0X00, (byte) 0X00,
        (byte) 0X00, (byte) 0X00, (byte) 0X00, (byte) 0XFF,
        (byte) 0X00, (byte) 0X00, (byte) 0X00, (byte) 0X00,
        (byte) 0X00, (byte) 0X00, (byte) 0X00, (byte) 0XFF,
    };

    private static final byte[] macRequestMask = new byte[]
    {
        (byte) 0X00, (byte) 0X00, (byte) 0X00, (byte) 0X00,
        (byte) 0X00, (byte) 0X00, (byte) 0XFF, (byte) 0X00,
        (byte) 0X00, (byte) 0X00, (byte) 0X00, (byte) 0X00,
        (byte) 0X00, (byte) 0X00, (byte) 0XFF, (byte) 0X00,
    };

    private static final byte[] dataKeyMask = new byte[]
    {
        (byte) 0X00, (byte) 0X00, (byte) 0X00, (byte) 0X00,
        (byte) 0X00, (byte) 0XFF, (byte) 0X00, (byte) 0X00,
        (byte) 0X00, (byte) 0X00, (byte) 0X00, (byte) 0X00,
        (byte) 0X00, (byte) 0XFF, (byte) 0X00, (byte) 0X00,
    };

    private static final byte[] _COCO = new byte[]
    {
        (byte) 0XC0, (byte) 0XC0, (byte) 0XC0, (byte) 0XC0,
        (byte) 0X00, (byte) 0X00, (byte) 0X00, (byte) 0X00,
        (byte) 0XC0, (byte) 0XC0, (byte) 0XC0, (byte) 0XC0,
        (byte) 0X00, (byte) 0X00, (byte) 0X00, (byte) 0X00,
    };

    private static final byte[] _E00000 = new byte[]
    {
        (byte) 0XE0, (byte) 0X00, (byte) 0X00
    };

    /**
     * Calculate the derived key from the KSN and BDK
     *
     * @param ksn Key Serial Number
     * @param bdk Base Derivation Key
     * @return Derived Key
     */
    public static DukptKey getPinEncryptionKey(DukptKey derivedKey) throws Exception
    {
        byte[] pinEncryptionKey = Utility.xorArray(derivedKey.getKey(), pinKeyMask);

        return new DukptKey(pinEncryptionKey);
    }

    /**
     * Calculate Message authentication, request or both ways
     *
     *
     * @param bdk Base Derivation Key
     * @param ksn Key Serial Number
     * @return MAC request Key
     */
    public static DukptKey getMacRequestKey(DukptKey derivedKey) throws Exception
    {
        byte[] macRequestKey = Utility.xorArray(derivedKey.getKey(), macRequestMask);

        return new DukptKey(macRequestKey);
    }

    /**
     * Calculate Data EncryptionKey, request or both ways
     *
     */
    public static DukptKey getDataEncryptionKey(DukptKey derivedKey) throws Exception
    {
        byte[] iv = new byte[8];

        byte[] variantKey = derivedKey.getKey();

        variantKey = Utility.xorArray(variantKey, dataKeyMask);

        byte[] variantKeyLeft = Arrays.copyOfRange(variantKey, 0, 8);
        byte[] variantKeyRight = Arrays.copyOfRange(variantKey, 8, 16);

        byte[] key_l = tripleDesEncrypt(variantKey, variantKeyLeft);

        byte[] key_r = tripleDesEncrypt(variantKey, variantKeyRight);

        return new DukptKey(Arrays.concatenate(key_l, key_r));
    }

    public static DukptKey DeriveKey(DukptKey key, KeySerialNumber ksn) throws Exception
    {
        DukptKey currentKey = key;

        byte[] modifiedKsn = ksn.getUnpaddedKsn();

        modifiedKsn = Utility.andArrayOffset(modifiedKsn, _E00000, modifiedKsn.length - 3);

        int counter = ksn.getTransactionCounter();

        int shiftRegister = 0X100000;

        while (shiftRegister > 0)
        {
            if ((counter & shiftRegister) > 0)
            {
                byte[] shiftr = Utility.toByteArray(shiftRegister);

                modifiedKsn = Utility.orArrayOffset(modifiedKsn, shiftr, modifiedKsn.length - 4);

                currentKey = deriveNextKey(currentKey, modifiedKsn);
            }
            shiftRegister >>= 1;    // Shift right one bit
        }

        return currentKey;
    }

    public static DukptKey deriveNextKey(DukptKey currentKey, byte[] modifiedKsn) throws Exception
    {
//        System.out.println("Derive nextKey=" + currentKey.getKeyString()
//                + " ksn=" + Utility.toHex(modifiedKsn));

        byte[] leftKey = currentKey.getLeftKey();

        byte[] rightKey = currentKey.getRightKey();

        byte[] newRightKey = Utility.xorArray(rightKey, modifiedKsn);

        newRightKey = desEncrypt(leftKey, newRightKey); // Encrypt the new right key with the left key

        newRightKey = Utility.xorArray(newRightKey, rightKey);

        byte[] maskedKey = Utility.xorArray(currentKey.getKey(), _COCO);

        currentKey = new DukptKey(maskedKey);

        leftKey = currentKey.getLeftKey();
        rightKey = currentKey.getRightKey();

        byte[] newLeftKey = Utility.xorArray(rightKey, modifiedKsn);

        newLeftKey = desEncrypt(leftKey, newLeftKey); // Encrypt the newLeft key with the left key

        newLeftKey = Utility.xorArray(newLeftKey, rightKey);

        return new DukptKey(Arrays.concatenate(newLeftKey, newRightKey));
    }

    /**
     * Calculate the Initial Pin Encryption Key from the current KSN
     *
     * @return IPEK
     */
    public static DukptKey getIpek(DukptKey bdk, KeySerialNumber ksn) throws Exception
    {
        byte[] iv = new byte[8];

        byte[] baseKsn = ksn.getBaseKsn();

//        System.out.println("baseKsn= " + Utility.toHex(baseKsn));

        byte[] leftIpek = tripleDesEncrypt(bdk.getKey(), baseKsn);

//        System.out.println("left= " + Utility.toHex(leftIpek));

        byte[] xorbdk = Utility.xorArray(bdk.getKey(), _COCO);

        byte[] rightIpek = tripleDesEncrypt(xorbdk, baseKsn);

//        System.out.println("right= " + Utility.toHex(rightIpek));

        return new DukptKey(Arrays.concatenate(leftIpek, rightIpek));

    }

    private static byte[] desEncrypt(byte[] key, byte[] data)
    {
        try
        {
            byte[] iv = new byte[8];

            AlgorithmParameterSpec paramSpec = new IvParameterSpec(iv);

            DESKeySpec keySpec = new DESKeySpec(key);

            SecretKey secretKey = SecretKeyFactory.getInstance("DES").generateSecret(keySpec);

            Cipher cipher = Cipher.getInstance("DES/CBC/NoPadding", "BC");

            cipher.init(Cipher.ENCRYPT_MODE, secretKey, paramSpec);

            byte[] encryptedData = cipher.doFinal(data);

            return encryptedData;
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
    }

    private static byte[] tripleDesEncrypt(byte[] key, byte[] data)
    {
        try
        {
            byte[] iv = new byte[8];

            byte[] desKey = new byte[24];

            if (key.length == 16)
            {
                System.arraycopy(key, 0, desKey, 0, 16);    // Make it a triple length key
                System.arraycopy(key, 0, desKey, 16, 8);
            }
            else if (key.length == 24)
            {
                System.arraycopy(key, 0, desKey, 0, 24); // Already triple length
            }

            DESedeKeySpec keySpec = new DESedeKeySpec(desKey);

            SecretKey secretKey = SecretKeyFactory.getInstance("DESede").generateSecret(keySpec);

            AlgorithmParameterSpec paramSpec = new IvParameterSpec(iv);

            Cipher cipher;

            cipher = Cipher.getInstance("DESede/CBC/NoPadding", "BC");

            cipher.init(Cipher.ENCRYPT_MODE, secretKey, paramSpec);

            byte[] encryptedData = cipher.doFinal(data);

            return encryptedData;
        }
        catch (Exception e)
        {
            System.out.println("Exception ;" + e.getMessage());
            throw new RuntimeException(e);
        }
    }

}
