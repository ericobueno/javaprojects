/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ebueno.dukpt;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.security.Security;
import org.apache.commons.codec.DecoderException;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

/**
 *
 * @author Erico
 */
public class Main
{

    /**
     * @param args the command line arguments
     * @throws org.apache.commons.codec.DecoderException
     */
    public static void main(String[] args) throws DecoderException, Exception
    {
        Security.addProvider(new BouncyCastleProvider());
       
//        testServerSide();
//        testTerminal();
//        testPlug();
        testIdtech();
//        testBlob();
//        testFullCircle();
    }

    static void testBlob() throws Exception
    {
        Blob blob = new IdtechBlob("02B701801F412400039B%*5121********4602^BUENO/ERICO ^*******************************?*;5121********4602=****************?*1966FDF8D4A592B77CC01FCF3FFB272AD9B228B51B3F3D4A24F8C0C2FBAE8AD0B2929B3E073A2AE4F026DAD8E5F6E002FDABEC4D7F76A68D5424DF7B449844DE3402A3A43CF69B85DA37B3595EF1ABD22DB6A8BB1D08A0F6DF13B4867BBA1C356FD0574FBE645CF894D4979A300F01A1202CFE2642432F7AF15510644F692F94ED450997C299505D7C43BDF596EACA50F8347E7FCE714B1C629949005C000560005C5DB703");
        System.out.println("t1Edata=" + blob.getEncryptedTrack1());
        System.out.println("t2Edata=" + blob.getEncryptedTrack2());
        System.out.println("t3Edata=" + blob.getEncryptedTrack3());
        System.out.println("ksn=" + blob.getKsn());
    }

    static void testPlug() throws Exception
    {
        DukptKey bdk = new DukptKey("0123456789ABCDEFFEDCBA9876543210");

        ServerTrsm serverTrsm = new ServerTrsm(bdk);

        String cData;

        try
        {
            DukptEncryptedPackage ep = new DukptEncryptedPackage("629949005C0005600055",
                    "7E043ED43FF06785591698D188BBD0D7345BF9D657A92B13CE7BDDADCE683C6276886D29F8B8142AD4FB9285E720598ADC5749439FFFEB3F74CA9F22F6BD1795DF3A58B251894231");

            cData = serverTrsm.decrypt(ep);

            System.out.println("\nDecrypted data :" + cData + "-len=" + cData.length());
        }
        catch (Exception ex)
        {
            System.out.println(ex.getMessage());
        }
        System.out.println("Done!");
    }

    static void testIdtech() throws Exception
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        DukptKey bdk = new DukptKey("0123456789ABCDEFFEDCBA9876543210");

        ServerTrsm serverTrsm = new ServerTrsm(bdk);

        boolean done = false;
        String readerData;

        String cData;
        IdtechBlob blob = new IdtechBlob();

        while (done == false)
        {
            try
            {
                System.out.println("\n=====================================================================================\n");
                System.out.print("\nSwipe your card1 : ");

                readerData = reader.readLine();

                if (readerData.equalsIgnoreCase("end"))
                {
                    break;

                }

                blob.setBlob(readerData);

                System.out.println("\nDecrypt T1: " + blob.getEncryptedTrack1());

                cData = serverTrsm.decrypt(blob.getKsn(), blob.getEncryptedTrack1());

                System.out.println("\nDecrypted T1: " + cData);

                System.out.println("\n-------------------------------------");

                System.out.println("\nDecrypt T2: " + blob.getEncryptedTrack2());

                cData = serverTrsm.decrypt(blob.getKsn(), blob.getEncryptedTrack2());

                System.out.println("\nDecrypted T2: " + cData);

                if (blob.getEt3Len() > 0)
                {
                    System.out.println("\n-------------------------------------");

                    System.out.println("\nDecrypt T3: " + blob.getEncryptedTrack3());

                    cData = serverTrsm.decrypt(blob.getKsn(), blob.getEncryptedTrack3());

                    System.out.println("\nDecrypted T3: " + cData);
                }
            }
            catch (Exception ex)
            {
                System.out.println("Error detected: " + ex.getMessage());
            }
        }
        reader.close();
        System.out.println("Done!");
    }

    static void testFullCircle() throws Exception
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        DukptKey bdk = new DukptKey("0123456789ABCDEFFEDCBA9876543210");

        ServerTrsm serverTrsm = new ServerTrsm(bdk);

        PosTrsm posTrsm = new PosTrsm();

        KeySerialNumber ksn = new KeySerialNumber("FFFF9876543210E00008", "503");

        DukptKey ipek = Dukpt.getIpek(bdk, ksn);

        posTrsm.loadInitialKey(ipek, ksn);

        boolean done = false;
        String readerData = null;
        String cData;

        while (done == false)
        {
            try
            {
                System.out.println("\n=====================================================================================\n");
                System.out.print("\nSwipe your card : ");
                readerData = reader.readLine();

                if (readerData.equalsIgnoreCase("end"))
                {
                    break;

                }

                int t1Start = readerData.indexOf('%');
                int t1End = readerData.indexOf('?');

                String track1 = readerData.substring(t1Start, t1End + 1);

                System.out.println("\nTrack1 : " + track1 + " len= " + track1.length());

                DukptEncryptedPackage ep = posTrsm.encrypt(track1);

                System.out.println("\nEncrypted Data (" + ep.getEncryptedData().length() / 2 + ") :" + ep.getEncryptedData());

                cData = serverTrsm.decrypt(ep);

                System.out.println("\nDecrypted data :" + cData);
            }
            catch (Exception ex)
            {
                System.out.println(ex.getMessage());
            }
        }
        reader.close();
        System.out.println("Done!");
    }

    static void testServerSide() throws Exception
    {
        ServerTrsm trsm = new ServerTrsm("0123456789ABCDEFFEDCBA9876543210");

        DukptKey pk = trsm.getPinKey("FFFF9876543210E0000F");
        System.out.println("Pin encryption key: " + pk.getKeyString());

        DukptKey mk = trsm.getMacKey("FFFF9876543210E0000F");
        System.out.println("MAC request key: " + mk.getKeyString());

        DukptKey dk = trsm.getDataKey("FFFF9876543210E0000F");
        System.out.println("Data Encryption key   : " + dk.getKeyString());
    }

    static void testTerminal() throws Exception
    {
        DukptKey bdk = new DukptKey("0123456789ABCDEFFEDCBA9876543210");

        KeySerialNumber ksn = new KeySerialNumber("FFFF9876543210E00003", "503");

        DukptKey ipek = Dukpt.getIpek(bdk, ksn);

        System.out.println("ipek: " + ipek.getKeyString());

        PosTrsm trsm = new PosTrsm();

        System.out.println("Loading :" + Utility.toHex(ksn.getPaddedKsn()));

        trsm.loadInitialKey(ipek, ksn);

        SessionKeyParameters sessionKeyParameters;

        int counter;

        try
        {
            for (int i = 0; i < 0x10; i++)
            {
//                sessionKeyParameters = trsm.getSessionKeyParameters();
//                sessionKeyParameters = trsm.getPinKeyParameters();
//                sessionKeyParameters = trsm.getMacKeyParameters();
                sessionKeyParameters = trsm.getDataKeyParameters();

                counter = sessionKeyParameters.getEncryptionCounter();

//                if (counter >= 0xFF800 && counter < 0XFFC03)
                {
                    System.out.println("cnt= " + Integer.toString(counter, 16)
                            + " SessionKey= " + sessionKeyParameters.getKey().getKeyString()
                            + " recepientKsn=" + sessionKeyParameters.getSessionKsn());
                }
            }
        }
        catch (Exception ex)
        {
            System.out.println(ex.getMessage());
        }
    }

}
