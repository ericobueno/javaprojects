package com.firstdata.tau;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class StaticBlock {

	private static Map<String, String> initials = new HashMap<>();
	private static int counter = 0;

	static {
		System.out.println("Loading map...");

		initials.put("eb", "Erico Bueno");
		initials.put("sv", "Suresh Vemulapali");

	}

	public static void main(String[] args) {

		System.out.println("Executing main...counter = " + counter);

		for (String key : initials.keySet()) {

			System.out.println("key= " + key + " " + "\"" + initials.get(key) + "\"");

		}

		if (counter++ < 5) {
			StaticBlock.main(args);
		}

	}

}
