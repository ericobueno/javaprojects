/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fdc;


import com.fdc.utils.Utils;
import java.math.BigInteger;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.MessageDigest;
import java.security.PublicKey;
import java.security.SecureRandom;
import javax.crypto.KeyAgreement;
import javax.crypto.interfaces.DHPrivateKey;
import javax.crypto.interfaces.DHPublicKey;
import javax.crypto.spec.DHParameterSpec;
import javax.crypto.spec.DHPublicKeySpec;

/**
 * /**
 *
 * @author ebueno
 */
public class Main
{
    /*
     private static BigInteger g512 = new BigInteger(
     "153d5d6172adb43045b68ae8e1de1070b6137005686d29d3d73a7"
     + "749199681ee5b212c9b96bfdcfa5b20cd5e3fd2044895d609cf9b"
     + "410b7a0f12ca1cb9a428cc", 16);
     */
    private static BigInteger g512 = new BigInteger("02", 16);

    private static BigInteger p512 = new BigInteger(
            "9494fec095f3b85ee286542b3836fc81a5dd0a0349b4c239dd387"
            + "44d488cf8e31db8bcb7d33b41abb9e5a33cca9144b1cef332c94b"
            + "f0573bf047a3aca98cdf3b", 16);

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception
    {
        testBasicDiffieHellman("BC");

        testModifiedDiffieHellman("BC");
    }

    public static void testBasicDiffieHellman( String provider) throws Exception
    {
        System.out.println("TestBasicDiffieHellman");

        DHParameterSpec dhParams = new DHParameterSpec(p512, g512);

        KeyPairGenerator keyGen = KeyPairGenerator.getInstance("DH", provider);

        keyGen.initialize(dhParams, new SecureRandom());

        // set up
        KeyAgreement aKeyAgree = KeyAgreement.getInstance("DH", provider);
        KeyPair aPair = keyGen.generateKeyPair();

        KeyAgreement bKeyAgree = KeyAgreement.getInstance("DH", provider);
        KeyPair bPair = keyGen.generateKeyPair();

        // two party agreement
        aKeyAgree.init(aPair.getPrivate());
        bKeyAgree.init(bPair.getPrivate());

        aKeyAgree.doPhase(bPair.getPublic(), true);

        bKeyAgree.doPhase(aPair.getPublic(), true);

        //      generate the key bytes
        MessageDigest hash = MessageDigest.getInstance("SHA1", provider);
        byte[] aShared = hash.digest(aKeyAgree.generateSecret());
        byte[] bShared = hash.digest(bKeyAgree.generateSecret());

        System.out.println(Utils.toHex(aShared));
        System.out.println(Utils.toHex(bShared));
    }

    public static void testModifiedDiffieHellman(String provider) throws Exception
    {
        System.out.println("\nModifiedDiffieHellman");

        DHParameterSpec dhParams = new DHParameterSpec(p512, g512);

        KeyPairGenerator akeyGen = KeyPairGenerator.getInstance("DH", provider);

        akeyGen.initialize(dhParams, new SecureRandom());

        KeyPair aPair = akeyGen.generateKeyPair();

        DHPrivateKey aPrivateKey = (DHPrivateKey) aPair.getPrivate();

        DHPublicKey aPublicKey = (DHPublicKey) aPair.getPublic();

        BigInteger aY = aPublicKey.getY();

        System.out.println("aY = " + aY.toString(16));

        // Alice (a) sends G, P and aY to Bob(b)
        BigInteger bY = sendToBob(provider, g512, p512, aY);

        DHPublicKeySpec publicKeySpec = new DHPublicKeySpec(bY, p512, g512);

        KeyFactory keyFactory = KeyFactory.getInstance("DH", provider);

        DHPublicKey bPublicKey = (DHPublicKey) keyFactory.generatePublic(publicKeySpec);

        KeyAgreement aKeyAgree = KeyAgreement.getInstance("DH", provider);

        aKeyAgree.init(aPair.getPrivate());

        aKeyAgree.doPhase(bPublicKey, true);

        MessageDigest hash = MessageDigest.getInstance("SHA256", provider);

        byte[] aSecret = aKeyAgree.generateSecret();

        System.out.println("a secret: " + Utils.toHex(aSecret));

        byte[] aShared = hash.digest(aSecret);

        System.out.println("a shared: " + Utils.toHex(aShared));
    }

    public static BigInteger sendToBob(String provider, BigInteger G, BigInteger P, BigInteger aY) throws Exception
    {
        DHPublicKeySpec publicKeySpec = new DHPublicKeySpec(aY, P, G);

        KeyFactory keyFactory = KeyFactory.getInstance("DH", provider);

        DHPublicKey aPublicKey = (DHPublicKey) keyFactory.generatePublic(publicKeySpec);

        DHPublicKeySpec a = keyFactory.getKeySpec(aPublicKey, DHPublicKeySpec.class);

        System.out.println("P spec= " + a.getP().toString(16));

        DHParameterSpec dhParams = new DHParameterSpec(P, G);

        KeyPairGenerator bkeyGen = KeyPairGenerator.getInstance("DH", provider);

        bkeyGen.initialize(dhParams, new SecureRandom());

        KeyPair bPair = bkeyGen.generateKeyPair();

        DHPrivateKey bPrivateKey = (DHPrivateKey) bPair.getPrivate();

        DHPublicKey bPublicKey = (DHPublicKey) bPair.getPublic();

        BigInteger bY = bPublicKey.getY();

        System.out.println("bY = " + bY.toString(16));

        //  Bob(b) Uses Alice Public key to generate secret key.
        KeyAgreement bKeyAgree = KeyAgreement.getInstance("DH", provider);

        bKeyAgree.init(bPrivateKey);

        bKeyAgree.doPhase(aPublicKey, true);

        MessageDigest hash = MessageDigest.getInstance("SHA256", provider);

        byte[] bSecret = bKeyAgree.generateSecret();

        byte[] bShared = hash.digest(bSecret);

        System.out.println("b secret: " + Utils.toHex(bSecret));

        System.out.println("b shared: " + Utils.toHex(bShared));

        return bY;  // return Bob public key material G^U mod P
    }
}
