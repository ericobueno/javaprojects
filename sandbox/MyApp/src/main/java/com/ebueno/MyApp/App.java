package com.ebueno.MyApp;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;

import com.ebueno.util.Utils;

/**
 * Hello world!
 *
 */
public class App {

	public static void main(String[] args) throws IOException {

		String host = "a3qvap998";
		int port = 15001;

		InetAddress address = InetAddress.getByName(host);
		Socket socket = new Socket(address, port);

		System.out.println("You're now connected to the Server");

		OutputStream os = socket.getOutputStream();
		// Send the message to the server

		String sendMessage = "<stm><rti>00000001</rti><echo>634059827728162747</echo></stm>"; // We
																								// tried
		DataOutputStream out = new DataOutputStream(os);
		out.writeUTF(sendMessage); 
		
		System.out.println("TransArmor Request =>" + sendMessage);

		InputStream inputStream = socket.getInputStream();

		DataInputStream in = new DataInputStream(inputStream);
		String response = in.readUTF();

		System.out.println("bytesRead=>" + response.length());
		System.out.println("TransArmor Response =>" + response);

	}

	public static void main1(String[] args) {
		System.out.println("Hello World!");

		Utils.sub1();
	}
}
