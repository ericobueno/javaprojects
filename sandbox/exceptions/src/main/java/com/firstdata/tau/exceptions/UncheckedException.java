//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
//
// Program        : Exception Tester
//
// Author         : Richard E. Pattis
//                  Computer Science Department
//                  Carnegie Mellon University
//                  5000 Forbes Avenue
//                  Pittsburgh, PA 15213-3891
//                  e-mail: pattis@cs.cmu.edu
//
// Maintainer     : Author
//
//
// Description:
//
//   This program illustrates the difference between how the Java compiler
// deals with "checked" and "unchecked" exceptions.  An "unchecked" exception
// is one that uses RuntimeException as its superclass (directly or
// indirectly); a "checked" exception does not not, but does extend Exception
// (directly or indirectly). Typically all exception classes include two
// overloaded constructors (they inherit a getMessage method from the Throwable
// class). Note that all the constraints are ensured by the compiler.
//
//   If a primary method calls a secondary method that might throw a checked
// exception, the primary method must either handle it, or if not it must
// specify that it "throws" that method.
//
// Known Bugs (if any):
//
// Program History:
//   3/21/02: R. Pattis - Operational for 15-100/15-111
//
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////

package com.firstdata.tau.exceptions;

//Declare an exception class that is unchecked (because it extends
//  RuntimeException).
//Define just the standard two constructors for exceptions.

public class UncheckedException extends RuntimeException
{
  public UncheckedException()
  {super();}
  
  public UncheckedException(String s)
  {super(s);}
}
