//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
//
// Program        : Exception Tester
//
// Author         : Richard E. Pattis
//                  Computer Science Department
//                  Carnegie Mellon University
//                  5000 Forbes Avenue
//                  Pittsburgh, PA 15213-3891
//                  e-mail: pattis@cs.cmu.edu
//
// Maintainer     : Author
//
//
// Description:
//
//   This program illustrates the difference between how the Java compiler
// deals with "checked" and "unchecked" exceptions.  An "unchecked" exception
// is one that uses RuntimeException as its superclass (directly or
// indirectly); a "checked" exception does not not, but does extend Exception
// (directly or indirectly). Typically all exception classes include two
// overloaded constructors (they inherit a getMessage method from the Throwable
// class). Note that all the constraints are ensured by the compiler.
//
//   If a primary method calls a secondary method that might throw a checked
// exception, the primary method must either handle it, or if not it must
// specify that it "throws" that method.
//
// Known Bugs (if any):
//
// Program History:
//   3/21/02: R. Pattis - Operational for 15-100/15-111
//
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////

package com.firstdata.tau;

import com.firstdata.tau.exceptions.CheckedException;
import com.firstdata.tau.exceptions.UncheckedException;

//////////////////////////////
//
// Primary methods in Application
//
//////////////////////////////

public class Application {

	// This method might throw a checked or unchecked exception.
	// It must say that it throws a checked one.
	// Comment-out "throws CheckedException" and the compiler complains

	public static void action(boolean throwChecked) throws CheckedException // required
	// throws CheckedException, SomeUncheckedException //not required, but a
	// good idea
	{
		if (throwChecked)
			throw new CheckedException("action throws CheckedException");
		else
			throw new UncheckedException("action throws UncheckedException");
	}

	// This method can throw only an unchecked exception, because it catches
	// the checked one and prints a message.
	// Try also running this with catch (Exception e){...} which catches
	// both possible exceptions

	public static void action1(boolean b)
	// throws UncheckedException //not required, but a good idea
	{
		try {
			action(b);
		} catch (CheckedException ce) {
			System.out.println("Caught \"" + ce.getMessage() + "\" in action1");
		}
		// }catch (Exception e){System.out.println("Caught " + e.getMessage()+ "
		// in action1");}

	}

	// This method can throw a checked or unchecked exception.
	// It mus say that it throws a checked one.
	// Comment-out "throws CheckedException" and the compiler complains

	public static void action2(boolean b) throws CheckedException
	// throws CheckedException, UncheckedException //not required, but a good
	// idea
	{
		action(b);
	}

	public static void main(String[] args) {
		try {

			action1(true);
			action1(false); // Comment this out to call next action2
			action2(true); // Comment this out to call next action2
			action2(false);

		} catch (Exception e) {
			System.out.println("Caught \"" + e.getMessage() + "\" in main");
			e.printStackTrace();
			System.out.println("main method in Application class terminating");
			System.exit(0);
		}

	}

}
