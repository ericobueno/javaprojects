package com.firstdata.tau.constants;

public enum ErrorCode {
	
	NO_ROUTE("906", "No active conection to the processor"),
	INVALID_MERCHANT("109", "Invalid merchant"),
	INVALID_TERMINAL("130", "Invalid terminal"),
	INTERNAL_ERROR("906", "An internal error occurred."),
	INVALID_XML("351", "The input XML is invalid"),
	TRANSIENT_ERROR("505", "Please Retry"),
	MISSING_DATA("EC005", "One or more required data fields is missing"),
	INVALID_DATA("EC002", "One or more data fields had a value that was not found in the database"),
	//new error code needs to be defined for invalid customer group id. For the time being keeping 109
	INVALID_CUSTOMER_GROUP_ID("109", "No customer present in databse with group id passed in request."), 
	INVALID_ROUTING_SETUP("201","Invalid routing Setup"), 
	INVALID_STORE("202","Invalid store");

	private String code;
	private String description;

	public String getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}

	private ErrorCode(String code, String description) {
		this.code = code;
		this.description = description;
	}

}
