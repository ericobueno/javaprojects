package com.firstdata.tau.main;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.reflect.Method;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import com.firstdata.tau.constants.ErrorCode;
import com.firstdata.tau.exception.GlobalException;
import com.firstdata.transarmor2.bean.*;

public class App {

	private static Schema validatorSchema = null;
	private static final Object[] oNull = null;
	private static final Class<?>[] cNull = null;

	public static void main(String[] args) throws Exception {

		GMFMessageVariants gmf = null;

		// String xmlData = readFileTo String("test.txt",
		// StandardCharsets.UTF_8);

		// String xmlData =
		// readFileToStringV6("src/main/resources/3.10_Credit_Auth_Txn.xml",
		// Charset.defaultCharset());
		
		String xmlData = readFileToStringV6("src/main/resources/test1.xml", Charset.defaultCharset());

		System.out.println("Jaxb Testing: ");
		System.out.println("xmlData = " + xmlData);

		SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);

		validatorSchema = factory.newSchema(new File("src/main/resources/UMF_Schema_16.02_V4.02.23.1.xsd"));

		// validatorSchema = factory.newSchema(getXSDFile());

		if (validateToXmlSchema(xmlData)) {
			System.out.println("File is good");
		} else {
			System.out.println("File is bad");
		}

		try {
			gmf = doUnmarshal(xmlData);
			CreditRequestDetails creditRequest = gmf.getCreditRequest();
			CommonGrp commonGrp;
			String pymtType;
			try {
				commonGrp = creditRequest.getCommonGrp();
				pymtType = commonGrp.getPymtType().value();
				System.out.println("Payment Type = " + pymtType);
				String tppid = commonGrp.getTPPID();
				System.out.println("TPPID= " + tppid);
				String txnAmt = commonGrp.getTxnAmt();
				System.out.println("TxnAmt =" + txnAmt);
				commonGrp.setTPPID("JJJ359");
			} catch (Exception e1) {
				System.out.println("Credit request not present");
			}
		} catch (Exception e2) {
			System.out.println("Error Unmarshalling = " + e2.getMessage());
		}

		if (gmf != null)
			doMarshal(gmf);

	}

	public static GMFMessageVariants doUnmarshal(String xmlData) throws Exception, JAXBException {

		JAXBContext jaxbContext = JAXBContext.newInstance(GMFMessageVariants.class);

		// JAXBContext jaxbContext =
		// JAXBContext.newInstance("com.firstdata.tau.jaxb");

		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

		// jaxbUnmarshaller.setSchema(validatorSchema); // Validate against
		// schema
		// when parsing

		StringReader xmlDataReader = new StringReader(xmlData);

		GMFMessageVariants gmf = null;

		try {
			gmf = (GMFMessageVariants) jaxbUnmarshaller.unmarshal(xmlDataReader);

			Object messageObject = getMessageObject(gmf);

			Class<?> c = messageObject.getClass();
		
			System.out.println("\n\n" + c.getName() + " methods are:");
			
			for (Method ix : c.getMethods()) {

				String n = ix.getName();
				System.out.println("method: " + n);
			}

			Method method = c.getMethod("getCommonGrp", cNull);
			CommonGrp cg = (CommonGrp) method.invoke(messageObject, oNull);

			// CommonGrp cg = (CommonGrp)(c.getMethod("getCommonGrp",
			// cNull).invoke(messageObject, oNull));

			try {
				CardGrp cardGrp = (CardGrp) (c.getMethod("getCardGrp", cNull).invoke(messageObject, oNull));
			} catch (NoSuchMethodException e) {
				System.out.println("Method getCardGrp failed e=" + e.getMessage());
				;
			}

			try {
				TAGrp taGroup = (TAGrp) (c.getMethod("getTAGrp", cNull).invoke(messageObject, oNull));
			} catch (NoSuchMethodException e) {
				System.out.println("Method getTAGrp failed e=" + e.getMessage());
				;
			}

			try {
				RespGrp respGrp = (RespGrp) (c.getMethod("getRespGrp", cNull).invoke(messageObject, oNull));
			} catch (NoSuchMethodException e) {
				System.out.println("Method getRespGrp failed e=" + e.getMessage());
				;
			}

			// System.out.println("Method Name=" + n);
		} catch (Exception e) {
			System.out.println("Error parsing = " + e.getMessage());
			throw new Exception("Error parsing XML (" + e.getMessage() + ")");
		} finally

		{
			xmlDataReader.close();
		}

		System.out.println("Unmarshalled data:");

		return gmf;

	}

	private static void doMarshal(GMFMessageVariants gmf) throws JAXBException, IOException {

		System.out.println("Marshal GMF to src/main/resources/out.xml");

		JAXBContext jaxbContext = JAXBContext.newInstance(GMFMessageVariants.class);

		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

		// jaxbMarshaller.setSchema(validatorSchema);

		File file = new File("src/main/resources/out.xml");

		// jaxbMarshaller.marshal(gmf, file); // Marshal into a file

		StringWriter xmlDataWriter = new StringWriter();

		jaxbMarshaller.marshal(gmf, xmlDataWriter); // Marshal into a String
													// Writer

		System.out.println("Fmt: " + xmlDataWriter.toString());

		Writer writer = new BufferedWriter(new FileWriter(file));

		writer.write(xmlDataWriter.toString());

		writer.close();

	}

	private static boolean validateToXmlSchema(String xmlData) {
		String failureCause;

		System.out.println("Validating xml againt schema");

		StringReader xmlDataReader = new StringReader(xmlData);

		try {

			Validator validator = validatorSchema.newValidator();

			validator.validate(new StreamSource(xmlDataReader));

		} catch (Exception e) {
			failureCause = e.toString();
			System.out.println("Error validating XML against XSD " + failureCause);
			return false;
		} finally {
			xmlDataReader.close();
		}
		return true;
	}

	public static String readFileToStringV7(String path, Charset encoding) throws IOException {

		// readAllBytes closes the file when all data is read or there is an
		// error

		byte[] encoded = Files.readAllBytes(Paths.get(path));

		return new String(encoded, encoding);
	}

	public static String readFileToStringV6(String file, Charset encoding) throws IOException {

		BufferedReader reader = new BufferedReader(new FileReader(file));
		String line = null;
		StringBuilder stringBuilder = new StringBuilder();
		String ls = System.getProperty("line.separator");

		while ((line = reader.readLine()) != null) {
			stringBuilder.append(line);
			stringBuilder.append(ls);
		}

		reader.close();

		return stringBuilder.toString();
	}

	private static Object getMessageObject(GMFMessageVariants gmf) throws GlobalException {

		Object messageObject = null;

		for (Method ix : gmf.getClass().getMethods()) {

			String n = ix.getName();

			// System.out.println("Method Name=" + n);

			if (n.equals("getClass") || !n.substring(0, 3).equals("get")) {
				// System.out.println("Ignore method " + n);
				continue; // Ignore all setXXX methods
			} else
				try {
					System.out.println("Invoke method " + n);
					Object o = ix.invoke(gmf, oNull);
					if (o != null) {

						if (messageObject != null)
							throw new GlobalException(ErrorCode.INVALID_XML, "message has multiple types");

						System.out.println("Method " + n + " Returns a valid object");
						System.out.println("Message is " + n.substring(3));

						messageObject = o;
					}
				} catch (GlobalException e) {
					throw e;
				} catch (Exception e) {
					throw new GlobalException(ErrorCode.INVALID_XML, "exception finding message type", e);
				}
		}
		if (messageObject == null)
			throw new GlobalException(ErrorCode.INVALID_XML, "unknown message type");
		return messageObject;
	}

}
