package com.firstdata.tau.exception;

import com.firstdata.tau.constants.ErrorCode;

public class GlobalException extends Exception {

	private static final long serialVersionUID = 1L;
	
//	private static final Logger logger = LoggerFactory.getLogger(GlobalException.class);

	private ErrorCode errorCode;
	private boolean isTransient;

	/* All global exceptions must be created with an ErrorCode */
	public GlobalException(ErrorCode errorCode) {
		super(errorCode.getDescription());
		this.errorCode = errorCode;
		this.isTransient = false;
//		TA2Logger.logError(logger,"Throwing an exception", this);
	}

	// You can manually override the generic description from the ErrorCode
	public GlobalException(ErrorCode errorCode, String description) {
		super(description);
		this.errorCode = errorCode;
		this.isTransient = false;
//		TA2Logger.logError(logger,"Throwing an exception", this);
	}
	public GlobalException(ErrorCode errorCode, Throwable cause) {
		super(errorCode.getDescription(), cause);
		this.errorCode = errorCode;
//		TA2Logger.logError(logger,"Throwing an exception", this);
	}
	public GlobalException(ErrorCode errorCode, boolean isTransient) {
		super(errorCode.getDescription());
		this.errorCode = errorCode;
		this.isTransient = isTransient;
//		TA2Logger.logError(logger,"Throwing an exception", this);
	}

	// Combinations: any two of description, cause, isTransient
	public GlobalException(ErrorCode errorCode, String description, Throwable cause) {
		super(description, cause);
		this.errorCode = errorCode;
		this.isTransient = false;
//		TA2Logger.logError(logger,"Throwing an exception", this);
	}
	public GlobalException(ErrorCode errorCode, String description, boolean isTransient) {
		super(description);
		this.errorCode = errorCode;
		this.isTransient = isTransient;
//		TA2Logger.logError(logger,"Throwing an exception", this);
	}
	public GlobalException(ErrorCode errorCode, Throwable cause, boolean isTransient) {
		super(errorCode.getDescription(), cause);
		this.errorCode = errorCode;
		this.isTransient = isTransient;
//		TA2Logger.logError(logger,"Throwing an exception", this);
	}

	// all of the above
	public GlobalException(ErrorCode errorCode, String description, Throwable cause, boolean isTransient) {
		super(description, cause);
		this.errorCode = errorCode;
		this.isTransient = isTransient;
//		TA2Logger.logError(logger,"Throwing an exception", this);
	}
	public ErrorCode getErrorCode() {
		return errorCode;
	}

	public boolean isTransient() {
		return this.isTransient;
	}
}
