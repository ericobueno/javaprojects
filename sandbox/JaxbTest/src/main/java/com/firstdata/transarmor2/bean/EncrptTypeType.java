//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2015.02.09 at 03:21:35 PM EST 
//


package com.firstdata.transarmor2.bean;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for EncrptTypeType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="EncrptTypeType">
 *   &lt;restriction base="{com/firstdata/Merchant/gmfV3.10}Max8AN">
 *     &lt;enumeration value="RSA"/>
 *     &lt;enumeration value="Verifon"/>
 *     &lt;enumeration value="TDES"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "EncrptTypeType")
@XmlEnum
public enum EncrptTypeType {

	@XmlEnumValue("RSA")
    RSA("RSA"),

    @XmlEnumValue("Verifone")
    VERIFONE("Verifone"),

	@XmlEnumValue("VSP")
	VSP("VSP"),

	@XmlEnumValue("3DES")
    TDES("3DES");
    private final String value;

    EncrptTypeType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static EncrptTypeType fromValue(String v) {
        for (EncrptTypeType c: EncrptTypeType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
