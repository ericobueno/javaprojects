/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fdc.sqltool;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLClientInfoException;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author ebueno
 */
public class SqlTool
{
    private String dbUrl;
    private String user;
    private String password;
    private Connection connection;
    Statement statement;
    private boolean connected = false;

    public SqlTool()
    {
    }

    public SqlTool(String dbUrl, String user, String password)
    {
        this.dbUrl = dbUrl;
        this.user = user;
        this.password = password;
    }

    public void executeSQL(String sql)
    {
        int columnCount;

        if (!connected)
        {
            try
            {
                System.out.println("Load mysql driver");

               // Class.forName(dbDriverClass);
                connection = DriverManager.getConnection(dbUrl, user, password);
                statement = connection.createStatement();
                connected = true;
            }
            catch (Exception ex)
            {
                System.out.println("Failed coonecting to database ex=" + ex.getMessage());
                System.exit(0);
            }
        }

        System.out.println("Executing [ " + sql + " ]");

        if (sql.toUpperCase().startsWith("SELECT"))
        {
            try (ResultSet resultSet = statement.executeQuery(sql))
            {
                int width = 0;

                ResultSetMetaData metadata = resultSet.getMetaData();

                columnCount = metadata.getColumnCount();

                for (int i = 0; i < columnCount; i++)
                {
                    width = metadata.getColumnDisplaySize(i + 1);
                    System.out.print(pad(metadata.getColumnName(i + 1), width));
                }
                System.out.println("");

                for (int i = 0; i < columnCount * width; i++)
                {
                    System.out.print("=");
                }

                System.out.println("");

                int max = 10;

                while (resultSet.next() && (max-- > 0))    // for each row
                {

                    for (int i = 0; i < columnCount; i++)
                    {
                        width = metadata.getColumnDisplaySize(i + 1);
                        System.out.print(pad(resultSet.getString(i + 1), width));
                    }

                    System.out.println("");
                    //   System.out.println("name=" + resultSet.getString("first_name"));
                }

            }
            catch (SQLException e)
            {
                e.printStackTrace();
            }

        }
        else
        {

        }

    }

    private String pad(String s, int width)
    {
        StringBuilder sb = new StringBuilder(25);
        int padCount;
        String outString;
        
        if( width < 10 )
        {
            width = 15;
        }

        if (s != null)
        {
            if (s.length() > width)
            {
                outString = s.substring(0, width - 1);
            }
            else
            {
                outString = s;
            }

            padCount = width - outString.length();

            sb.append(outString);
        }
        else
        {
            padCount = width;
        }

        for (int i = 0; i < padCount; i++)
        {
            sb.append(" ");
        }

        return sb.toString();
    }

}
