/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fdc.sqltool;

import javax.swing.JOptionPane;

/**
 *
 * @author ebueno
 */
public class Main
{

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        SqlTool tool;
        String mysqlDriver = "com.mysql.jdbc.Driver";
        String derbyDriver = "org.apache.derby.jdbc.ClientDriver";
        
        String dbUrl = "jdbc:mysql://localhost:3306/sakila";
        String user = "root";
        String password = "Buddy357";

        tool = new SqlTool(dbUrl, user, password);

        while (true)
        {
            String sql = JOptionPane.showInputDialog("Enter SQL statment");

            if (sql != null)
            {
                sql.trim();
                tool.executeSQL(sql);
            }
            else
            {
                break;
            }
        }
    }
}
