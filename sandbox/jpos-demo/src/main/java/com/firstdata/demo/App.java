package com.firstdata.demo;

import java.io.IOException;

import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.packager.GenericPackager;
import org.jpos.util.Logger;
import org.jpos.util.SimpleLogListener;

import com.firstdata.util.Util;


/**
 * Hello world!
 *
 */
public class App {
	
	public static void main(String[] args) throws IOException, ISOException {
		
		Logger logger = new Logger();
		logger.addListener(new SimpleLogListener(System.out));
		
		System.out.println("JPOS Demo!");

		// Create Packager based on XML that contain DE type

		// GenericPackager packager = new GenericPackager("basic.xml");

		GenericPackager packager = new GenericPackager("iso87binary.xml");
		
		packager.setLogger(logger, "bueno-looger");

		// Create ISO Message
		ISOMsg outIsoMsg = new ISOMsg();
		outIsoMsg.setPackager(packager);

		outIsoMsg.setMTI("0100");
		outIsoMsg.set(2, "5480180000000008"); // PAN
		outIsoMsg.set(3, "000000"); // Processing Code
		outIsoMsg.set(4, "13100"); // Transaction Amount
		outIsoMsg.set(11, "522601"); // System trace
		outIsoMsg.set(14, "0804"); // Expiration Date
		outIsoMsg.set(18, "5964"); // MCC
		outIsoMsg.set(22, "012"); // POS entry mode
		outIsoMsg.set(24, "000"); // NII - Network Int Ident

		outIsoMsg.set(25, "08"); // POS COndition Code
		outIsoMsg.set(37, "05 0226 001"); // RRN - Retrieval Reference Number
		outIsoMsg.set(41, "00654321"); // Card Acceptor Term ID
		outIsoMsg.set(42, "000318270506997"); // Card Acceptor ID - Merch
		outIsoMsg.set(48, "9921031    1313 MOCKINGBIRD LN "); // Additional Data
																// (MC)
		outIsoMsg.set(59, "372140000"); // Merchant Zip code
//		outIsoMsg.set(63, "AAABBBCCC");
		byte [] fld63 = {0x01,0x02,0x03 };
		outIsoMsg.set(63, fld63);
		// print the DE list

		logISOMsg(outIsoMsg);

		// Get and print the output result

		byte[] data = outIsoMsg.pack();

		Util.dumpHex(data, true);		// Pretty print
		
//		Util.dumpHex(data, false);		// Raw print
		
		System.out.println("");
			
		System.out.println("\nUnpacking\n\n");

		ISOMsg inIsoMsg = new ISOMsg();

		inIsoMsg.setPackager(packager);

		inIsoMsg.unpack(data);

		logISOMsg(inIsoMsg);

		System.out.println("\nUnpacking Hard coded ISO message\n\n");
		
		String amsg = "01007024458008c1002216548018000000000800000000000001310052260108045964001200000830352030323236203030312030303635343332313030303331383237303530363939370031393932313033312020202031333133204d4f434b494e4742495244204c4e20093337323134303030300009414141424242434343";
			
		byte [] data2 = Util.hexStringToByteArray(amsg);
		
	//	Util.dumpHex(data2, false);

		ISOMsg inIsoMsg2 = new ISOMsg();

		inIsoMsg2.setPackager(packager);

		inIsoMsg2.unpack(data2);

		logISOMsg(inIsoMsg2);

	}

	private static void logISOMsg(ISOMsg msg) {
		System.out.println("----ISO MESSAGE-----");
		try {
			System.out.println("  MTI : " + msg.getMTI());

			for (int i = 1; i <= msg.getMaxField(); i++) {
				if (msg.hasField(i)) {
					System.out.println("    Field-" + i + " : " + msg.getString(i));
				}
			}
		} catch (ISOException e) {
			e.printStackTrace();
		} finally {
			System.out.println("--------------------");
		}
	}

}
