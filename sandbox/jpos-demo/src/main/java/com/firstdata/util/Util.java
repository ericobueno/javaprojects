package com.firstdata.util;

public class Util {

	final protected static char[] hexArray = "0123456789ABCDEF".toCharArray();

	public static String bytesToHex(byte[] bytes) {

		char[] hexChars = new char[bytes.length * 2];
		for (int j = 0; j < bytes.length; j++) {
			int v = bytes[j] & 0xFF;
			hexChars[j * 2] = hexArray[v >>> 4];
			hexChars[j * 2 + 1] = hexArray[v & 0x0F];
		}
		return new String(hexChars);
	}

	public static void dumpHex(byte[] data, boolean pretty) {
		for (int i = 0; i < data.length; i++) {
			if (pretty && (i % 16 == 0)) {
				System.out.printf("\n");
			}
			if (pretty)
				System.out.printf("%02x ", data[i]);
			else
				System.out.printf("%02x", data[i]);
		}
	}

	public static byte[] hexStringToByteArray(String hexstring) {
		int i = 0;

		if (hexstring == null || hexstring.length() <= 0) {
			return null;
		}

		String stringvector = "0123456789ABCDEF0123456789abcdef";

		byte[] bytevector = { 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E,
				0x0F, 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F };

		byte[] out = new byte[hexstring.length() / 2];

		while (i < hexstring.length() - 1) {
			byte ch = 0x00;
			// Convert high nibble charater to a hex byte
			ch = (byte) (ch | bytevector[stringvector.indexOf(hexstring.charAt(i))]);

			ch = (byte) (ch << 4); // move this to the high bit

			// Convert the low nibble to a hexbyte
			ch = (byte) (ch | bytevector[stringvector.indexOf(hexstring.charAt(i + 1))]); // next
																							// hex
																							// value
			out[i / 2] = ch;
			i++;
			i++;
		}
		return out;
	}
}
