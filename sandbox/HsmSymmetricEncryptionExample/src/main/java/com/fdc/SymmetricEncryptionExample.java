/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fdc;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;

/**
 *
 * @author ebueno
 */
public class SymmetricEncryptionExample
{

    public static void main(String[] args) throws NoSuchProviderException,
            NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException,
            BadPaddingException, InvalidAlgorithmParameterException, NoSuchAlgorithmException
    {

//        String provider = "BC";
        String provider = "nCipherKM";

        /*
         * keyTypes lists the supported symmetric key types.
         * Note that not all mechanisms are available, depending on the underlying
         * software and hardware support.
         */
        String[] keyTypes = new String[]
        {
            /*
             "ArcFour", "BlowfishKey", "CAST256",
             "CAST", "DES2", "DESede", "DES", "Rijndael", "Serpent", "Twofish"
             */
            "AES"

        };
        /*
         * cipherModes lists the supported symmetric cipher modes.
         * paddingTypes lists the supported symmetric padding types.
         */
        String[] cipherModes = new String[]
        {
//            "CBC", "CFB", "CTR", "ECB", "OFB"
            "ECB"
        };

        String[] paddingTypes = new String[]
        {
            /*
             "ANSIX923Padding", "ISO10126Padding",
             "ISO7816Padding", "NoPadding", "PKCS5Padding", "ZeroBytePadding"
             */
            "NoPadding"
        };

        /*
         * For each symmetric key type, cipher mode and padding type supported by 
         * the nCipher JCE provider, perform an encryption and decryption on a
         * sample plain text.
         */
        for (int kt = 0; kt < keyTypes.length; kt++)
        {
            try
            {
                for (int cm = 0; cm < cipherModes.length; cm++)
                {
                    for (int pt = 0; pt < paddingTypes.length; pt++)
                    {

                        performEncryptDecrypt(keyTypes[kt],
                                cipherModes[cm],
                                paddingTypes[pt],
                                provider);

                        System.out.println("===================================");
                    }
                }
            }
            catch (NoSuchAlgorithmException e)
            {
                System.out.println(keyTypes[kt] + " algorithm unavailable @ " + System.currentTimeMillis());
            }
        }
    }

    private static void performEncryptDecrypt(String keyType, String cipherMode,
            String paddingType, String provider)
            throws NoSuchAlgorithmException, NoSuchProviderException,
            NoSuchPaddingException, InvalidKeyException,
            InvalidAlgorithmParameterException, IllegalBlockSizeException,
            BadPaddingException
    {
        System.out.print("KeyType = " + keyType);
        System.out.print(" cipher = " + cipherMode);
        System.out.println(" Padding = " + paddingType + " @ " + System.currentTimeMillis());

        Cipher encCipher = Cipher.getInstance(keyType + "/"
                + cipherMode + "/" + paddingType, provider );

        if (encCipher != null)
        {
            System.out.println("Encrypt/Decrypt using = " + encCipher.getAlgorithm());
        }

        /*
         * Symmetric SecretKeys are generated with the KeyGenerator class.
         */
        KeyGenerator kg = KeyGenerator.getInstance(keyType, provider);
        if (kg == null)
        {
            System.exit(0);
        }

        kg.init(192);

        SecretKey key = kg.generateKey();

//        System.out.println("key : " + Utils.toHex(key.getEncoded()));

        /* 
         * This example uses the HSM true random source to produce an example
         * plain text.
         */
//        SecureRandom r = SecureRandom.getInstance("RNG", provider);
        SecureRandom r = new SecureRandom();

        byte[] plainText = new byte[800];
        r.nextBytes(plainText);

        /* 
         * Create and initialise a Cipher instance for encryption.
         */
        /*
         * All the supported cipher modes other than ECB mode require an 
         * initialisation vector (IV). The same IV must be used for encryption 
         * and decryption. Here a new byte array (guaranteed by Java language to 
         * be all zeros) is used.
         */
        if (cipherMode.equalsIgnoreCase("ECB"))
        {
            encCipher.init(Cipher.ENCRYPT_MODE, key);
        }
        else
        {
            encCipher.init(Cipher.ENCRYPT_MODE, key,
                    new IvParameterSpec(new byte[encCipher.getBlockSize()]));
        }

        /* 
         * Encrypt the plain text.
         */
        System.out.println("Encrypting @ " + System.currentTimeMillis());

        byte[] cipherText = encCipher.doFinal(plainText);

        System.out.println("cipher text size = " + cipherText.length);
        /* 
         * Create and initialise a Cipher instance for decryption
         */
        Cipher decCipher = Cipher.getInstance(keyType + "/"
                + cipherMode + "/" + paddingType, provider);

        if (cipherMode.equalsIgnoreCase("ECB"))
        {
            decCipher.init(Cipher.DECRYPT_MODE, key);
        }
        else
        {
            decCipher.init(Cipher.DECRYPT_MODE, key,
                    new IvParameterSpec(new byte[encCipher.getBlockSize()]));
        }

        /* 
         * Decrypt the cipher text - the resulting deciphered text should match the
         * original plaintext.
         */
        byte[] decipheredText = decCipher.doFinal(cipherText);

    }

}
