/*
 * EncryptDecrypt to encrypt and decrypt messages using a certificate file
 * cacert.pem (contains the public key) and a private key file (cakey.p8c).
 * 
 * Created by Mobilefish.com (http://www.mobilefish.com)
 * 
 * File: EncryptDecrypt.java 
 * Needs: Certificate file (cacert.pem). See:
 *        http://www.mobilefish.com/developer/openssl/openssl_quickguide_create_ca.html
 * 
 *        To create private key file (cakey.p8c):
 *        - First create file (cakey.pem):
 *          http://www.mobilefish.com/developer/openssl/openssl_quickguide_create_ca.html
 *        - Convert PEM to PKCS#8 (cakey.pem into cakey.p8c):
 *          http://www.mobilefish.com/developer/openssl/openssl_quickguide_command_examples.html#step7a
 * 
 * Resources: -
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * If you have any questions, please contact contact@mobilefish.com
 */

package com.firstdata.EncryptDecrypt;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.security.cert.X509Certificate;
import java.security.cert.CertificateFactory;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.KeyFactory;
import java.security.interfaces.RSAPublicKey;
import java.security.interfaces.RSAPrivateKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Base64;
import java.security.SecureRandom;
import java.security.Security;

import javax.crypto.Cipher;

import org.bouncycastle.jce.provider.BouncyCastleProvider;

import com.firstdata.ta.crypto.CryptoUtils;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.Principal;
import java.security.cert.CertificateException;

public class App {

	/*
	 * Convert into hex values
	 */
	private static String hex(String binStr) {

		String newStr = new String();

		try {
			String hexStr = "0123456789ABCDEF";
			byte[] p = binStr.getBytes();
			newStr = "[" + p.length + "] ";
			for (int k = 0; k < p.length; k++) {
				int j = (p[k] >> 4) & 0xF;
				newStr = newStr + hexStr.charAt(j);
				j = p[k] & 0xF;
				newStr = newStr + hexStr.charAt(j) + " ";
			}
		} catch (Exception e) {
			System.out.println("Failed to convert into hex values: " + e);
		}
		return newStr;
	}

	/*
	 * Encrypt a message using a certificate file cacert.pem (contains public
	 * key). Decrypt the encrypted message using a private key file (cakey.p8c).
	 */
	public static void main(String[] args) {

		String myCert = "-----BEGIN CERTIFICATE-----\n"
				+ "MIICpTCCAY0CBQED8D3qMA0GCSqGSIb3DQEBBQUAMBExDzANBgNVBAMTBlRBQ0FU\n"
				+ "MTAeFw0xNjA3MjkxMzQ5MjBaFw0yNjA3MjcxMzQ5MjBaMBwxGjAYBgNVBAMTEVRB\n"
				+ "Q0FUMTY2MjU3OTgyNTQ2MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA\n"
				+ "tR68pmUj11dm2IRIcG7y8zDdWVj1KXiTUwgL2/0XweLeyUXD1S79EZPN7lz5UssP\n"
				+ "xhNGb4dEURBJHHj7wmYxYFGVgqgwvDx6b5BnJHBB/n/lXvEv9BwRyfvGCzxAfZ6z\n"
				+ "e7B6jV35MRaaPv+6Ih4ICdVO5fitmeF7DF1rBtb/dyJjw6dTGlTk+0fn0q4Sfbhj\n"
				+ "CRWhLGNIchv6u4OBFvponymB8uv/XJgVgZ8xaLLvir80+cg3dwCtQ3nGxVyC9Q/b\n"
				+ "ZnA7qcprzkX9dvVtiJWvWHpJXd9McZsGYbHhb2lHQ6sDLFfIRrfk4/+m3KotCJ/y\n"
				+ "JPzRHWDp8/rjgqmlsIDCZwIDAQABMA0GCSqGSIb3DQEBBQUAA4IBAQCf8cxfWsLu\n"
				+ "JiHpVNCRshnfPJ1oiI+DC0LAGS6A5Ouzn/FDCXeVoF+o5S7xaZ6joxyT7U8D56Xh\n"
				+ "gZ2w0trjVh0McZ6fd0cPl6ms0utO2d+VAYhrBkg1KZt1HYjbZ2/odb0EvzKL/z51\n"
				+ "o7YM5d+grdXRoqzOsEzQcJzyBiDW7mPv4CDZs6uGvVGjONGbEQS2H7DZzYyNzaWa\n"
				+ "X2ihDrEJeKCxautptJerEWIn5n905fbjuGRnAeAxjjuCyUIRJDn3TGtsFF4B+Xn7\n"
				+ "OzF/QZI9aGBQt5oQ53QASsTKWwnWf0PqgqB257dTXJoWKb/c5gpPIbTTTPCVwjzD\n" 
				+ "Mt5suxvXksC5\n"
				+ "-----END CERTIFICATE-----\n";

		String message = "";
		byte[] messageBytes;
		byte[] tempPub = null;
		String sPub = null;
		byte[] ciphertextBytes = null;
		byte[] textBytes = null;

		Security.addProvider(new BouncyCastleProvider());

		try {

			// The source of randomness
			SecureRandom secureRandom = new SecureRandom();

			// Obtain a RSA Cipher Object
			// Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding", "BC");

			Cipher cipher = Cipher.getInstance("RSA/None/OAEPWithSHA1AndMGF1Padding", "BC");

			// Loading certificate file
			String certFile = "TACAT166257982546.pem";

//			InputStream inStream = new FileInputStream(certFile);
			
			InputStream inStream = new ByteArrayInputStream(myCert.getBytes(StandardCharsets.UTF_8));

			CertificateFactory cf = CertificateFactory.getInstance("X.509");

			X509Certificate cert = (X509Certificate) cf.generateCertificate(inStream);

			inStream.close();

			// Read the public key from certificate file
			// RSAPublicKey pubkey = (RSAPublicKey) cert.getPublicKey();

			Principal subject = cert.getSubjectDN();
			System.out.println("Subject: " + subject.getName());

			PublicKey pubkey = cert.getPublicKey();

			System.out.println("toString: " + cert.toString());

			tempPub = pubkey.getEncoded();
			sPub = new String(tempPub);

			System.out.println("Public key from certificate file:\n" + CryptoUtils.toHexSpaced(tempPub) + "\n");
			
			System.out.println("Public Key Algorithm = " + pubkey.getAlgorithm() + "\n");

			// Set plain message
			message = "This Erico Bueno secret message. Do you dig it?";
			messageBytes = message.getBytes();
			System.out.println("Plain message:\n" + message + "\n");

			// Initialize the cipher for encryption
			cipher.init(Cipher.ENCRYPT_MODE, pubkey, secureRandom);

			// Encrypt the message
			ciphertextBytes = cipher.doFinal(messageBytes);

			byte[] edata = Base64.getEncoder().encode(ciphertextBytes);

			// System.out.println("Message encrypted with certificate file
			// public key:\n" + new String(ciphertextBytes) + "\n");

//			System.out.println("Message encrypted with certificate file public key:\n" + new String(edata) + "\n");
			System.out.println("Message encrypted with certificate file public key:\n" + prettyBase64(ciphertextBytes) + "\n");


			/*
			 * 
			 * // Loading private key file String keyFile = "cakey.p8c";
			 * inStream = new FileInputStream(keyFile); byte[] encKey = new
			 * byte[inStream.available()]; inStream.read(encKey);
			 * inStream.close();
			 * 
			 * // Read the private key from file
			 * System.out.println("RSA PrivateKeyInfo: " + encKey.length +
			 * " bytes\n"); PKCS8EncodedKeySpec privKeySpec = new
			 * PKCS8EncodedKeySpec(encKey); KeyFactory keyFactory =
			 * KeyFactory.getInstance("RSA");
			 * System.out.println("KeyFactory Object Info:");
			 * System.out.println("Algorithm = " + keyFactory.getAlgorithm());
			 * System.out.println("Provider = " + keyFactory.getProvider());
			 * PrivateKey priv = (RSAPrivateKey)
			 * keyFactory.generatePrivate(privKeySpec);
			 * System.out.println("Loaded " + priv.getAlgorithm() + " " +
			 * priv.getFormat() + " private key.");
			 * 
			 * // Initialize the cipher for decryption
			 * cipher.init(Cipher.DECRYPT_MODE, priv, secureRandom);
			 * 
			 * // Decrypt the message textBytes =
			 * cipher.doFinal(ciphertextBytes);
			 * System.out.println("Message decrypted with file private key:\n" +
			 * new String(textBytes) + "\n");
			 */

		} catch (IOException e) {
			System.out.println("IOException:" + e);
		} catch (CertificateException e) {
			System.out.println("CertificateException:" + e);
		} catch (NoSuchAlgorithmException e) {
			System.out.println("NoSuchAlgorithmException:" + e);
		} catch (Exception e) {
			System.out.println("Exception:" + e);
		}
	}
	
	public static String prettyBase64( byte [] in)
	{
		String ugly;
		String pretty = "";
		int ndx = 0;
		
		byte[] ob = Base64.getEncoder().encode(in);

		ugly = new String(ob);
		
		System.out.println("Ugly=" + ugly );
		int remainder = ugly.length();
		System.out.println("remainder=" + remainder );
		System.out.println("ugly=" + ugly);
				
		while ( remainder > 0 )
		{
			System.out.println("reaminder = " + remainder + " ndx=" + ndx);
			if(remainder >= 64 )
			{
				pretty += ugly.substring(ndx, ndx + 64) + "\n";
				ndx += 64;
				remainder -= 64;
			}
			else
			{
				pretty += ugly.substring(ndx) + "\n";
				remainder = 0;
			}
		}
		
		return pretty;
	}
}
