package com.firstdata.ta.ssl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLParameters;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

public class SslMain {

	public static void main(String[] args) throws IOException, Exception {

		System.setProperty("javax.net.ssl.trustStore", "/Users/ebueno/ca/ca.truststore");
		System.setProperty("javax.net.ssl.trustStorePassword", "changeit");

		TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {

			public java.security.cert.X509Certificate[] getAcceptedIssuers() {
				return null;
			}

			public void checkClientTrusted(X509Certificate[] certs, String authType) {
			}

			public void checkServerTrusted(X509Certificate[] certs, String authType) {
			}
		} };

		// Install the all-trusting trust manager

		final SSLContext sc = SSLContext.getInstance("SSL");
		
		sc.init(null, trustAllCerts, new java.security.SecureRandom());
		
		// HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

		// Create all-trusting host name verifier

		HostnameVerifier allHostsValid = new HostnameVerifier() {
			public boolean verify(String hostname, SSLSession session) {
				return true;
			}
		};

		HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);

		// URL url = new URL("https://www.google.com");
		URL url = new URL("https://n3qvwb1006.1dc.com");

		HttpsURLConnection con = (HttpsURLConnection) url.openConnection();

		System.out.println("Response Code : " + con.getResponseCode());
		System.out.println("Cipher Suite : " + con.getCipherSuite());
		System.out.println("\n");

		Certificate[] certs = con.getServerCertificates();

		for (Certificate cert : certs) {
			System.out.println("Cert Type : " + cert.getType());
			System.out.println("Cert Hash Code : " + cert.hashCode());
			System.out.println("Cert Public Key Algorithm : " + cert.getPublicKey().getAlgorithm());
			System.out.println("Cert Public Key Format : " + cert.getPublicKey().getFormat());
			System.out.println("\n");
		}

		Reader reader = new InputStreamReader(con.getInputStream());

		final BufferedReader br = new BufferedReader(reader);

		boolean done = false;
		while (done == false) {

			String line = "";
			while ((line = br.readLine()) != null) {
				System.out.println(line);
			}
			Thread.sleep(1000L);
		}
		
		System.out.println("Closing connection");
		
		br.close();

	}

}
