package com.fdc.excptions;

public class Main {
	public static void main(String args[]) {
		
		int a[] = new int[2];
		
		System.out.println("Entering try block");
		try {
			System.out.println("Access element three :" + a[3]); // THis causes an exception
			System.out.println("After excption causing event");
			return;
		}
		catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("Exception thrown  :" + e.toString());

		}
		finally {
			a[0] = 6;
			System.out.println("First element value: " + a[0]);
			System.out.println("The finally statement is executed");

		}
		System.out.println("Out of the blocks");
	}
}
