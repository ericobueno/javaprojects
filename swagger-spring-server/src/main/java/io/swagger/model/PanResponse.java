package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;




/**
 * PanResponse
 */
@javax.annotation.Generated(value = "class io.swagger.codegen.languages.SpringCodegen", date = "2016-11-03T14:48:00.337Z")

public class PanResponse   {
  private String encryptedPan = null;

  private String requestDate = null;

  private String errorMsg = null;

  private String respCode = null;

  private String token = null;

  public PanResponse encryptedPan(String encryptedPan) {
    this.encryptedPan = encryptedPan;
    return this;
  }

   /**
   * Encrypted PAN
   * @return encryptedPan
  **/
  @ApiModelProperty(required = true, value = "Encrypted PAN")
  public String getEncryptedPan() {
    return encryptedPan;
  }

  public void setEncryptedPan(String encryptedPan) {
    this.encryptedPan = encryptedPan;
  }

  public PanResponse requestDate(String requestDate) {
    this.requestDate = requestDate;
    return this;
  }

   /**
   * Request Date
   * @return requestDate
  **/
  @ApiModelProperty(required = true, value = "Request Date")
  public String getRequestDate() {
    return requestDate;
  }

  public void setRequestDate(String requestDate) {
    this.requestDate = requestDate;
  }

  public PanResponse errorMsg(String errorMsg) {
    this.errorMsg = errorMsg;
    return this;
  }

   /**
   * Error Message
   * @return errorMsg
  **/
  @ApiModelProperty(value = "Error Message")
  public String getErrorMsg() {
    return errorMsg;
  }

  public void setErrorMsg(String errorMsg) {
    this.errorMsg = errorMsg;
  }

  public PanResponse respCode(String respCode) {
    this.respCode = respCode;
    return this;
  }

   /**
   * Response Code
   * @return respCode
  **/
  @ApiModelProperty(required = true, value = "Response Code")
  public String getRespCode() {
    return respCode;
  }

  public void setRespCode(String respCode) {
    this.respCode = respCode;
  }

  public PanResponse token(String token) {
    this.token = token;
    return this;
  }

   /**
   * Financial Token
   * @return token
  **/
  @ApiModelProperty(required = true, value = "Financial Token")
  public String getToken() {
    return token;
  }

  public void setToken(String token) {
    this.token = token;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PanResponse panResponse = (PanResponse) o;
    return Objects.equals(this.encryptedPan, panResponse.encryptedPan) &&
        Objects.equals(this.requestDate, panResponse.requestDate) &&
        Objects.equals(this.errorMsg, panResponse.errorMsg) &&
        Objects.equals(this.respCode, panResponse.respCode) &&
        Objects.equals(this.token, panResponse.token);
  }

  @Override
  public int hashCode() {
    return Objects.hash(encryptedPan, requestDate, errorMsg, respCode, token);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class PanResponse {\n");
    
    sb.append("    encryptedPan: ").append(toIndentedString(encryptedPan)).append("\n");
    sb.append("    requestDate: ").append(toIndentedString(requestDate)).append("\n");
    sb.append("    errorMsg: ").append(toIndentedString(errorMsg)).append("\n");
    sb.append("    respCode: ").append(toIndentedString(respCode)).append("\n");
    sb.append("    token: ").append(toIndentedString(token)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

