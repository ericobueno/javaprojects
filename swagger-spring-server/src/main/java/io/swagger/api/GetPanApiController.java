package io.swagger.api;

import io.swagger.model.PanRequest;
import io.swagger.model.PanResponse;

import io.swagger.annotations.*;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;


@javax.annotation.Generated(value = "class io.swagger.codegen.languages.SpringCodegen", date = "2016-11-03T14:48:00.337Z")

@Controller
public class GetPanApiController implements GetPanApi {

    public ResponseEntity<PanResponse> getPan1UsingPOST(

@ApiParam(value = "panRequest" ,required=true ) @RequestBody PanRequest panRequest

) {
        // do some magic!
        return new ResponseEntity<PanResponse>(HttpStatus.OK);
    }

    public ResponseEntity<PanResponse> getPanUsingGET(@ApiParam(value = "Point of sale ID (8 bytes)", required = true) @RequestParam(value = "posId", required = true) String posId



,
        @ApiParam(value = "RSA/PKI Key ID", required = true) @RequestParam(value = "keyId", required = true) String keyId



,
        @ApiParam(value = "Financial token (FToken)", required = true) @RequestParam(value = "token", required = true) String token



) {
        // do some magic!
        return new ResponseEntity<PanResponse>(HttpStatus.OK);
    }

}
