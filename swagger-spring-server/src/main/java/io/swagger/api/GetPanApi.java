package io.swagger.api;

import io.swagger.model.PanRequest;
import io.swagger.model.PanResponse;

import io.swagger.annotations.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@javax.annotation.Generated(value = "class io.swagger.codegen.languages.SpringCodegen", date = "2016-11-03T14:48:00.337Z")

@Api(value = "getPan", description = "the getPan API")
public interface GetPanApi {

    @ApiOperation(value = "Get encrypted PAN", notes = "This method converts a financial token (FToken) to an encrypted PAN", response = PanResponse.class, tags={ "Encrypted PAN Service", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "Detokenize and encrypt response", response = PanResponse.class),
        @ApiResponse(code = 201, message = "Created", response = PanResponse.class),
        @ApiResponse(code = 401, message = "Unauthorized", response = PanResponse.class),
        @ApiResponse(code = 403, message = "Forbidden", response = PanResponse.class),
        @ApiResponse(code = 404, message = "Error getting encrypted PAN", response = PanResponse.class) })
    @RequestMapping(value = "/getPan",
        produces = { "application/json" }, 
        consumes = { "application/json" },
        method = RequestMethod.POST)
    ResponseEntity<PanResponse> getPan1UsingPOST(

@ApiParam(value = "panRequest" ,required=true ) @RequestBody PanRequest panRequest

);


    @ApiOperation(value = "Get encrypted PAN", notes = "This method converts a financial token (FToken) to an encrypted PAN", response = PanResponse.class, tags={ "Encrypted PAN Service", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "Detokenize and encrypt response", response = PanResponse.class),
        @ApiResponse(code = 401, message = "Unauthorized", response = PanResponse.class),
        @ApiResponse(code = 403, message = "Forbidden", response = PanResponse.class),
        @ApiResponse(code = 404, message = "Error getting encrypted PAN", response = PanResponse.class) })
    @RequestMapping(value = "/getPan",
        produces = { "application/json" }, 
        consumes = { "application/json" },
        method = RequestMethod.GET)
    ResponseEntity<PanResponse> getPanUsingGET(@ApiParam(value = "Point of sale ID (8 bytes)", required = true) @RequestParam(value = "posId", required = true) String posId



,@ApiParam(value = "RSA/PKI Key ID", required = true) @RequestParam(value = "keyId", required = true) String keyId



,@ApiParam(value = "Financial token (FToken)", required = true) @RequestParam(value = "token", required = true) String token



);

}
