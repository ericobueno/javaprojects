package com.example.demo.controllers;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DemoController {

	private static Logger logger = Logger.getLogger(DemoController.class);

	@Value("${logging.file}")
	String logFile;

	/*
	 * Please see properties files for settings
	 * 
	 */
	
	
	// http://localhost:8080/hello?name=jane

	@RequestMapping(value = "/hello", method = RequestMethod.GET)
	public String hello(@RequestParam(value = "name", defaultValue = "erico") String name) {

		logger.info("Greetings..." + name);

		logger.warn("Greeting..." + name);

		logger.warn("logFile=" + logFile);

		String greetings = "Hello1 " + name;

		return greetings;
	}
	

	// http://localhost:8080/hello/jane
	
	@RequestMapping(value = "/hello/{name}", method = RequestMethod.GET)
	public String hello1(@PathVariable("name") String name) {

		logger.info("Greeting..." + name);

		logger.warn("Greeting..." + name);

		logger.warn("logFile=" + logFile);

		String greetings = "Hello " + name;

		return greetings;
	}


}
