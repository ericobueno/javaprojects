package com.example;

import java.sql.SQLException;

import javax.sql.DataSource;

import org.apache.commons.dbcp2.BasicDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

import oracle.jdbc.pool.OracleDataSource;

@SpringBootApplication
public class StocksApplication {

	Logger logger = LoggerFactory.getLogger(StocksApplication.class);

	public static void main(String[] args) {
		
		ApplicationContext ctx = SpringApplication.run(StocksApplication.class, args);

		String[] beanDefinitionNames = ctx.getBeanDefinitionNames();
		/*
		 * for (String name : beanDefinitionNames) { System.out.println("name=" + name);
		 * }
		 */

	}
	
/*
	@Bean
	DataSource dataSource() throws SQLException {

		OracleDataSource dataSource = new OracleDataSource();
		dataSource.setUser("ebueno");
		dataSource.setPassword("Buddy357");
		dataSource.setURL("jdbc:oracle:thin:@localhost:1521:xe");
		dataSource.setImplicitCachingEnabled(true);
		dataSource.setFastConnectionFailoverEnabled(true);
		return dataSource;
	}
*/
	
	/*
	mvn install:install-file -Dfile=C:\Users\erico\Downloads\ojdbc6.jar -DgroupId=com.oracle -DartifactId=ojdbc6 -Dversion=11.2.0.4 -Dpackaging=jar
	mvn install:install-file -Dfile=C:\Users\erico\Downloads\ojdbc7.jar -DgroupId=com.oracle -DartifactId=ojdbc7 -Dversion=12.1.0.2 -Dpackaging=jar
	mvn install:install-file -Dfile=C:\Users\erico\Downloads\ojdbc8.jar -DgroupId=com.oracle -DartifactId=ojdbc8 -Dversion=12.2.0.1 -Dpackaging=jar
	*/
			 
		/*
	 * 
	 *  mvn install:install-file -Dfile=D:\Downloads\Java\ojdbc7.jar
	 *  -DgroupId=com.oracle -DartifactId=ojdbc7 -Dversion=12.1.0.2 -Dpackaging=jar
	 * 
	 * <dependency> <groupId>com.oracle</groupId> <artifactId>ojdbc7</artifactId>
	 * <version>12.1.0.2</version> </dependency>
	 */

	// @Bean
	// DataSource datasource() {
	//
	// BasicDataSource dataSource = new BasicDataSource();
	//
	// dataSource.setDriverClassName("com.mysql.jdbc.Driver");
	// dataSource.setUrl("jdbc:mysql://localhost/stocks?useSSL=false");
	// dataSource.setUsername("ebueno");
	// dataSource.setPassword("Buddy357");
	//
	// return dataSource;
	// }

	@Bean(name = "myBean")
	CommandLineRunner loadDatabase(StockRepository sr) {
		return args -> {

			logger.warn("loading database..");

			sr.save(new Stock("ATT", 101.23));
			sr.save(new Stock("FDC", 15.25));
			sr.save(new Stock("MCI", 42.12));

			logger.warn("record count: {}", sr.count());

			sr.findAll().forEach(x -> logger.warn(x.toString()));
		};

	}

}
