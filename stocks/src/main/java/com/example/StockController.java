package com.example;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class StockController {
	
	private static Logger logger = Logger.getLogger(StockController.class);
	
	@Value("${logging.file}")
	String logFile;
	
	@Autowired
	StockRepository stockRepository;

	public StockController() {
	}

	
    @RequestMapping(value="/stock", method=RequestMethod.GET)
    public Stock stock(@RequestParam(value="ticker", defaultValue="NFLX") String ticker) {
    	
    	logger.info("Searching..." + ticker);
    	
    	logger.warn("logFile=" + logFile);
    	
    	Stock stock = stockRepository.findOne(ticker);
    	if(stock == null){

    	}
    	logger.info("Found: " + stock);
    	return stock;
    }
    
    @RequestMapping(value="/stock", method=RequestMethod.POST)
    public Stock stock(@RequestParam(value="ticker", defaultValue="NFLX") String ticker,
    		@RequestParam(value="price", defaultValue="100.0") double price) {
    	
    	Stock stock = new Stock(ticker, price);
    	logger.info("Saving...: " + stock );
    	Stock savedStock = stockRepository.save(stock);
    	logger.info("Saved: " + savedStock);
    	return savedStock;
    	
    }
    
    @RequestMapping(value="/addstock", method=RequestMethod.POST)
    public Stock stock( @RequestBody Stock stock ) {
    	
    	
    	logger.info("Saving...: " + stock );
    	
    	Stock savedStock = stockRepository.save(stock);
    	
    	logger.info("Saved: " + savedStock);
    	
    	return savedStock;
    	
    }
    
    @RequestMapping("/stocks")
    public List<Stock> stocks() {
    	
    	List<Stock> stocks = new ArrayList<>();
    	
    	Iterable<Stock> findAll = stockRepository.findAll();
    	for (Stock stock : findAll)
    	{
    		System.out.println("stock: " + stock);
    		stocks.add(stock);
    	}
    	
    	return stocks;
    	
    }
    
}