package com.firstdata.ca;

import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;

import javax.xml.rpc.ServiceException;

import com.rsa.ca.CADelegate;
import com.rsa.ca.CAService;
import com.rsa.ca.CAServiceLocator;
import com.rsa.ca.CertificateObject;
import com.rsa.ca.RSACAException;

public class Main {

	public static void main(String[] args) throws MalformedURLException, ServiceException, RSACAException, RemoteException {
		
		URL endPoint = new URL("https://n3qvwb1006.1dc.com/CAService/CAService");
		
		CAService service = new CAServiceLocator();
		
		CADelegate caPort = service.getCAPort(endPoint);
		
		//CADelegate caPort = service.getCAPort();
		
		caPort.certInit("?", "?", "?");
		
		CertificateObject cert = caPort.getCert("TACAT1");
		
		System.out.println("cert: " + cert.getCertificate());

	}

}
