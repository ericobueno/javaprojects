/**
 * CertificateObject.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.rsa.ca;

public class CertificateObject  implements java.io.Serializable {
    private com.rsa.ca.ErrorCode returnCode;

    private java.lang.String certificate;

    private java.lang.String chain;

    public CertificateObject() {
    }

    public CertificateObject(
           com.rsa.ca.ErrorCode returnCode,
           java.lang.String certificate,
           java.lang.String chain) {
           this.returnCode = returnCode;
           this.certificate = certificate;
           this.chain = chain;
    }


    /**
     * Gets the returnCode value for this CertificateObject.
     * 
     * @return returnCode
     */
    public com.rsa.ca.ErrorCode getReturnCode() {
        return returnCode;
    }


    /**
     * Sets the returnCode value for this CertificateObject.
     * 
     * @param returnCode
     */
    public void setReturnCode(com.rsa.ca.ErrorCode returnCode) {
        this.returnCode = returnCode;
    }


    /**
     * Gets the certificate value for this CertificateObject.
     * 
     * @return certificate
     */
    public java.lang.String getCertificate() {
        return certificate;
    }


    /**
     * Sets the certificate value for this CertificateObject.
     * 
     * @param certificate
     */
    public void setCertificate(java.lang.String certificate) {
        this.certificate = certificate;
    }


    /**
     * Gets the chain value for this CertificateObject.
     * 
     * @return chain
     */
    public java.lang.String getChain() {
        return chain;
    }


    /**
     * Sets the chain value for this CertificateObject.
     * 
     * @param chain
     */
    public void setChain(java.lang.String chain) {
        this.chain = chain;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CertificateObject)) return false;
        CertificateObject other = (CertificateObject) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.returnCode==null && other.getReturnCode()==null) || 
             (this.returnCode!=null &&
              this.returnCode.equals(other.getReturnCode()))) &&
            ((this.certificate==null && other.getCertificate()==null) || 
             (this.certificate!=null &&
              this.certificate.equals(other.getCertificate()))) &&
            ((this.chain==null && other.getChain()==null) || 
             (this.chain!=null &&
              this.chain.equals(other.getChain())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getReturnCode() != null) {
            _hashCode += getReturnCode().hashCode();
        }
        if (getCertificate() != null) {
            _hashCode += getCertificate().hashCode();
        }
        if (getChain() != null) {
            _hashCode += getChain().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CertificateObject.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ca.rsa.com/", "certificateObject"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("returnCode");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ReturnCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ca.rsa.com/", "errorCode"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("certificate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Certificate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("chain");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Chain"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
