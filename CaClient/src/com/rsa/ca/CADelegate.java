/**
 * CADelegate.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.rsa.ca;

public interface CADelegate extends java.rmi.Remote {
    public com.rsa.ca.ErrorCode certInit(java.lang.String appName, java.lang.String configFile, java.lang.String password) throws java.rmi.RemoteException, com.rsa.ca.RSACAException;
    public com.rsa.ca.ErrorCode caSignKey(java.lang.String signingKeyID, java.lang.String csr, int lifetimeDays, java.lang.String CN) throws java.rmi.RemoteException, com.rsa.ca.RSACAException;
    public com.rsa.ca.CertificateObject getCert(java.lang.String keyID) throws java.rmi.RemoteException, com.rsa.ca.RSACAException;
}
