/**
 * CAServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.rsa.ca;

public class CAServiceLocator extends org.apache.axis.client.Service implements com.rsa.ca.CAService {

    public CAServiceLocator() {
    }


    public CAServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public CAServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for CAPort
    private java.lang.String CAPort_address = "http://n3qvwb1006.1dc.com:80/CAService/CAService";

    public java.lang.String getCAPortAddress() {
        return CAPort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String CAPortWSDDServiceName = "CAPort";

    public java.lang.String getCAPortWSDDServiceName() {
        return CAPortWSDDServiceName;
    }

    public void setCAPortWSDDServiceName(java.lang.String name) {
        CAPortWSDDServiceName = name;
    }

    public com.rsa.ca.CADelegate getCAPort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(CAPort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getCAPort(endpoint);
    }

    public com.rsa.ca.CADelegate getCAPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.rsa.ca.CAPortBindingStub _stub = new com.rsa.ca.CAPortBindingStub(portAddress, this);
            _stub.setPortName(getCAPortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setCAPortEndpointAddress(java.lang.String address) {
        CAPort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.rsa.ca.CADelegate.class.isAssignableFrom(serviceEndpointInterface)) {
                com.rsa.ca.CAPortBindingStub _stub = new com.rsa.ca.CAPortBindingStub(new java.net.URL(CAPort_address), this);
                _stub.setPortName(getCAPortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("CAPort".equals(inputPortName)) {
            return getCAPort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://ca.rsa.com/", "CAService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://ca.rsa.com/", "CAPort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("CAPort".equals(portName)) {
            setCAPortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
