package com.rsa.ca;

public class CADelegateProxy implements com.rsa.ca.CADelegate {
  private String _endpoint = null;
  private com.rsa.ca.CADelegate cADelegate = null;
  
  public CADelegateProxy() {
    _initCADelegateProxy();
  }
  
  public CADelegateProxy(String endpoint) {
    _endpoint = endpoint;
    _initCADelegateProxy();
  }
  
  private void _initCADelegateProxy() {
    try {
      cADelegate = (new com.rsa.ca.CAServiceLocator()).getCAPort();
      if (cADelegate != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)cADelegate)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)cADelegate)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (cADelegate != null)
      ((javax.xml.rpc.Stub)cADelegate)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.rsa.ca.CADelegate getCADelegate() {
    if (cADelegate == null)
      _initCADelegateProxy();
    return cADelegate;
  }
  
  public com.rsa.ca.ErrorCode certInit(java.lang.String appName, java.lang.String configFile, java.lang.String password) throws java.rmi.RemoteException, com.rsa.ca.RSACAException{
    if (cADelegate == null)
      _initCADelegateProxy();
    return cADelegate.certInit(appName, configFile, password);
  }
  
  public com.rsa.ca.ErrorCode caSignKey(java.lang.String signingKeyID, java.lang.String csr, int lifetimeDays, java.lang.String CN) throws java.rmi.RemoteException, com.rsa.ca.RSACAException{
    if (cADelegate == null)
      _initCADelegateProxy();
    return cADelegate.caSignKey(signingKeyID, csr, lifetimeDays, CN);
  }
  
  public com.rsa.ca.CertificateObject getCert(java.lang.String keyID) throws java.rmi.RemoteException, com.rsa.ca.RSACAException{
    if (cADelegate == null)
      _initCADelegateProxy();
    return cADelegate.getCert(keyID);
  }
  
  
}