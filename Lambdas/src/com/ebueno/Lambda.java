package com.ebueno;

public class Lambda {

	public static void main(String[] args) {

		Lambda lambda = new Lambda();

		Greeting english = (s, n) -> {
			System.out.println(s);
			System.out.println(n);
		};

		lambda.greet(english);

		Thread myThread = new Thread(() -> System.out.println("From thread"), "BuenoTHread");

		myThread.start();

		Thread another = new Thread(new Runnable() {

			@Override
			public void run() {
				System.out.println("From another");
			}
		});

		another.start();
	}

	void greet(Greeting greeting) {

		greeting.doit("Hello", "Erico");
	}

}

@FunctionalInterface
interface Greeting {
	void doit(String g, String n);
}
