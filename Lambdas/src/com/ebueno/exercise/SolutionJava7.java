package com.ebueno.exercise;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class SolutionJava7 {

	public static void main(String[] args) {

		List<Person> people = Arrays.asList(new Person("Bueno", "Erico", 65), new Person("Lewis", "Carroll", 42),
				new Person("Thomas", "Carlyle", 51), new Person("Charlotte", "Bronte", 45),
				new Person("Mathew", "Arnold", 39));

		printAll(people);

		// 1 - Sort list by last name

		Collections.sort(people, new Comparator<Person>() {

			@Override
			public int compare(Person o1, Person o2) {
				return o1.getLastName().compareTo(o2.getLastName());
			}
		});

		System.out.println("\nAfter Sort\n");

		// 2 - Create method that prints all elements in the list

		printAll(people);

		// 3 - Create a method that prints all people that have last name starting with

		printLastNameBeginningWithC(people);

		printConditionally(people, new Predicado<Person>() {

			@Override
			public boolean test(Person p) {
				return p.getLastName().startsWith("C");
			}
		});
		
		printConditionally(people, new Predicado<Person>() {

			@Override
			public boolean test(Person p) {
				return p.getFirstName().startsWith("C");
			}
		});

	}

	private static void printAll(List<Person> people) {

		// people.forEach(p -> System.out.println(p)); // Java8

		for (Person person : people) {
			System.out.println(person);
		}
	}

	private static void printLastNameBeginningWithC(List<Person> people) {

		System.out.println("\nNames starting with C");

		for (Person person : people) {

			if (person.getLastName().startsWith("C"))
				System.out.println(person);
		}

	}

	private static void printConditionally(List<Person> people, Predicado condition) {

		System.out.println("\nPrint Conditionally");

		for (Person person : people) {

			if (condition.test(person))
				System.out.println(person);
		}

	}
}


