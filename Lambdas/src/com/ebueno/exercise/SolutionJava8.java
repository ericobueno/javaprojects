package com.ebueno.exercise;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class SolutionJava8 {

	public static void main(String[] args) {

		List<Person> people = Arrays.asList(new Person("Bueno", "Erico", 65), new Person("Lewis", "Carroll", 42),
				new Person("Thomas", "Carlyle", 51), new Person("Charlotte", "Bronte", 45),
				new Person("Mathew", "Arnold", 39));

		printConditionally(people, p -> true);   // Print ALL

		// 1 - Sort list by last name

		Collections.sort(people, (o1, o2) -> o1.getLastName().compareTo(o2.getLastName()));
	
		// OR this
		
		people.sort((o1, o2) -> o1.getLastName().compareTo(o2.getLastName()));
		

		System.out.println("\nAfter Sort\n");

		// 2 - Create method that prints all elements in the list

		printConditionally(people, p -> true);   // Print ALL

		// 3 - Create a method that prints all people that have last name starting with

		printConditionally(people, p -> p.getLastName().startsWith("C"));
		
		printConditionally(people, p -> p.getFirstName().startsWith("C"));
	}

	private static void printConditionally(List<Person> people, Predicado<Person> condition) {

		System.out.println("\nPrint Conditionally");

		for (Person person : people) {

			if (condition.test(person))
				System.out.println(person);
		}

	}
}

