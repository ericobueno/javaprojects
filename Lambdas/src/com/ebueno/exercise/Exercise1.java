package com.ebueno.exercise;

import java.util.Arrays;
import java.util.List;

public class Exercise1 {

	public static void main(String[] args) {

		List<Person> people = Arrays.asList(new Person("Bueno", "Erico", 65), new Person("Lewis", "Carroll", 42),
				new Person("Thomas", "Carlyle", 51), new Person("Charlotte", "Bronte", 45),
				new Person("Mathew", "Arnold", 39));
		
		
		people.forEach( p ->System.out.println(p) );
		
		// 1 - Sort list by last name
		
		// 2 - Create method that prints all elements in the list
		
		// 3 - Create a method that prints all people that have last name starting with C
		

	}

}
