package com.ebueno.exercise;

public interface Predicado<T> {
	boolean test(T t);
}
