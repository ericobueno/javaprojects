package io.swagger.client;

import io.swagger.client.api.EncryptedPANServiceApi;
import io.swagger.client.model.PanRequest;
import io.swagger.client.model.PanResponse;

public class Main {

	public static void main(String[] args) throws ApiException {

		String posId = "12345678";
		String keyId = "66257982546";
		String token = "9194902635013456";

	//	EncryptedPANServiceApi api = new EncryptedPANServiceApi();
	//	api.setApiClient(apiClient);
		
		ApiClient apiClient = new ApiClient();
		apiClient.setBasePath("https://detok-encrypt.apps.us-oma1-inp1.1dc.com");
		
		EncryptedPANServiceApi api = new EncryptedPANServiceApi(apiClient);
		
		PanResponse response = api.getPanUsingGET(posId, keyId, token);

		System.out.println("GET response= " + response + "\n");
		
		PanRequest panRequest = new PanRequest();
		
		panRequest.setKeyId("66257982546");
		panRequest.setPosId("12345678");
		panRequest.setToken("9194902635013456");
		
        response = api.getPan1UsingPOST(panRequest);

        System.out.println("POST response= " + response);
	}

}
