# EncryptedPANServiceApi

All URIs are relative to *https://localhost:8080/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getPan1UsingPOST**](EncryptedPANServiceApi.md#getPan1UsingPOST) | **POST** /getPan | Get encrypted PAN
[**getPanUsingGET**](EncryptedPANServiceApi.md#getPanUsingGET) | **GET** /getPan | Get encrypted PAN


<a name="getPan1UsingPOST"></a>
# **getPan1UsingPOST**
> PanResponse getPan1UsingPOST(panRequest)

Get encrypted PAN

This method converts a financial token (FToken) to an encrypted PAN

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.EncryptedPANServiceApi;


EncryptedPANServiceApi apiInstance = new EncryptedPANServiceApi();
PanRequest panRequest = new PanRequest(); // PanRequest | panRequest
try {
    PanResponse result = apiInstance.getPan1UsingPOST(panRequest);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling EncryptedPANServiceApi#getPan1UsingPOST");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **panRequest** | [**PanRequest**](PanRequest.md)| panRequest |

### Return type

[**PanResponse**](PanResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getPanUsingGET"></a>
# **getPanUsingGET**
> PanResponse getPanUsingGET(posId, keyId, token)

Get encrypted PAN

This method converts a financial token (FToken) to an encrypted PAN

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.EncryptedPANServiceApi;


EncryptedPANServiceApi apiInstance = new EncryptedPANServiceApi();
String posId = "posId_example"; // String | Point of sale ID (8 bytes)
String keyId = "keyId_example"; // String | RSA/PKI Key ID
String token = "token_example"; // String | Financial token (FToken)
try {
    PanResponse result = apiInstance.getPanUsingGET(posId, keyId, token);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling EncryptedPANServiceApi#getPanUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **posId** | **String**| Point of sale ID (8 bytes) |
 **keyId** | **String**| RSA/PKI Key ID |
 **token** | **String**| Financial token (FToken) |

### Return type

[**PanResponse**](PanResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

