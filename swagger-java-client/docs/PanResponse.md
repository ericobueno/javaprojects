
# PanResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**encryptedPan** | **String** | Encrypted PAN | 
**requestDate** | **String** | Request Date | 
**errorMsg** | **String** | Error Message |  [optional]
**respCode** | **String** | Response Code | 
**token** | **String** | Financial Token | 



