
# PanRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**keyId** | **String** |  |  [optional]
**posId** | **String** |  |  [optional]
**token** | **String** |  |  [optional]



